Feature: Reporting a card lost/stolen
As a Customer
I want to report my card lost/stolen
So that other people cant use it

Scenario: An authenticated Customer can report a card lost/stolen
Given I am an authenticated Customer with an ISSUED card
When I report my card lost/stolen
Then the card will be marked as HOTLISTED

Scenario: An authenticated Customer can list why a card is lost/stolen
Given I am an authenticated Customer with an ISSUED card
When I report my card lost/stolen with details
Then the card will be marked as HOTLISTED

Scenario: An unauthenticated Customer cannot report their card lost/stolen
Given I am an unauthenticated Customer with an ISSUED card
When I try to report my card lost/stolen
Then I should be denied because I have not authenticated

Scenario: An authenticated Customer cannot report their hotlisted card as lost/stolen
Given I am an authenticated Customer with a HOTLISTED card
When I try to report my card as lost/stolen
Then I should be denied because the card is already Hotlisted

Scenario: An authenticated Customer cannot report an unlinked card as lost/stolen
Given I am an authenticated Customer with an unlinked ISSUED card
When I try to report my card as lost/stolen
Then I should be denied because the card does not belong to anyone

Scenario: An authenticated Customer cannot report another Customers card as lost/stolen
Given I am an authenticated Customer
When I try to report another Customers card as lost/stolen
Then I should be denied because the reported card does not belong to me

Scenario: An authenticated unlinked card cannot report itself as lost/stolen
Given I am an authenticated unlinked card with ISSUED state
When I try to report myself as lost/stolen
Then I should be denied because I cannot be marked as lost/stolen

Scenario: An authenticated unlinked card cannot report a Customers card as lost/stolen
Given I am an authenticated unlinked card with ISSUED state
When I try to report another Customers card as lost/stolen
Then I should be denied because I do not have permission