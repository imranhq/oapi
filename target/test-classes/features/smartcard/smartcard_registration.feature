Feature: Register a Smartcard to a Customer
As a holder of an unregistered smartcard
I want to register the smartcard to my account
So that no-one else can use this smartcard

Scenario: Already registered smartcard cannot be re-registered
Given I have authenticated as a customer holding an unlinked smartcard
And I successfully register that unlinked card to me
When I try to reregister that smartcard to myself
Then I will get an error trying to reregister my smartcard
And I can successfully unregister my registered card

Scenario: An authenticated Customer with an unlinked card can register the card as theirs
Given I have authenticated as a Customer
When I register an unlinked card
Then I will receive confirmation I've registered my card
And I will see the card in my list of linked cards

Scenario: An authenticated Smartcard cannot register itself
Given I have authenticated as a Smartcard
When I try to register myself
Then I will be denied permission to register the card to myself

Scenario: An authenticated Customer cannot register another Customers registered card
Given I have authenticated as a Customer
When I try to register another Customers card
Then I will be denied permission to register that card

Scenario: An authenticated Customer cannot register an Entitlement card
Given I have authenticated as a Customer
And I have an entitlement card I wish to register
When I try to register the entitlement card
Then I will not be able to register the entitlement card

Scenario: An unauthenticated Customer cannot register an unregistered unlinked card
Given I have not authenticated as a Customer holding an unregistered card
When I try to register an unlinked card
Then I will be denied permission to register my card

