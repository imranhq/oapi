Feature: Activating a new Smartcard
As a client app
I want to activate a new Smartcard
So that I can use the card for travelling

# Based off story OAP2-79

Scenario: A valid CustomerId and SmartcardId is provided
Given I am authenticated as a Customer
And the Smartcard is related to the registered Customer
When I activate the Smartcard
Then I will receive the SmartcardId as a response

Scenario: A valid Customer cannot activate another Customer card
Given I am authenticated as a Customer
When I try to activate using an unrelated CustomerId
Then I will receive a 403 error
And the Smartcard will not be activated

Scenario: A Customer cannot activate an unrelated Smartcard
Given I am authenticated as the Customer
And the Smartcard is not related to the registered Customer
When I try to activate the Smartcard
Then I will receive a 403 error
And the Smartcard will not be activated

Scenario Outline: An invalid Smartcard is rejected
Given I am authenticated as the Customer
And I have an invalid Card with ID <SmartcardId>, SC <SmartcardSN>
When I try to activate the invalid Card
Then I will receive an error
And the Smartcard will not be activated

Examples:
|SmartcardId     |SmartcardSN|
|123456          |0001       |
|3085230000000000|0001       |
|3085220000000000|0000       |

Scenario: A Smartcard that is already activated cannot be activated again
Given I am authenticated as the Customer
And I have an activated Smartcard
When I try to activate the Smartcard
Then I will receive an error

Scenario: A hotlisted Smartcard will not be activated
Given I am authenticated as the Customer
And I have a hotlisted Smartcard
When I try to activate the Smartcard
Then I will receive an error

Scenario: An authenticated Smartcard cannot activate itself
Given I am authenticated as a Smartcard
When I try to activate the Smartcard
Then I will receive an error