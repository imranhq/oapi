Feature: Viewing Smartcard Details
As a holder of a smartcard
I want to review my smartcard details
So that I can verify the information TfNSW has collected

Scenario: Authenticated Customer can view details on smartcard registered with them
Given I have authenticated as a customer with a linked smartcard
When I review the details for one of my smartcards
Then I should see the details of that smartcard

Scenario: Authenticated Customer without a linked smartcard gets a blank list when viewing details
Given I have authenticated as a customer without a linked smartcard
When I review the details of my non-existent smartcards
Then I should see no details about a smartcard

Scenario: Unregistered Smartcard can view details on itself
Given I have an unregistered smartcard
When I review the details of the smartcard
Then I should see the details of that smartcard

Scenario: Registered smartcard cannot view details on itself without authenticating
Given I have a registered smartcard
And I have not authenticated
When I review the details of the smartcard
Then I should not see the details of that smartcard

Scenario: Authenticated Customer cannot view details on another Customer card
Given I have authenticated as a customer
When I review the details for another customer card
Then I should be denied access to the details of that card