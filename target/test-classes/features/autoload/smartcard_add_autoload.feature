Feature: Setup of Smartcard Autoload
As a holder of a smartcard
I want to set my smartcard autoload
So that I dont have to queue each Monday morning to pay for my ticket

Scenario: An authenticated Customer can view autoload options for their card
Given I have an authenticated Customer with a linked card
When I look up autoload options for my card
Then I should see the autoload options for my card

Scenario: An authenticated Customer cannot view autoload options for another card
Given I have an authenticated Customer with a linked card
When I try to look up the autoload options for another card
Then I should be denied because its not my card

Scenario Outline: An authenticated Customer can view autoload options for a fare category
Given I have an authenticated Customer
When I look up autoload options for a <fare category>
Then I should see autoload options for the <fare category>

Examples:
|fare category|
|0            |
|1            |
|2            |
|3            |

Scenario: An authenticated Customer cannot see autoload options for an invalid fare category
Given I have an authenticated Customer
When I try to look up autoload options for an invalid fare category
Then I should receive an error because the fare category does not exist

Scenario: An authenticated Customer can set up an autoload for their card
Given I have an authenticated Customer without autoload set up
When I add a new autoload to my card
Then the autoload details for my card should have been added

Scenario: An authenticated Customer cannot change an autoload for an unlinked card
Given I have an authenticated Customer with stored payment details
When I try to change the autoload on an unlinked card
Then I should be denied because its not my card

Scenario: An authenticated Customer cannot change autoload for another Customers card
Given I have an authenticated Customer
When I ask to change the autoload on someone elses card
Then I should be denied because its not my card

Scenario: An authenticated unlinked card cannot set up autoload for itself
Given I have an authenticated unlinked card
When I ask to change the autoload on the unlinked card
Then I should be denied because Im not a customer

Scenario: An authenticated Customer cannot set up autoload if there is no billing information
Given I have an authenticated Customer without a stored payment method
When I ask to change the autoload on my card
Then I should be denied because theres no stored payment method

Scenario Outline: An authenticated Customer cannot set up autoload with an invalid amount
Given I have an authenticated Customer with a linked card and billing info
When I ask to change the autoload with <amount>
Then I should be denied because the autoload amount is invalid

Examples:
|amount|
|-1    |
|0000  |
|0001  |
|1001  |
|2005  |
|2100  |
|10001 |
|100000|