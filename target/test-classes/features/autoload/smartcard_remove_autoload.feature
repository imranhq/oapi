Feature: Setup of Smartcard Autoload
As a holder of a smartcard
I want to remove my smartcard autoload
So that I can take control of my finances

Scenario: An authenticated Customer can remove autoload on their card
Given I am an authenticated Customer with autoload on a linked card
When I remove autoload on my card
Then I should see no autoload details on my card

Scenario: An authenticated Customer cannot remove autoload on a card linked to another Customer
Given I am an authenticated Customer trying to annoy another Customer
When I attempt to remove autoload on another Customers card
Then I should be denied because I cannot remove autoload on another Customer card