Feature: Customer Payment Accounts
As a customer 
I want to review my billing details
So that I can verify the billing information stored is correct

Scenario: Customer billing details provided when authorized
Given I have authenticated as a  site user
When I request my billing details
Then I should see my details if I have provided my billing info

Scenario: Customer billing details not provided when unauthorized
Given I have not authenticated as a site user
When I request my billing details
Then I should not see deails of billing information


