Feature: Customer can view details on linked Smartcards
As a Customer
I want to view my Smartcards
So that I can maintain a healthy Smartcard lifestyle

Scenario: An authenticated Customer can view their linked cards
Given I am an authenticated Customer with some linked cards
When I view my Smartcards
Then I should see details of my linked cards

Scenario: An authenticated Customer without a card gets a blank list
Given I am an authenticated Customer without linked cards
When I view my Smartcards
Then I should receive a blank list instead of card details

Scenario: An unauthenticated Customer cannot view details on their linked card
Given I am an unauthenticated Customer with a linked card
When I view my Smartcards
Then I should be denied access to my smartcards