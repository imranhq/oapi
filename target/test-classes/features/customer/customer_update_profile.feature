Feature: Update non-security information on Customer profile
As a Customer
I want to update my details
So that correspondence with Opal is correct

Scenario: An authenticated Customer can update their profile
Given I am an authenticated Customer
When I updated my profile with valid information
Then I my profile will be updated to reflect the new information

Scenario: An unauthenticated Customer cannot update their profile
Given I am an unauthenticated Customer
When I try to update my profile
Then I will be denied because I have not authenticated

Scenario: An authenticated unlinked card cannot update their profile
Given I am an authenticated unlinked card
When I try to update my profile
Then I will be denied because I have no profile to update

Scenario: An authenticated Customer cannot update their profile with invalid information
Given I am an authenticated Customer
When I try to update my profile with invalid details
Then I will be denied because the information is invalid

Scenario: An authenticated Customer cannot update their profile with no update
Given I am an authenticated Customer
When I try to update my profile without providing any update
Then I will be denied because I need to provide an update

Scenario: An authenticated Customer cannot update their profile with the same info
Given I am an authenticated Customer
When I try to update my profile with the same information
Then I will be denied because its the same information