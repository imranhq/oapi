Feature: Unsuccessful smartcard topup scenarios
As a security analyst
I want to verify the integrity of ad-hoc payments
So that I have confidence in the asynchronous system

Scenario: An authenticated Customer cannot top-up an unlinked card
Given I am an authenticated Customer attempting a payment
When I try to topup an unlinked card
Then I should be denied because the card does not belong to me

Scenario: An authenticated Customer cannot top-up another Customers card
Given I am an authenticated Customer attempting a payment
When I try to topup another Customers card
Then I should be denied because the card does not belong to me

Scenario: An authenticated unlinked card cannot top-up another card
Given I am an authenticated card attempting a payment
When I try to top-up another card
Then I should be denied because the card is not the one I authenticated with

Scenario: An authenticated Customer cannot top-up with an invalid fare category
Given I am an authenticated Customer attempting a payment
When I try to top-up with an invalid Fare Category
Then I should be denied because the Fare Category does not exist

Scenario: An authenticated unlinked card cannot top-up with an invalid Fare Category
Given I am an authenticated unlinked card attempting a payment
When I try to top-up with an invalid Fare Category
Then I should be denied because the Fare Category does not exist

Scenario: An authenticated Customer cannot re-use the same Merchant Receipt
Given I am an authenticated Customer attempting a payment
When I make a payment and receive a Merchant Receipt
Then I should not be able to re-use that Merchant Receipt

Scenario: An authenticated Customer cannot use another Customers Merchant Receipt
Given I am an authenticated Customer attempting a payment
When I make a payment and receive a Merchant Receipt
Then another Customer should not be able to use that Merchant Receipt

Scenario: An authenticated Customer cannot refund money with a negative top-up
Given I am an authenticated Customer attempting a payment
When I make a payment with a negative amount
Then I should be denied because I should only be able to top-up with positive amounts

Scenario: An authenticated Customer cannot top-up with a random amount
Given I am an authenticated Customer attempting a payment
When I make a payment with an invalid positive amount
Then I should be denied because I should only be able to top-up with standard amounts

Scenario: An authenticated Customer cannot topup a hotlisted card
Given I am an authenticated Customer attempting a payment
When I make a payment with a hotlisted card
Then I should be denied because the card is hotlisted
