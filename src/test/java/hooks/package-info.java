/**
 * Contains the details that link Feature Files and Step Definitions.  Allows step definitions to be called by Cucumber.
 */
/**
 * Contains the details that link Feature Files and Step Definitions.  Allows step definitions to be called by Cucumber.
 * @author Mike Fleig - mfleig@planittesting.com
 * 
 */
package hooks;