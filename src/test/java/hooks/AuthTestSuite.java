package hooks;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * The parent of all tests.  This class drives the 'hook' between features and steps.
 * @author Mike Fleig - mfleig@planittesting.com
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features/authenticate", glue={"steps"}, plugin="json:target/cucumber-auth.json")
public class AuthTestSuite {
	
	/**
	 * Executed before the start of all features.
	 */
	@BeforeClass
	public static void FireUpSuite() {
		System.out.println("Starting execution");
		System.setProperty("java.net.preferIPv4Stack" , "true");
	}
	
	/**
	 * Executed at the end of all features.
	 */
	@AfterClass
	public static void ShutDownSuite() {
		
		System.out.println("Closing execution");
	}	
}

