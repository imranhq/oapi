package hooks;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * The organization and setup for Topup Features.  This class drives the 'hook' between features and steps.
 * @author Mike Fleig - mfleig@planittesting.com
 *
 */
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features/topup", glue={"steps/topup"}, plugin="json:target/cucumber-customer.json")
public class TopupTestSuite {
	
	/**
	 * Executed before the start of all features.
	 */
	@BeforeClass
	public static void FireUpSuite() {
		System.out.println("Started Topup Test Suite");
		System.setProperty("java.net.preferIPv4Stack" , "true");
	}
	
	/**
	 * Executed at the end of all features.
	 */
	@AfterClass
	public static void ShutDownSuite() {
		
		System.out.println("Completed Topup Test Suite");
	}	
}

