Feature: Two legged authentication
As an app developer that refuses to use Webviews
I want to use two legged authentication
So that I can protect my artistic integrity

Scenario: Customer can successfully authenticate using two-legged authentication
Given I am a valid Customer
When I successfully authenticate using two-legged authentication
Then I should be able to access my secured resources

Scenario: Invalid customer password cannot authenticate using two-legged authentication
Given I am a Customer with an incorrect password
When I try to authenticate using two-legged authentication
Then I should be denied trying to access secured resources

Scenario: Invalid customer username cannot authenticate using two-legged authentication
Given I am a Customer with an invalid username
When I try to authenticate using two-legged authentication
Then I should not be granted an access token

Scenario: Smartcard can successfully authenticate using two-legged authentication
Given I am an unlinked Smartcard
When I successfully authenticate using two-legged authentication
Then I should be able to access secured resources

Scenario: Invalid Smartcard security number cannot authenticate using two-legged authentication
Given I am an unlinked Smartcard with an invalid security number
When I try to authenticate using two-legged authentication
Then I should not be granted an access token
And I cannot access secured resources

Scenario: Invalid Smartcard Id cannot authenticate using two-legged authentication
Given I am an unlinked Smartcard with an invalid Id
When I try to authenticate using two-legged authentication
Then I should not be granted an access token
And I cannot access secured resources

Scenario: Customer cannot log in with dual credentials
Given I have valid Customer details and valid unlinked Smartcard details
When I try to authenticate with both credentials at once
Then I should not be granted an access token
And I cannot access secured resources

Scenario: Customer cannot log in with mixed credentials
Given I have a valid Customer and 4 digit security code
When I try to authenticate using username and security code
Then I should be denied because the credentials are incompatible

Scenario: Customer cannot log in with mixed credentials
Given I have a valid Customer and unlinked Smartcard
When I try to authenticate using Smartcard id and password
Then I should be denied because the credentials are incompatible

Scenario: Blocked Customer cannot log in
Given I am a valid Customer with my login blocked
When I try to authenticate using two-legged authentication
Then I should not be granted an access token