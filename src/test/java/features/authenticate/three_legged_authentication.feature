Feature: Three legged authentication rules
As a requester of the API
I want to authenticate myself
So that I can access appropriate resources

Scenario: Valid smartcard credentials are accepted
Given I log in with a valid smartcard
When I request a token with a valid authentication code
Then I should receive a valid token to access secure resources

Scenario: Valid user credentials are accepted
Given I log in with username miketest001 and password T3ST1NG!
When I request a token with a valid authentication code
Then I should receive a valid token to access secure resources

Scenario: Invalid client_id is rejected before login
Given I have an invalid client_id
When I load request the callback
Then I will not receive a session_id

Scenario: Invalid credentials are rejected at login
Given I have an invalid username and password
When I try to get an authentication code
Then I shall receive an error

Scenario: Invalid password is rejected at login
Given I have an valid username and invalid password
When I try to get an authentication code
Then I shall receive an error

Scenario: Invalid security code is rejected at login
Given I have a valid smartcard and invalid security code
When I try to get an authentication code
Then I shall receive an error

Scenario: Already registered smartcard is rejected at login
Given I have an a smartcard belonging to a Customer
When I try to get an authentication code
Then I shall receive an error

Scenario: Invalid authentication code does not provide a token
Given I log in with username miketest001 and password T3ST1NG!
When I request a token with an invalid authentication code
Then I should not receive a token

Scenario: Mismatching client_ids does not provide a token
Given I log in with username miketest001 and password T3ST1NG!
When I request a token with a different client_id
Then I should not receive a token

Scenario: Mismatching redirect_uris do not provide a token
Given I log in with username miketest001 and password T3ST1NG!
When I request a token with a different redirect_uri
Then I should not receive a token