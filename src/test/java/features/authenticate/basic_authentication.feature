Feature: Basic authentication rules
As a requester of the API
I want to authenticate myself
So that I can access appropriate resources

@authenticate
Scenario: Authenticated user has refresh revoked
Given I have authenticated with username miketest001 and password T3ST1NG!
When I revoke my refresh token
Then I cannot refresh my access with my refresh token

@authenticate
Scenario: Authenticated user has access revoked
Given I have authenticated with username miketest001 and password T3ST1NG!
When I revoke my access token
Then I cannot access my secure resources

@authenticate
Scenario: Authenticated smartcard has refresh revoked
Given I have authenticated with an unregistered smartcard
When I revoke my refresh token
Then I cannot refresh my access with my refresh token

@authenticate
Scenario: Authenticated smartcard has access revoked
Given I have authenticated with an unregistered smartcard
When I revoke my access token
Then I cannot access the secure resources

@authenticate
Scenario: Authenticated user cannot refresh with invalid refresh token
Given I have authenticated with username miketest001 and password T3ST1NG!
When I refresh my token with an invalid refresh_token
Then I will not receive a new token
And my current token is still valid

@authenticate
Scenario: Authenticated user can refresh with valid refresh token
Given I have authenticated with username miketest001 and password T3ST1NG!
When I refresh my token with a valid refresh_token
Then I will receive a new valid token
And my old token is invalid

@authenticate
Scenario: Authenticated smartcard cannot refresh with invalid refresh token
Given I have authenticated with an unregistered smartcard
When I refresh my token with an invalid refresh_token
Then I will not receive a new token
And my current token is still valid

@authenticate
Scenario: Authenticated user can refresh with valid refresh token
Given I have authenticated with an unregistered smartcard
When I refresh my token with a valid refresh_token
Then I will receive a new valid token
And my old token is invalid