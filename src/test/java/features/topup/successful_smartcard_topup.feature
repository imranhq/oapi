Feature: Successful top-up scenarios
Given I am a Customer
I want to top-up my smartcard
So that I can pay for my trips

Scenario: An authenticated Customer can view top-up options for a linked card
Given I am an authenticated Customer with a linked card
When I view the top-up options for my card
Then I should see a list of top-up options for my card

Scenario: An authenticated unlinked card can view top-up options for itself
Given I am an authenticated unlinked card
When I view the top-up options for myself
Then I should see a list of top-up options for myself

Scenario Outline: An authenticated Customer can view top-up options for a Fare Category
Given I am an authenticated Customer with a linked card
When I view top-up options for Fare Category <category>
Then I should see a list of top-up options for Fare Category <category>
Examples:
|category|
|0       |
|1       |
|2       |
|3       |

Scenario Outline: An authenticated unlinked card can view top-up options for a Fare Category
Given I am an authenticated unlinked card
When I view top-up options for Fare Category <category>
Then I should see a list of top-up options for Fare Category <category>
Examples:
|category|
|0       |
|1       |
|2       |
|3       |

Scenario: An authenticated Customer can top-up a linked card with existing billing information
Given I am an authenticated Customer with existing billing information
When I top-up my card with my existing billing information
Then I should see my balance has been updated

Scenario: An authenticated Customer can top-up a linked card with ad-hoc billing information
Given I am an authenticated Customer with a linked card
When I top-up my card with ad-hoc billing information
Then I should see my balance has been updated

Scenario: An authenticated unlinked card can top-up with ad-hoc billing information
Given I am an authenticated unlinked card
When I top-up myself with ad-hoc billing information
Then I should see my balance has been updated