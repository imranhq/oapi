Feature: View entitlement details of linked cards
As a Customer
I want to see the entitlement details for my card
So I can monitor and track my concession cards

Scenario: An authenticated Customer can view details of Entitlement card
Given I am an authenticated Customer with a Concession card
When I look up the details of my concession card
Then I will see the correct details of my concession card

Scenario: An authenticated Customer cannot look up entitlement details of another Customer card
Given I am an authenticated Customer
When I try to look up the details of another Customers concession card
Then I will be denied because the card does not belong to me

Scenario: An authenticated Customer cannot look up entitlement details of an unlinked card
Given I am an authenicated Customer
When I try to look up the details of an unlinked card
Then I will be denied because the card does not belong to anyone

Scenario: An authenticated Customer cannot look up entitlement details of a non-entitlement card
Given I am an authenticated Customer with a non-concession card
When I try to look up the entitlement details of my non-concession card
Then I will be denied because there are no entitlement details

Scenario: An unauthenticated Customer cannot look up entitlement details of their card
Given I am an unauthenticated Customer with a valid concession card
When I try to look up the details of my concession card
Then I will be denied because I have not logged in

Scenario: An authenticated unlinked card can look up entitlement details of itself
Given I have authenticated as an unlinked smartcard
When I look up the details of myself
Then I will be able to see my entitlement details

Scenario: An authenticated Customer cannot look up entitlement details of an invalid card
Given I have authenticated as a Customer
When I try to look up the details of an invalid card
Then I should be denied because the card is invalid