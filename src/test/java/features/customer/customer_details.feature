Feature: Customer details
As a customer
I want to review my details
So that I can verify the information TfNSW has collected on me

@smoke
Scenario: Customer details provided when authorized
Given I have authenticated as a user
When I request my details
Then I should see my details

Scenario: Customer details not provided when unauthorized
Given I have not authenticated as a user
When I request customer details
Then I should see no details

Scenario: Customer details not provided when authorized as Smartcard
Given I have authenticated as a smartcard
When I request my details
Then I should be denied access

Scenario: Customer details informaton provided when authorized
Given I have authenticated as a user
When I request my details
Then My details should match initial details

 
