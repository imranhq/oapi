package steps;

import java.util.HashMap;

import org.junit.Assert;

import model.actors.Authenticator;
import model.datatypes.Token;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import utilities.Response;
import utilities.ResponseElement;

/**
 * Methods relating to all the step definitions
 * @author Mike Fleig - mfleig@planittesting.com
 */
public class BaseSteps {
	protected Authenticator requester;
	protected AuthenticateService authService;
	protected String sessionID;
	protected Response response;
	protected ResponseElement details;
	protected String authCode;
	protected Token newToken;
	protected HashMap<String, ParameterValue> badParams = new HashMap<String, ParameterValue>();
	
	protected Authenticator authenticateUser(Authenticator req) {
		//Log in
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
		
		authCode = authService.requestAuthorizationCode(req, sessionID);
		Assert.assertNotEquals("An invalid authorization code was returned", "", authCode);
	    
		newToken = authService.requestAccessToken(authCode);
		req.Token = newToken;

		//Check we have access before continuing on
	    Assert.assertNotNull("We do not have an access token when authenticating as a user", req.Token.accessToken);
	    Assert.assertNotNull("We do not have a refresh token when authenticating as a user", req.Token.refreshToken);
	    
	    return req;
	}
}
