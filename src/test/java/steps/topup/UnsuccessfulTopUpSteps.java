package steps.topup;

import org.junit.Assert;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.Authenticator;
import model.actors.MobileUser;
import model.actors.Smartcard;
import model.datatypes.BPointPayReference;
import model.datatypes.CreditCard;
import model.datatypes.DetailedSmartcard;
import model.datatypes.Order;
import model.datatypes.PaymentAuthority;
import model.services.AuthenticateService;
import model.services.BPointAuthService;
import model.services.BPointPayService;
import model.services.PaymentService;
import model.services.TopUpService;
import steps.BaseSteps;
import utilities.ESB;
import utilities.data.TestData;

public class UnsuccessfulTopUpSteps extends BaseSteps{
	
	private TopUpService topUpService;
	private PaymentAuthority payAuth;
	private long payReceipt;
	private int topup;
	
	@Before
	public void setupAuthentication() {
		authService = new AuthenticateService();

	}
	
	@Given("^I am an authenticated Customer attempting a payment$")
	public void i_am_an_authenticated_Customer_attempting_a_payment() {
		requester = TestData.getUser(true, true);
		requester = authenticateUser(requester);		
		topUpService = new TopUpService(requester);
		requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID()).getID()));
		
	    if(ESB.GetValueOnCard(String.valueOf(requester.getCard().getID())) > 8000) {
	    	ESB.AddValueToCard(String.valueOf(requester.getCard().getID()), -8000);
	    }
	}

	@Given("^I am an authenticated unlinked card attempting a payment$")
	public void i_am_an_authenticated_unlinked_card_attempting_a_payment() throws Throwable {
	    requester = TestData.getSmartcard();
	    requester = authenticateUser(requester);
		topUpService = new TopUpService(requester);
	    if(ESB.GetValueOnCard(requester.getID()) > 8000) {
	    	ESB.AddValueToCard(requester.getID(), -8000);
	    }
	}
	
	@Given("^I am an authenticated card attempting a payment$")
	public void i_am_an_authenticated_card_attempting_a_payment() throws Throwable {
	    requester = TestData.getSmartcard();
	    requester = authenticateUser(requester);
		topUpService = new TopUpService(requester);
	    if(ESB.GetValueOnCard(requester.getID()) > 8000) {
	    	ESB.AddValueToCard(requester.getID(), -8000);
	    }
	}
	
	@When("^I try to topup an unlinked card$")
	public void i_try_to_topup_an_unlinked_card() {
	    response = topUpService.getTopUpAmountsResponse(TestData.getSmartcard().getCard());
	}

	@When("^I try to topup another Customers card$")
	public void i_try_to_topup_another_Customers_card() {
	    MobileUser otherCustomer = TestData.getUser(true);
	    Smartcard otherCard = TestData.getUsersSmartcard(otherCustomer.getID());
	    
	    response = topUpService.getTopUpAmountsResponse(otherCard.getCard());
	}

	@When("^I try to top-up another card$")
	public void i_try_to_top_up_another_card() throws Throwable {
	    MobileUser otherCustomer = TestData.getUser(true);
	    Smartcard otherCard = TestData.getUsersSmartcard(otherCustomer.getID());
	    
	    response = topUpService.getTopUpAmountsResponse(otherCard.getCard());
	}

	@When("^I make a payment with an invalid positive amount$")
	public void i_make_a_payment_with_an_invalid_positive_amount() throws Throwable {
		BPointAuthService bpointAuthService = new BPointAuthService(requester);
		BPointPayService bpointPayService = new BPointPayService();

		int topup = 1337;

		CreditCard creditCard = new CreditCard(Long.parseLong("5123456789012346"), 123, "00", "99");
		
		//Client app call the API 5.12 BPoint Payment Auth Token - GET /bpoint/paymentauthtoken/
		//API Gateway returns PaymentAuthToken.
		PaymentAuthority payAuth = bpointAuthService.getPaymentAuthority(requester.getCard(), topup);
		
		//Client app do POST to BPoint pay endpoint. PaymentAuthToken is included as “in_pay_token” parameter. Consult BPoint Payment Connector manual for sample request & more information.
		//BPoint returns response.
		BPointPayReference bpPayRef = bpointPayService.pay(payAuth, creditCard, topup);
		
		//If BPoint return success in response, client app call the API 5.13 BPoint Payment Transaction - POST /bpoint/paymenttransaction/. BPoint response parameters are included as PaymentProviderResponse parameter.
		PaymentService payService = new PaymentService(requester);
		long payReceipt = payService.processPayment(requester.getCard(), bpPayRef);
		
		//Proceed with the intention of payment, such as top up or card order.
		TopUpService topUpService = new TopUpService(requester);
		response = topUpService.topUpCardResponse(requester.getCard(), payReceipt, topup);
	}
	
	@When("^I try to top-up with an invalid Fare Category$")
	public void i_try_to_top_up_with_an_invalid_Fare_Category() throws Throwable {
		BPointAuthService bpointAuthService = new BPointAuthService(requester);
		BPointPayService bpointPayService = new BPointPayService();

		int topup = 5000;

		CreditCard creditCard = new CreditCard(Long.parseLong("5123456789012346"), 123, "00", "99");
		
		//Client app call the API 5.12 BPoint Payment Auth Token - GET /bpoint/paymentauthtoken/
		//API Gateway returns PaymentAuthToken.
		PaymentAuthority payAuth = bpointAuthService.getPaymentAuthority(requester.getCard(), topup);
		
		//Client app do POST to BPoint pay endpoint. PaymentAuthToken is included as “in_pay_token” parameter. Consult BPoint Payment Connector manual for sample request & more information.
		//BPoint returns response.
		BPointPayReference bpPayRef = bpointPayService.pay(payAuth, creditCard, topup);
		
		//If BPoint return success in response, client app call the API 5.13 BPoint Payment Transaction - POST /bpoint/paymenttransaction/. BPoint response parameters are included as PaymentProviderResponse parameter.
		PaymentService payService = new PaymentService(requester);
		long payReceipt = payService.processPayment(requester.getCard(), bpPayRef);
		
		//Proceed with the intention of payment, such as top up or card order.
		TopUpService topUpService = new TopUpService(requester);
		response = topUpService.topUpCardResponse(requester.getCard(), payReceipt, topup);
	}
	
	@When("^I make a payment and receive a Merchant Receipt$")
	public void i_make_a_payment_and_receive_a_Merchant_Receipt() throws Throwable {
		BPointAuthService bpointAuthService = new BPointAuthService(requester);
		BPointPayService bpointPayService = new BPointPayService();

		topup = 4000;

		CreditCard creditCard = new CreditCard(Long.parseLong("5123456789012346"), 123, "00", "99");
		
		//Client app call the API 5.12 BPoint Payment Auth Token - GET /bpoint/paymentauthtoken/
		//API Gateway returns PaymentAuthToken.
		PaymentAuthority payAuth = bpointAuthService.getPaymentAuthority(requester.getCard(), topup);
		
		//Client app do POST to BPoint pay endpoint. PaymentAuthToken is included as “in_pay_token” parameter. Consult BPoint Payment Connector manual for sample request & more information.
		//BPoint returns response.
		BPointPayReference bpPayRef = bpointPayService.pay(payAuth, creditCard, topup);
		
		//If BPoint return success in response, client app call the API 5.13 BPoint Payment Transaction - POST /bpoint/paymenttransaction/. BPoint response parameters are included as PaymentProviderResponse parameter.
		PaymentService payService = new PaymentService(requester);
		payReceipt = payService.processPayment(requester.getCard(), bpPayRef);
	}

	@When("^I make a payment with a negative amount$")
	public void i_make_a_payment_with_a_negative_amount() throws Throwable {
		BPointAuthService bpointAuthService = new BPointAuthService(requester);
		
		topup = -4000;

		//Client app call the API 5.12 BPoint Payment Auth Token - GET /bpoint/paymentauthtoken/
		//API Gateway returns PaymentAuthToken.
		payAuth = bpointAuthService.getPaymentAuthority(requester.getCard(), topup);
	}
	
	@When("^I make a payment with a hotlisted card$")
	public void i_make_a_payment_with_a_hotlisted_card() {
		BPointAuthService bpointAuthService = new BPointAuthService(requester);
		BPointPayService bpointPayService = new BPointPayService();

		int topup = 4000;
		if(!ESB.GetCardState(String.valueOf(requester.getCard().getID())).equals("HOTLISTED")) {
			ESB.HotlistCard(String.valueOf(requester.getCard().getID()));
		}

		CreditCard creditCard = new CreditCard(Long.parseLong("5123456789012346"), 123, "00", "99");
		
		//Client app call the API 5.12 BPoint Payment Auth Token - GET /bpoint/paymentauthtoken/
		//API Gateway returns PaymentAuthToken.
		PaymentAuthority payAuth = bpointAuthService.getPaymentAuthority(requester.getCard(), topup);
		
		//Client app do POST to BPoint pay endpoint. PaymentAuthToken is included as “in_pay_token” parameter. Consult BPoint Payment Connector manual for sample request & more information.
		//BPoint returns response.
		BPointPayReference bpPayRef = bpointPayService.pay(payAuth, creditCard, topup);
		
		//If BPoint return success in response, client app call the API 5.13 BPoint Payment Transaction - POST /bpoint/paymenttransaction/. BPoint response parameters are included as PaymentProviderResponse parameter.
		PaymentService payService = new PaymentService(requester);
		long payReceipt = payService.processPayment(requester.getCard(), bpPayRef);
		
		//Proceed with the intention of payment, such as top up or card order.
		TopUpService topUpService = new TopUpService(requester);
		response = topUpService.topUpCardResponse(requester.getCard(), payReceipt, topup);
		if(ESB.GetCardState(String.valueOf(requester.getCard().getID())).equals("HOTLISTED")) {
			ESB.DeHotlistCard(String.valueOf(requester.getCard().getID()));
		}
	}

	@Then("^I should be denied because the card is hotlisted$")
	public void i_should_be_denied_because_the_card_is_hotlisted() {
		Assert.assertTrue("We topped up a hotlisted card", response.getBodyElement().contains("ErrorType"));
		Assert.assertEquals("We did not get an expected business error", 425, response.getResponseCode());
	}
	 
	@Then("^I should be denied because the card does not belong to me$")
	public void i_should_be_denied_because_the_card_does_not_belong_to_me() throws Throwable {
	    Assert.assertEquals("We weren't denied access to topping up another persons card", 403, response.getResponseCode());
	}

	@Then("^I should be denied because the card is not the one I authenticated with$")
	public void i_should_be_denied_because_the_card_is_not_the_one_I_authenticated_with() {
		Assert.assertEquals("We weren't denied access to topping up another persons card", 403, response.getResponseCode());
	}

	@Then("^I should be denied because the Fare Category does not exist$")
	public void i_should_be_denied_because_the_Fare_Category_does_not_exist() throws Throwable {
		Assert.assertTrue("We topped up with a non-standard Fare Category amount", response.getBodyElement().contains("ErrorType"));
		Assert.assertEquals("We did not get an expected business error", 425, response.getResponseCode());
	}

	@Then("^I should not be able to re-use that Merchant Receipt$")
	public void i_should_not_be_able_to_re_use_that_Merchant_Receipt() throws Throwable {
		
		//Proceed with the intention of payment, such as top up or card order.
		TopUpService topUpService = new TopUpService(requester);
		Order order = topUpService.topUpCard(requester.getCard(), payReceipt, topup);
		
		Assert.assertEquals("We did not successfully top up", requester.getCard().getID(), order.getSmartcardId());
		
		response = topUpService.topUpCardResponse(requester.getCard(), payReceipt, topup);
		
		Assert.assertTrue("We topped up twice with a merchant receipt", response.getBodyElement().contains("ErrorType"));
		Assert.assertEquals("We did not get an expected business error", 425, response.getResponseCode());	
	}

	@Then("^another Customer should not be able to use that Merchant Receipt$")
	public void another_Customer_should_nto_be_able_to_use_that_Merchant_Receipt() throws Throwable {
		//Proceed with the intention of payment, such as top up or card order.
		Authenticator otherCustomer = TestData.getUser(true);
		otherCustomer.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(otherCustomer.getID()).getID()));
		otherCustomer = authenticateUser(otherCustomer);
		
		TopUpService topUpService = new TopUpService(otherCustomer);
		response = topUpService.topUpCardResponse(otherCustomer.getCard(), payReceipt, topup);
		Assert.assertEquals("We managed to use the Merchant receipt to order for another Customer", 425, response.getResponseCode());
	}

	@Then("^I should be denied because I should only be able to top-up with positive amounts$")
	public void i_should_be_denied_because_I_should_only_be_able_to_top_up_with_positive_amounts() throws Throwable {
		Assert.assertEquals("We received authority to proceed with payment for a negative amount", "", payAuth.getPaymentAuthToken());
		Assert.assertEquals("We were provided a Merchant receipt for an invalid transaction", 0, payAuth.getMerchantReceipt());
	}

	@Then("^I should be denied because I should only be able to top-up with standard amounts$")
	public void i_should_be_denied_because_I_should_only_be_able_to_top_up_with_standard_amounts() throws Throwable {
		Assert.assertTrue("We topped up with a non-standard amount", response.getBodyElement().contains("ErrorType"));
		Assert.assertEquals("We did not get an expected business error", 425, response.getResponseCode());
	}
}
