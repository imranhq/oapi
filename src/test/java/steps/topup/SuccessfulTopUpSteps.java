package steps.topup;

import java.util.ArrayList;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.MobileUser;
import model.actors.Smartcard;
import model.datatypes.BPointPayReference;
import model.datatypes.CreditCard;
import model.datatypes.Order;
import model.datatypes.PaymentAuthority;
import model.datatypes.TopUpAmount;
import model.services.AuthenticateService;
import model.services.BPointAuthService;
import model.services.BPointPayService;
import model.services.CustomerService;
import model.services.PaymentService;
import model.services.SmartcardDetailsService;
import model.services.TopUpService;
import steps.BaseSteps;
import utilities.ESB;
import utilities.data.TestData;

public class SuccessfulTopUpSteps extends BaseSteps {
	
	private TopUpService topUpService;
	private ArrayList<TopUpAmount> amounts;
	private ArrayList<TopUpAmount> expectedAmounts;
	private int oldBalance;
	private int topup;
	private int newBalance;
	private Order order;
	private ArrayList<TopUpAmount> adultAmounts;
	private ArrayList<TopUpAmount> seniorAmounts;
	private ArrayList<TopUpAmount> childAmounts;
	private ArrayList<TopUpAmount> concessionAmounts;
	
	@Before
	public void BeforeScenario(Scenario scenario) {
		authService = new AuthenticateService();
		amounts = new ArrayList<TopUpAmount>();
		buildAmounts();
	}
	
	private void buildAmounts() {
		//Adults!
		adultAmounts = new ArrayList<TopUpAmount>();
		adultAmounts.add(new TopUpAmount(4000, 4000, "$40 Adult Remote Top Up"));
		adultAmounts.add(new TopUpAmount(6000, 6000, "$60 Adult Remote Top Up"));
		adultAmounts.add(new TopUpAmount(8000, 8000, "$80 Adult Remote Top Up"));
		adultAmounts.add(new TopUpAmount(10000, 10000, "$100 Adult Remote Top Up"));
		adultAmounts.add(new TopUpAmount(12000, 12000, "$120 Adult Remote Top Up"));
		
		//Seniors!
		seniorAmounts = new ArrayList<TopUpAmount>();
		seniorAmounts.add(new TopUpAmount(1000, 1000, "$10 Senior/Pensioner Remote Top Up"));
		seniorAmounts.add(new TopUpAmount(2000, 2000, "$20 Senior/Pensioner Remote Top Up"));
		seniorAmounts.add(new TopUpAmount(3000, 3000, "$30 Senior/Pensioner Remote Top Up"));
		seniorAmounts.add(new TopUpAmount(4000, 4000, "$40 Senior/Pensioner Remote Top Up"));
		seniorAmounts.add(new TopUpAmount(5000, 5000, "$50 Senior/Pensioner Remote Top Up"));
		seniorAmounts.add(new TopUpAmount(6000, 6000, "$60 Senior/Pensioner Remote Top Up"));
		
		//Kids!
		childAmounts = new ArrayList<TopUpAmount>();
		childAmounts.add(new TopUpAmount(2000, 2000, "$20 Child/Youth Remote Top Up"));
		childAmounts.add(new TopUpAmount(3000, 3000, "$30 Child/Youth Remote Top Up"));
		childAmounts.add(new TopUpAmount(4000, 4000, "$40 Child/Youth Remote Top Up"));
		childAmounts.add(new TopUpAmount(5000, 5000, "$50 Child/Youth Remote Top Up"));
		childAmounts.add(new TopUpAmount(6000, 6000, "$60 Child/Youth Remote Top Up"));
		
		//Concession!
		concessionAmounts = new ArrayList<TopUpAmount>();
		concessionAmounts.add(new TopUpAmount(2000, 2000, "$20 Concession Remote Top Up"));
		concessionAmounts.add(new TopUpAmount(3000, 3000, "$30 Concession Remote Top Up"));
		concessionAmounts.add(new TopUpAmount(4000, 4000, "$40 Concession Remote Top Up"));
		concessionAmounts.add(new TopUpAmount(5000, 5000, "$50 Concession Remote Top Up"));
		concessionAmounts.add(new TopUpAmount(6000, 6000, "$60 Concession Remote Top Up"));
	}
	
	private ArrayList<TopUpAmount> getExpectedAmounts(int fcc) {
		switch(fcc) {
		case 0:
			return adultAmounts;
		case 1:
			return concessionAmounts;
		case 2:
			return seniorAmounts;
		case 3:
			return childAmounts;
		default:
			return adultAmounts;
		}
	}
	
	@Given("^I am an authenticated unlinked card$")
	public void i_am_an_authenticated_unlinked_card() {
	    requester = TestData.getSmartcard();
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	}
	
	@Given("^I am an authenticated Customer with a linked card$")
	public void i_am_an_authenticated_Customer_with_a_linked_card() {
		requester = TestData.getUser(true);
		requester = authenticateUser(requester);

		CustomerService cs = new CustomerService(requester);
		requester.addCard(cs.getSmartcardDetails());
		
	    if(ESB.GetValueOnCard(String.valueOf(requester.getCard().getID())) > 8000) {
	    	ESB.AddValueToCard(String.valueOf(requester.getCard().getID()), -8000);
	    }
	}

	@Given("^I am an authenticated Customer with existing billing information$")
	public void i_am_an_authenticated_Customer_with_existing_billing_information() {
		requester = TestData.getUser(true, true);
		requester = authenticateUser(requester);
		
		CustomerService cs = new CustomerService(requester);
		requester.addCard(cs.getSmartcardDetails());
		
	    if(ESB.GetValueOnCard(String.valueOf(requester.getCard().getID())) > 8000) {
	    	ESB.AddValueToCard(String.valueOf(requester.getCard().getID()), -8000);
	    }
	}
	
	@When("^I view the top-up options for my card$")
	public void i_view_the_top_up_options_for_my_card() {
		topUpService = new TopUpService(requester);
		amounts = topUpService.getTopUpAmounts(requester.getCard());
	}

	@When("^I top-up my card with ad-hoc billing information$")
	public void i_top_up_my_card_with_ad_hoc_billing_information() {
		BPointAuthService bpointAuthService = new BPointAuthService(requester);
		BPointPayService bpointPayService = new BPointPayService();
		SmartcardDetailsService scDS = new SmartcardDetailsService(requester);
		topup = 4000;
		oldBalance = scDS.requestDetails(String.valueOf(requester.getCard().getID())).getIntValue("SVPending");
		CreditCard creditCard = new CreditCard(Long.parseLong("5123456789012346"), 123, "00", "99");
		
		//Client app call the API 5.12 BPoint Payment Auth Token - GET /bpoint/paymentauthtoken/
		//API Gateway returns PaymentAuthToken.
		PaymentAuthority payAuth = bpointAuthService.getPaymentAuthority(requester.getCard(), topup);
		
		//Client app do POST to BPoint pay endpoint. PaymentAuthToken is included as “in_pay_token” parameter. Consult BPoint Payment Connector manual for sample request & more information.
		//BPoint returns response.
		BPointPayReference bpPayRef = bpointPayService.pay(payAuth, creditCard, topup);
		
		//If BPoint return success in response, client app call the API 5.13 BPoint Payment Transaction - POST /bpoint/paymenttransaction/. BPoint response parameters are included as PaymentProviderResponse parameter.
		PaymentService payService = new PaymentService(requester);
		long payReceipt = payService.processPayment(requester.getCard(), bpPayRef);
		
		//Proceed with the intention of payment, such as top up or card order.
		TopUpService topUpService = new TopUpService(requester);
		topUpService.topUpCard(((MobileUser)requester).getCard(), payReceipt, topup);
	}

	@When("^I top-up myself with ad-hoc billing information$")
	public void i_top_up_myself_with_ad_hoc_billing_information() throws Throwable {
		BPointAuthService bpointAuthService = new BPointAuthService(requester);
		BPointPayService bpointPayService = new BPointPayService();
		SmartcardDetailsService scDS = new SmartcardDetailsService(requester);
		topup = 4000;
		oldBalance = scDS.requestDetails(requester.getID()).getIntValue("SVPending");
		CreditCard creditCard = new CreditCard(Long.parseLong("5123456789012346"), 123, "00", "99");
		
		//Client app call the API 5.12 BPoint Payment Auth Token - GET /bpoint/paymentauthtoken/
		//API Gateway returns PaymentAuthToken.
		PaymentAuthority payAuth = bpointAuthService.getPaymentAuthority(requester.getCard(), topup);
		
		//Client app do POST to BPoint pay endpoint. PaymentAuthToken is included as “in_pay_token” parameter. Consult BPoint Payment Connector manual for sample request & more information.
		//BPoint returns response.
		BPointPayReference bpPayRef = bpointPayService.pay(payAuth, creditCard, topup);
		
		//If BPoint return success in response, client app call the API 5.13 BPoint Payment Transaction - POST /bpoint/paymenttransaction/. BPoint response parameters are included as PaymentProviderResponse parameter.
		PaymentService payService = new PaymentService(requester);
		long payReceipt = payService.processPayment(requester.getCard(), bpPayRef);
		
		//Proceed with the intention of payment, such as top up or card order.
		TopUpService topUpService = new TopUpService(requester);
		topUpService.topUpCard(requester.getCard(), payReceipt, topup);
	}

	@When("^I view the top-up options for myself$")
	public void i_view_the_top_up_options_for_myself() throws Throwable {
		topUpService = new TopUpService(requester);
		amounts = topUpService.getTopUpAmounts(requester.getCard());
	}

	@When("^I view top-up options for Fare Category (\\d+)$")
	public void i_view_the_top_up_options_for_a_Fare_Category(int fcc) throws Throwable {
		topUpService = new TopUpService(requester);
		amounts = topUpService.getTopUpAmounts(fcc);
	}

	@When("^I top-up my card with my existing billing information$")
	public void i_top_up_my_card_with_my_existing_billing_information() throws Throwable {
	    topUpService = new TopUpService(requester);
		SmartcardDetailsService scDS = new SmartcardDetailsService(requester);
		topup = 4000;
		oldBalance = scDS.requestDetails(String.valueOf(requester.getCard().getID())).getIntValue("SVPending");
		order = topUpService.topUpCard(requester.getCard(), topup);
	}
	
	@Then("^I should see a list of top-up options for my card$")
	public void i_should_see_a_list_of_top_up_options_for_my_card() throws Throwable {
		TopUpAmount expectedAmount = new TopUpAmount(4000, 4000, "$40 Adult Remote Top Up");
	    Assert.assertEquals(expectedAmount, amounts.get(0));
	    Assert.assertTrue(topUpService.topUpAmountsContain(((MobileUser) requester).getCard(), expectedAmount));
	}

	@Then("^I should see a list of top-up options for myself$")
	public void i_should_see_a_list_of_top_up_options_for_myself() throws Throwable {
		TopUpAmount expectedAmount = new TopUpAmount(4000, 4000, "$40 Adult Remote Top Up");
	    Assert.assertEquals(expectedAmount, amounts.get(0));
	    Assert.assertTrue(topUpService.topUpAmountsContain(requester.getCard(), expectedAmount));
	}

	@Then("^I should see a list of top-up options for Fare Category (\\d+)$")
	public void i_should_see_a_list_of_top_up_options_for_the_Fare_Category(int fcc) throws Throwable {
		expectedAmounts = getExpectedAmounts(fcc);
		
		for(TopUpAmount expectedAmount : expectedAmounts) {
			Assert.assertTrue("Our topups did not contain an expected amount", amounts.contains(expectedAmount));
		}
	}
	
	@Then("^I should see my balance has been updated$")
	public void i_should_see_my_balance_has_been_updated() {
		SmartcardDetailsService scDS = new SmartcardDetailsService(requester);
		String card = "";
		if(requester instanceof Smartcard) {
			card = requester.getID();
		}
		
		if(requester instanceof MobileUser) {
			card = String.valueOf(requester.getCard().getID());
		}
		
		newBalance = scDS.requestDetails(card).getIntValue("SVPending");
		
		//Balance Diff should reflect difference between old balance before test and new balance after test
		int balanceDiff = newBalance - oldBalance;
		
		Assert.assertEquals("The difference before and after topup does not match topup amount", topup, balanceDiff);
	}

	@Then("^I can see my active payment account$")
	public void i_can_see_my_active_payment_account() throws Throwable {
	    System.out.println(order);
	    throw new PendingException();
	}
}
