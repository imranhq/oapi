package steps;

import java.util.HashMap;

import org.junit.Assert;
 
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.MobileUser;
import model.datatypes.CustomerDetails;
import model.datatypes.Token;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import model.services.CustomerService;
import utilities.ResponseElement;
import utilities.data.TestData;
import utilities.data.TestDataC;

public class CustomerDetailsSteps extends BaseSteps {
	
	private ResponseElement details;
	private CustomerService customerService;
	
	
	
	
	public CustomerDetailsSteps() {
		super();
	}
	
	@Given("^I have authenticated as a user$")
	public void i_have_authenticated_as_a_user() {
		//requester = TestData.getUser("miketest001");
		//requester = new MobileUser("imranengtest","Imran001");
		
		requester = TestDataC.getCustomerDetail("imranengtest");
		authService = new AuthenticateService();
		requester = authenticateUser(requester);
	
		
	}

	@Given("^I have not authenticated as a user$")
	public void i_have_not_authenticated_as_a_user() throws Throwable {
	    requester = new MobileUser("JohnSmith", "ABC123");
	    authService = new AuthenticateService();
	    requester.Token = new Token();
	}
	
	@Given("^I have authenticated as a smartcard$")
	public void i_have_authenticated_as_a_smartcard() {
	    requester = TestData.getSmartcard();
		authService = new AuthenticateService();
		requester = authenticateUser(requester);
		
		
	}
	
	@When("^I request my details$")
	public void i_request_my_details() throws Throwable {
	    customerService = new CustomerService(requester);
	    response = customerService.requestDetailsResponse(new HashMap<String, ParameterValue>());
	    details = response.getBodyElement();
	    System.out.println("value of response is "+response);
	    System.out.println("value of details is "+details);	    
	    
	}

	@When("^I request customer details$")
	public void i_request_customer_details() throws Throwable {
	    customerService = new CustomerService(requester);
	    response = customerService.requestDetailsResponse(new HashMap<String, ParameterValue>());
	    //System.out.print("value of response2 is "+response);
	}
	
	
/**
@When("^I request my details having bill information$")
public void i_request_my_details_having_bill_information() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
}


@When("^I request my details having no billing details$")
public void i_request_my_details_having_no_billing_details() throws Throwable {
    // Write code here that turns the phrase above into concrete actions
    throw new PendingException();
}

*/

	@Then("^I should see my details$")
	public void i_should_see_my_details() {
		Assert.assertTrue("Required field Address was not included", details.contains("Address"));
	    //Assert.assertEquals("The returned name does not match the customer details", "Mike", details.getValue("FirstName"));
	    Assert.assertEquals("The returned name does not match the customer details", "Imran", details.getValue("FirstName"));		
	}
	
	@Then("^I should see no details$")
	public void i_should_see_no_details() {
		//we have details at this point, not 
	    Assert.assertEquals("We received details for something we weren't authenticated for", 401, response.getResponseCode());
	}
	
	@Then("^I should be denied access$")
	public void i_should_be_denied_access() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Assert.assertEquals("We received details for something we weren't authenticated for", 403, response.getResponseCode());
	}
	
	
	@Then("^My details should match initial details$")
	public void my_details_should_match_initial_details() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		CustomerDetails customer = new CustomerDetails(details);
		
	    Assert.assertEquals("Email check", customer.getEmail(), details.getValue("Email"));
	    Assert.assertEquals("Pin check", customer.getPIN(), details.getValue("PIN"));	    
 	    Assert.assertEquals("Title check", customer.getTitle(), details.getValue("Title"));
 	    Assert.assertEquals("Firstname check", customer.getFirstName(), details.getValue("FirstName"));
 	    Assert.assertEquals("Lastname check", customer.getLastName(), details.getValue("LastName"));
 	    Assert.assertEquals("Telephone check", customer.getTelephone(), details.getLongValue("Telephone"));
 	    Assert.assertEquals("Mobile check", customer.getMobile(), details.getLongValue("Mobile"));
 	    Assert.assertEquals("Address1 check", customer.getAddress1(), details.getValue("Address1"));
 	    Assert.assertEquals("Address2 check", customer.getAddress2(), details.getValue("Address2"));
 	    Assert.assertEquals("Address3 check", customer.getAddress3(), details.getValue("Address3"));
 	    Assert.assertEquals("Suburb check", customer.getSuburb(), details.getValue("Suburb"));
 	    Assert.assertEquals("Postcode check", customer.getPostcode(), details.getIntValue("Postcode"));
 	    Assert.assertEquals("State check", customer.getState(), details.getValue("State"));
 	    Assert.assertEquals("Country check", customer.getCountry(), details.getValue("Country")); 	    
 	    
	    
	    
	}

}
