package steps;

import java.util.HashMap;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.MobileUser;
import model.datatypes.PaymentAccount;
import model.datatypes.Token;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import model.services.CustomerService;
import utilities.ResponseElement;
import utilities.data.TestDataC;

public class CustomerPaymentAccountSteps extends BaseSteps {
	
	private ResponseElement details;
	private CustomerService customerService;
	
	
	

	public CustomerPaymentAccountSteps() {
		super();
	}
	
	@Given("^I have authenticated as a  site user$")
	public void i_have_authenticated_as_a_site_user() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    
		
		
		requester = TestDataC.getCustomerDetail("john1234");
		//requester = TestDataBilling.getCustomerDetail("imranengtest");
		authService = new AuthenticateService();
		requester = authenticateUser(requester);


	}

	
	@Given("^I have not authenticated as a site user$")
	public void i_have_not_authenticated_as_a_site_user() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		
		/**
		   requester = TestData.getUser(true);
		    authService = new AuthenticateService();
		    requester = authenticateUser(requester);
		    
		    CustomerService cs = new CustomerService(requester);
		   */
		
		    requester = new MobileUser("JohnSmith", "ABC123");
		    authService = new AuthenticateService();
		    requester.Token = new Token();

		 
	}

	
	@When("^I request my billing details$")
	public void i_request_my_billing_details() throws Throwable {
	    
		customerService = new CustomerService(requester);
	    response = customerService.requestPaymentDetails(new HashMap<String, ParameterValue>());
	    details = response.getBodyElement();
	    
	    System.out.println("value of response is "+response);
	    System.out.println("value of details is "+details);	    
	    
	}

	

	@Then("^I should see my details if I have provided my billing info$")
	public void i_should_see_my_details_if_I_have_provided_my_billing_info() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		
		PaymentAccount payment = new PaymentAccount(details);
		Assert.assertEquals("Card type check", payment.getBankCardType(), details.getValue("BankCardType"));
		Assert.assertEquals("Expiry month check", payment.getExpiryMonth(), details.getIntValue("ExpiryMonth"));
		Assert.assertEquals("Masked Account Number check", payment.getMaskedAccountNumber(), details.getValue("MaskedAccountNumber"));
		Assert.assertEquals("Account status check", payment.getAccountStatus(), details.getValue("AccountStatus"));
		Assert.assertEquals("Expiry year check", payment.getExpiryYear(), details.getIntValue("ExpiryYear"));
		Assert.assertEquals("Account holder name check", payment.getAccountHolderName(), details.getValue("AccountHolderName"));
		
	
		
		System.out.println("\n\n\n");
		
			
		
		System.out.println("\n\n\n");

		
		System.out.println("\n\n\n");
		
		System.out.println("\n actual card type "+payment.getBankCardType());
		System.out.println("\n actual expiry month "+payment.getExpiryMonth());
		System.out.println("\n actual acc "+payment.getMaskedAccountNumber());
		System.out.println("\n actual status "+payment.getAccountStatus());
		System.out.println("\n actual year "+payment.getExpiryYear());
		System.out.println("\n actual name "+payment.getAccountHolderName());
		
		System.out.println("\n\n\n");
		
	
		
		System.out.println("\n expected card type "+details.getValue("BankCardType"));
		System.out.println("\n expected expiry month "+details.getIntValue("ExpiryMonth"));
		System.out.println("\n expected acc "+details.getValue("MaskedAccountNumber"));
		System.out.println("\n expected status "+details.getValue("AccountStatus"));
		System.out.println("\n expected year "+details.getIntValue("ExpiryYear"));
		System.out.println("\n expected name "+payment.getAccountHolderName());
		
		
	}

	@Then("^I should see blank details if I have not provided my billing info$")
	public void i_should_see_blank_details_if_I_have_not_provided_my_billing_info() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	 //   throw new PendingException();
	}


	@Then("^I should not see deails of billing information$")
	public void i_should_not_see_deails_of_billing_information() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		
		System.out.println("response code now is "+response.getResponseCode());
		
		Assert.assertEquals("We received details for something we weren't authenticated for", 401, response.getResponseCode());
	}


	
}
