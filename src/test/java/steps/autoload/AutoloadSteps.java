package steps.autoload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.MobileUser;
import model.actors.Smartcard;
import model.datatypes.AutoloadAmount;
import model.datatypes.DetailedSmartcard;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import model.services.AutoloadService;
import steps.BaseSteps;
import utilities.ESB;
import utilities.data.TestData;

public class AutoloadSteps extends BaseSteps {

	private AutoloadService autoloadService;
	private ArrayList<AutoloadAmount> amounts;
	private ArrayList<AutoloadAmount> expectedAmounts;
	private ArrayList<AutoloadAmount> adultAmounts;
	private ArrayList<AutoloadAmount> seniorAmounts;
	private ArrayList<AutoloadAmount> childAmounts;
	private ArrayList<AutoloadAmount> concessionAmounts;
	private int autoload;
	
	private void buildAmounts() {
		//Adults!
		adultAmounts = new ArrayList<AutoloadAmount>();
		adultAmounts.add(new AutoloadAmount(0, 4000, "$40 Adult Autoloads"));
		adultAmounts.add(new AutoloadAmount(0, 6000, "$60 Adult Autoloads"));
		adultAmounts.add(new AutoloadAmount(0, 8000, "$80 Adult Autoloads"));
		adultAmounts.add(new AutoloadAmount(0, 10000, "$100 Adult Autoloads"));
		adultAmounts.add(new AutoloadAmount(0, 12000, "$120 Adult Autoloads"));
		
		//Seniors!
		seniorAmounts = new ArrayList<AutoloadAmount>();
		seniorAmounts.add(new AutoloadAmount(0, 1000, "$10 Senior/Pensioner Autoloads"));
		seniorAmounts.add(new AutoloadAmount(0, 2000, "$20 Senior/Pensioner Autoloads"));
		seniorAmounts.add(new AutoloadAmount(0, 3000, "$30 Senior/Pensioner Autoloads"));
		seniorAmounts.add(new AutoloadAmount(0, 4000, "$40 Senior/Pensioner Autoloads"));
		seniorAmounts.add(new AutoloadAmount(0, 5000, "$50 Senior/Pensioner Autoloads"));
		seniorAmounts.add(new AutoloadAmount(0, 6000, "$60 Senior/Pensioner Autoloads"));
		
		//Kids!
		childAmounts = new ArrayList<AutoloadAmount>();
		childAmounts.add(new AutoloadAmount(0, 2000, "$20 Child/Youth Autoload Configuration"));
		childAmounts.add(new AutoloadAmount(0, 3000, "$30 Child/Youth Autoload Configuration"));
		childAmounts.add(new AutoloadAmount(0, 4000, "$40 Child/Youth Autoload Configuration"));
		childAmounts.add(new AutoloadAmount(0, 5000, "$50 Child/Youth Autoload Configuration"));
		childAmounts.add(new AutoloadAmount(0, 6000, "$60 Child/Youth Autoload Configuration"));
		
		//Concession!
		concessionAmounts = new ArrayList<AutoloadAmount>();
		concessionAmounts.add(new AutoloadAmount(0, 2000, "$20 Concession Autoloads"));
		concessionAmounts.add(new AutoloadAmount(0, 3000, "$30 Concession Autoloads"));
		concessionAmounts.add(new AutoloadAmount(0, 4000, "$40 Concession Autoloads"));
		concessionAmounts.add(new AutoloadAmount(0, 5000, "$50 Concession Autoloads"));
		concessionAmounts.add(new AutoloadAmount(0, 6000, "$60 Concession Autoloads"));
	}
	
	private ArrayList<AutoloadAmount> getExpectedAmounts(int fcc) {
		switch(fcc) {
		case 0:
			return adultAmounts;
		case 1:
			return concessionAmounts;
		case 2:
			return seniorAmounts;
		case 3:
			return childAmounts;
		default:
			return adultAmounts;
		}
	}
	
	@Before
	public void autoloadSetup() {
		authService = new AuthenticateService();
		amounts = new ArrayList<AutoloadAmount>();
		expectedAmounts = new ArrayList<AutoloadAmount>();
		buildAmounts();
	}
	@Given("^I have an authenticated Customer with a linked card$")
	public void i_have_an_authenticated_Customer_with_a_linked_card() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser(true);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	    requester = authenticateUser(requester);
	}

	@When("^I look up autoload options for my card$")
	public void i_look_up_autoload_options_for_my_card() throws Throwable {
	    autoloadService = new AutoloadService(requester);
	    amounts = autoloadService.getAutoloadAmounts(requester.getCard().getID());
	}

	@Then("^I should see the autoload options for my card$")
	public void i_should_see_the_autoload_options_for_my_card() throws Throwable {
		expectedAmounts = getExpectedAmounts(0);
		
		for(AutoloadAmount expectedAmount : expectedAmounts) {
			Assert.assertTrue("Our topups did not contain an expected amount", amounts.contains(expectedAmount));
		}
	}

	@When("^I try to look up the autoload options for another card$")
	public void i_try_to_look_up_the_autoload_options_for_another_card() throws Throwable {
		autoloadService = new AutoloadService(requester);
		MobileUser otherCustomer = TestData.getAnyUserBut(requester.getID());
		Smartcard otherCard = TestData.getUsersSmartcard(otherCustomer.getID());
	    response = autoloadService.getAutoloadAmountsResponse(otherCard.getID(), new HashMap<String, ParameterValue>());  
	}

	@Then("^I should be denied because its not my card$")
	public void i_should_be_denied_because_its_not_my_card() throws Throwable {
	    Assert.assertEquals("We weren't denied access to another Customer details", 403, response.getResponseCode());
	    Assert.assertTrue("We didn't receive an expected error message", response.getBodyElement().contains("ErrorType"));
	    Assert.assertFalse("We unexpectedly received autoload information", response.getBodyElement().contains("AutoloadAmounts"));
	}

	@Given("^I have an authenticated Customer$")
	public void i_have_an_authenticated_Customer() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser();
	    requester = authenticateUser(requester);
	}

	@When("^I look up autoload options for a (\\d+)$")
	public void i_look_up_autoload_options_for_a(int fcc) throws Throwable {
	    autoloadService = new AutoloadService(requester);
	    amounts = autoloadService.getAutoloadAmounts(fcc);
	}

	@Then("^I should see autoload options for the (\\d+)$")
	public void i_should_see_autoload_options_for_the(int fcc) throws Throwable {
		expectedAmounts = getExpectedAmounts(fcc);
		
		for(AutoloadAmount expectedAmount : expectedAmounts) {
			Assert.assertTrue("Our topups did not contain an expected amount", amounts.contains(expectedAmount));
		}
	}

	@When("^I try to look up autoload options for an invalid fare category$")
	public void i_try_to_look_up_autoload_options_for_an_invalid_fare_category() throws Throwable {
	    autoloadService = new AutoloadService(requester);
	    response = autoloadService.getAutoloadAmountsResponse(5, new HashMap<String, ParameterValue>());
	}

	@Then("^I should receive an error because the fare category does not exist$")
	public void i_should_receive_an_error_because_the_fare_category_does_not_exist() throws Throwable {
		Assert.assertEquals("We weren't denied access to another Customer details", 400, response.getResponseCode());
		Assert.assertTrue("We didn't receive an expected error message", response.getBodyElement().contains("ErrorType"));
		Assert.assertFalse("We unexpectedly received autoload information", response.getBodyElement().contains("AutoloadAmounts"));
	}

	@Given("^I have an authenticated Customer without autoload set up$")
	public void i_have_an_authenticated_Customer_without_autoload_set_up() throws Throwable {
	    requester = TestData.getUser(true, true);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	    requester = authenticateUser(requester);
	}
	
	@Given("^I have an authenticated Customer with a linked card and billing info$")
	public void i_have_an_authenticated_Customer_with_a_linked_card_and_billing_info() throws Throwable {
	    requester = TestData.getUser(true, true);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	    requester = authenticateUser(requester);
	}

	@When("^I add a new autoload to my card$")
	public void i_add_a_new_autoload_to_my_card() throws Throwable {
	    autoloadService = new AutoloadService(requester);
	    amounts = autoloadService.getAutoloadAmounts(requester.getCard().getID());
	    Random r = new Random();
	    int n = r.nextInt(4);
	    autoload = amounts.get(n).getValue();
	    String id = autoloadService.setAutoloadAmount(requester.getCard().getID(), autoload);
	    Assert.assertEquals("We did not autoload successfully", String.valueOf(requester.getCard().getID()), id);
	}

	@Then("^the autoload details for my card should have been added$")
	public void the_autoload_details_for_my_card_should_have_been_added() throws Throwable {
	    int currentAutoload = ESB.GetAutoloadOnCard(String.valueOf(requester.getCard().getID()));
	    
	    Assert.assertEquals("Our current autoload differs from what we were expecting", currentAutoload, autoload);
	}

	@Given("^I have an authenticated Customer with stored payment details$")
	public void i_have_an_authenticated_Customer_with_stored_payment_details() throws Throwable {
	    requester = TestData.getUser(true, true);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	    requester = authenticateUser(requester);
	}

	@When("^I try to change the autoload on an unlinked card$")
	public void i_try_to_change_the_autoload_on_an_unlinked_card() throws Throwable {
	    autoloadService = new AutoloadService(requester);
	    Smartcard card = TestData.getSmartcard();
	    response = autoloadService.setAutoloadAmountResponse(card.getID(), String.valueOf(4000), new HashMap<String, ParameterValue>());
	}

	@When("^I ask to change the autoload on someone elses card$")
	public void i_ask_to_change_the_autoload_on_someone_elses_card() throws Throwable {
		autoloadService = new AutoloadService(requester);
		MobileUser otherCustomer = TestData.getAnyUserBut(requester.getID());
	    Smartcard card = TestData.getUsersSmartcard(otherCustomer.getID());
	    response = autoloadService.setAutoloadAmountResponse(card.getID(), String.valueOf(4000), new HashMap<String, ParameterValue>());
	}

	@Given("^I have an authenticated unlinked card$")
	public void i_have_an_authenticated_unlinked_card() throws Throwable {
	    requester = TestData.getSmartcard();
	    requester = authenticateUser(requester);
	}

	@When("^I ask to change the autoload on the unlinked card$")
	public void i_ask_to_change_the_autoload_on_the_unlinked_card() throws Throwable {
		 autoloadService = new AutoloadService(requester);
		 response = autoloadService.setAutoloadAmountResponse(String.valueOf(requester.getCard().getID()), "4000", new HashMap<String, ParameterValue>());
	}

	@Then("^I should be denied because Im not a customer$")
	public void i_should_be_denied_because_Im_not_a_customer() throws Throwable {
		Assert.assertEquals("We weren't denied access to another Customer details", 403, response.getResponseCode());
		Assert.assertTrue("We didn't receive an expected error message", response.getBodyElement().contains("ErrorType"));
		Assert.assertFalse("We unexpectedly received autoload information", response.getBodyElement().contains("AutoloadAmounts"));
	}

	@Given("^I have an authenticated Customer without a stored payment method$")
	public void i_have_an_authenticated_Customer_without_a_stored_payment_method() throws Throwable {
	    requester = TestData.getUser(true, false);
	    requester = authenticateUser(requester);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	}

	@When("^I ask to change the autoload on my card$")
	public void i_ask_to_change_the_autoload_on_my_card() throws Throwable {
	    autoloadService = new AutoloadService(requester);
	    response = autoloadService.setAutoloadAmountResponse(String.valueOf(requester.getCard().getID()), "4000", new HashMap<String, ParameterValue>());
	}

	@Then("^I should be denied because theres no stored payment method$")
	public void i_should_be_denied_because_theres_no_stored_payment_method() throws Throwable {
		Assert.assertEquals("We weren't denied access to another Customer details", 425, response.getResponseCode());
		Assert.assertTrue("We didn't receive an expected error message", response.getBodyElement().contains("ErrorType"));
		Assert.assertFalse("We unexpectedly received autoload information", response.getBodyElement().contains("AutoloadAmounts"));	
	}

	@When("^I ask to change the autoload with (\\S+)$")
	public void i_ask_to_change_the_autoload_with(String value) throws Throwable {
	    autoloadService = new AutoloadService(requester);
	    response = autoloadService.setAutoloadAmountResponse(requester.getCard().getID(), Integer.valueOf(value), new HashMap<String, ParameterValue>());
	}

	@Then("^I should be denied because the autoload amount is invalid$")
	public void i_should_be_denied_because_the_autoload_amount_is_invalid() throws Throwable {
	    Assert.assertNotEquals("We unexpectedly changed the autoload with an invalid amount", 200, response.getResponseCode());
	    Assert.assertFalse("We received an unexpected success response", response.getBodyElement().contains("SmartcardId"));
	}
	
	@Given("^I am an authenticated Customer with autoload on a linked card$")
	public void i_am_an_authenticated_Customer_with_autoload_on_a_linked_card() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser(true, true);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	    requester = authenticateUser(requester);
	    
	    autoloadService = new AutoloadService(requester);
	    amounts = autoloadService.getAutoloadAmounts(requester.getCard().getID());
	    Random r = new Random();
	    int n = r.nextInt(4);
	    autoload = amounts.get(n).getValue();
        String id = autoloadService.setAutoloadAmount(requester.getCard().getID(), autoload);
	    Assert.assertEquals("We did not autoload successfully", String.valueOf(requester.getCard().getID()), id);
	    
	}

	@Given("^I am an authenticated Customer trying to set autoload$")
	public void i_am_an_authenticated_Customer_trying_to_set_autoload() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser(true, true);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	    requester = authenticateUser(requester);
	}
	
	@Given("^I am an authenticated Customer trying to annoy another Customer$")
	public void i_am_an_authenticated_Customer_trying_to_annoy_another_Customer() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser(true, true);
	    requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
	    requester = authenticateUser(requester);
	}
	
	@When("^I remove autoload on my card$")
	public void i_remove_autoload_on_my_card() throws Throwable {
	    String id = autoloadService.deleteAutoloadAmount(requester.getCard().getID());
	    Assert.assertEquals("We did not autoload successfully", String.valueOf(requester.getCard().getID()), id);
	}

	@Then("^I should see no autoload details on my card$")
	public void i_should_see_no_autoload_details_on_my_card() throws Throwable {
	    int current = ESB.GetAutoloadOnCard(requester.getCard().getID());
	    Assert.assertEquals("There is an autoload still pending on the card", 0, current);
	}

	@When("^I attempt to remove autoload on another Customers card$")
	public void i_attempt_to_remove_autoload_on_another_Customers_card() throws Throwable {
		autoloadService = new AutoloadService(requester);
		MobileUser otherCustomer = TestData.getAnyUserBut(requester.getID());
		Smartcard otherCard = TestData.getUsersSmartcard(otherCustomer.getID());
	    response = autoloadService.deleteAutoloadAmountResponse(otherCard.getID(), new HashMap<String, ParameterValue>());
	}

	@Then("^I should be denied because I cannot remove autoload on another Customer card$")
	public void i_should_be_denied_because_I_cannot_remove_autoload_on_another_Customer_card() throws Throwable {
	    Assert.assertEquals("We did not receive an error response", 403, response.getResponseCode());
	    Assert.assertFalse("We removed autoload on another Customers card by mistake", response.getBodyElement().contains("SmartcardId"));
	}
}
