package steps.smartcard;

import java.util.HashMap;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.Smartcard;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import model.services.CustomerService;
import model.services.SmartcardDetailsService;
import steps.BaseSteps;
import utilities.ResponseElement;
import utilities.data.TestData;

public class SmartcardDetailsSteps extends BaseSteps {

	private SmartcardDetailsService scDS;
	private String smartcardID;
	private ResponseElement details;
	
	@Given("^I have authenticated as a customer$")
	public void i_have_authenticated_as_a_customer() {
		requester = TestData.getUser("miketest001");
		smartcardID = "3085226900000038";
		authService = new AuthenticateService();
		requester = authenticateUser(requester);
	}

	@When("^I review the details for one of my smartcards$")
	public void i_review_the_details_for_one_of_my_smartcards() {
	    scDS = new SmartcardDetailsService(requester);
	    response = scDS.requestDetailsResponse(smartcardID, requester.Token.accessToken, new HashMap<String, ParameterValue>());
	    details = response.getMessageElement();
	}

	@Then("^I should see the details of that smartcard$")
	public void i_should_see_the_details_of_that_smartcard() {
	    Assert.assertEquals("The returned SmartcardId did not match the requested one", smartcardID, details.getValue("SmartcardId"));
	    Assert.assertTrue("The non-optional values were not returned", scDS.responseContainsRequiredFields(smartcardID));
	}

	@Given("^I have an unregistered smartcard$")
	public void i_have_an_unregistered_smartcard() {
	    requester = TestData.getSmartcard();
	    smartcardID = requester.getID();
		authService = new AuthenticateService();
		requester = authenticateUser(requester);
	}
	
	@Given("^I have authenticated as a customer with a linked smartcard$")
	public void i_have_authenticated_as_a_customer_with_a_linked_smartcard() throws Throwable {
	    
		
		requester = TestData.getUser("miketest001");
	    smartcardID = "3085226900000038";

	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	}

	@Given("^I have authenticated as a customer without a linked smartcard$")
	public void i_have_authenticated_as_a_customer_without_a_linked_smartcard() throws Throwable {
	    
		
		requester = TestData.getUser("imranengtest");
		//requester = TestData.getUser(false);
	    
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	}
	
	@When("^I review the details of the smartcard$")
	public void i_review_the_details_of_the_smartcard() {
	    scDS = new SmartcardDetailsService(requester);
	    response = scDS.requestDetailsResponse(smartcardID, requester.Token.accessToken, new HashMap<String, ParameterValue>());
	    details = response.getBodyElement();
	}

	@Given("^I have a registered smartcard$")
	public void i_have_a_registered_smartcard() {
	    requester = new Smartcard("3085226900000038", "0006");
	}

	@Given("^I have not authenticated$")
	public void i_have_not_authenticated() {
	    authService = new AuthenticateService();
	    requester.Token.accessToken = "";
	    Assert.assertEquals("We have been provided an unexpected Access Token", "", requester.Token.accessToken);
	    //Don't do anything else!
	}

	@Then("^I should not see the details of that smartcard$")
	public void i_should_not_see_the_details_of_that_smartcard() throws Throwable {
	    Assert.assertFalse("We unexpectedly received smartcard details of a card we don't have access to", details.contains("SmartcardId"));
	}
	
	@Then("^I should see no details about a smartcard$")
	public void i_should_see_no_details_about_a_smartcard() throws Throwable {
	    Assert.assertFalse("We unexpectedly received smartcard details when we were expecting none", details.contains("SmartcardId"));
		//Assert.assertTrue("We unexpectedly received smartcard details when we were expecting none", details.isEmpty());
		
		
			
		CustomerService cs = new CustomerService(requester);
		boolean isEmpty = cs.hasLinkedSmartcards();
		
		System.out.println("value of details is now1 "+cs.hasLinkedSmartcards());
		System.out.println("value of details is now2 "+isEmpty);		
		
		Assert.assertFalse("no card exists", cs.hasLinkedSmartcards());
		//System.out.println("value of details is now2 "+cs.getSmartcardDetails());
		//System.out.println("value of details is now "+cs.hasCard("1111222233334444"));
		//System.out.println("value of details is now "+cs.requestDetailsResponse(overloadedParams));
			
		
		//Assert.assertTrue("No card exists", card.getCardTypeCode() < 0);
		
		//System.out.println("value of boolean2 at this point is "+details.isEmpty());
	    //Assert.assertTrue("There were unexpected card details displayed", isEmpty);
	    //Assert.assertTrue("A valid card was unexpectedly returned", card.getID() < 0);
		
		//Assert.assertEquals("Blanks list of cards ",isEmpty , details.isEmpty());
		
		//Assert.assertTrue("No card details", card.getCardTypeCode() < 0);
		
		//System.out.println("details smartcard now are "+details.getValue("smartcardID"));
		
		//Assert.assertTrue("No card detail", +details.getValue("smartcardID") < 0);
	}

	@When("^I review the details for another customer card$")
	public void i_review_the_details_for_another_customer_card() {
	    scDS = new SmartcardDetailsService(requester);
	    smartcardID = "30855200000053";
	    response = scDS.requestDetailsResponse(smartcardID, requester.Token.accessToken, new HashMap<String, ParameterValue>());
	    details = response.getBodyElement();
	    System.out.println(details.toString());
	}

	@When("^I review the details of my non-existent smartcards$")
	public void i_review_the_details_of_my_non_existent_smartcards() throws Throwable {
	    scDS = new SmartcardDetailsService(requester);
	    smartcardID = "";
	    response = scDS.requestDetailsResponse(smartcardID, requester.Token.accessToken, new HashMap<String, ParameterValue>());
	    details = response.getBodyElement();
	    System.out.println(details.toString());
	}
	
	@Then("^I should be denied access to the details of that card$")
	public void i_should_be_denied_access_to_the_details_of_that_card() throws Throwable {
		Assert.assertFalse("We unexpectedly received smartcard details of someone we should not see", details.contains("SmartcardId"));
	}
} 
