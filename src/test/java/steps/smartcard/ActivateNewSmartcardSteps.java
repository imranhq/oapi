package steps.smartcard;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.Smartcard;
import model.services.AuthenticateService;
import model.services.SmartcardActivateService;
import steps.BaseSteps;
import utilities.data.TestData;

/**
 * Steps relating to activating a new Smartcard
 * @author Mike Fleig - mfleig@planittesting.com
 */
public class ActivateNewSmartcardSteps extends BaseSteps {
	 
	private Smartcard smartcard;
	private Smartcard tempcard;
	
	public ActivateNewSmartcardSteps() {
		super();
		authService = new AuthenticateService();
	}
	
	@Given("^I am authenticated as a Customer$")
	public void i_am_authenticated_as_a_Customer() {
	    requester = TestData.getUser("miketest001");
	    requester = authenticateUser(requester);
	}

	@Given("^the Smartcard is related to the registered Customer$")
	public void the_Smartcard_is_related_to_the_registered_Customer() {
	    smartcard = new Smartcard("3085226900000038", "0006");
	}

	@Given("^I am authenticated as the Customer$")
	public void i_am_authenticated_as_the_Customer() {
	    requester = TestData.getUser("miketest001");
	    requester = authenticateUser(requester);
	}

	@Given("^the Smartcard is not related to the registered Customer$")
	public void the_Smartcard_is_not_related_to_the_registered_Customer() throws Throwable {
	    smartcard = new Smartcard("3085226900000053", "0085");
	}

	@Given("^I have an invalid Card with ID (\\S+), SC (\\S+)$")
	public void i_have_an_invalid_Card(String id, String sc) {
	    tempcard = new Smartcard(id, sc);
	    
	}

	@Given("^I have an activated Smartcard$")
	public void i_have_an_activated_Smartcard() throws Throwable {
		i_am_authenticated_as_a_Customer();
		the_Smartcard_is_related_to_the_registered_Customer();
	}

	@Given("^I have a hotlisted Smartcard$")
	public void i_have_a_hotlisted_Smartcard() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
	
	@Given("^I am authenticated as a Smartcard$")
	public void i_am_authenticated_as_a_Smartcard() throws Throwable {
	    requester = TestData.getSmartcard();
	    requester = authenticateUser(requester);
	    smartcard = (Smartcard) requester;
	}
	
	@When("^I activate the Smartcard$")
	public void i_activate_the_Smartcard() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
	
	@When("^I try to activate the Smartcard$")
	public void i_try_to_activate_the_Smartcard() throws Throwable {
	    SmartcardActivateService sas = new SmartcardActivateService(requester);
	    response = sas.activateCardResponse(smartcard, badParams);
	}


	@When("^I try to activate the invalid Card$")
	public void i_try_to_activate_the() {
	    SmartcardActivateService sas = new SmartcardActivateService(requester);
	    response = sas.activateCardResponse(tempcard, badParams);
	}


	@When("^I try to activate using an unrelated CustomerId$")
	public void i_try_to_activate_using_an_unrelated_CustomerId() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
	
	@Then("^I will receive the SmartcardId as a response$")
	public void i_will_receive_the_SmartcardId_as_a_response() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}


	@Then("^I will receive a (\\d+) error$")
	public void i_will_receive_a_error(int errorCode) throws Throwable {
	    Assert.assertEquals("The response code does not match our expectation", response.getResponseCode(), errorCode);
	}

	@Then("^the Smartcard will not be activated$")
	public void the_Smartcard_will_not_be_activated() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I will receive an error$")
	public void i_will_receive_an_error() throws Throwable {
		System.out.println(response.getBodyElement().toString());
	    Assert.assertTrue("We did not receive an expected error", response.getBodyElement().contains("ErrorType"));
	}

}
