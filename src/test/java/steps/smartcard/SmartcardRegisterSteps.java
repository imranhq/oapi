package steps.smartcard;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.MobileUser;
import model.actors.Smartcard;
import model.datatypes.Token;
import model.services.AuthenticateService;
import model.services.SmartcardRegisterService;
import steps.BaseSteps;
import utilities.data.TestData;

public class SmartcardRegisterSteps extends BaseSteps {

	private SmartcardRegisterService scRegService;
	private Smartcard cardToRegister;
	 
	@Given("^I have authenticated as a customer holding an unlinked smartcard$")
	public void i_have_authenticated_as_a_customer_holding_an_unlinked_smartcard() throws Throwable {
	    requester = TestData.getUser();
	    cardToRegister = TestData.getSmartcard();
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	}

	@Given("^I successfully register that unlinked card to me$")
	public void i_successfully_register_that_unlinked_card_to_me() throws Throwable{
	    throw new PendingException();
	    //scRegService = new SmartcardRegisterService(requester);
	    //String id = scRegService.registerCard(cardToRegister);
	    //Assert.assertEquals("We did not successfully register the card", cardToRegister.getID(), id);
	}

	@When("^I try to reregister that smartcard to myself$")
	public void i_try_to_reregister_that_smartcard_to_myself() throws Throwable {
	    throw new PendingException();
	    //scRegService = new SmartcardRegisterService(requester);
	    //response = scRegService.registerCardResponse(cardToRegister);
	}
	

	@Then("^I will get an error trying to reregister my smartcard$")
	public void i_will_get_an_error_trying_to_reregister_my_smartcard() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	    //Assert.assertTrue("We did not receive the expected error", response.getBodyElement().contains("ErrorMessage"));
	}

	@Then("^I can successfully unregister my registered card$")
	public void i_can_successfully_unregister_my_registered_card() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	    //Some SOAP call to unregister a card
	}

	@Given("^I have authenticated as a Customer$")
	public void i_have_authenticated_as_a_Customer() {
	    requester = TestData.getUser();
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	}

	@When("^I register an unlinked card$")
	public void i_register_an_unlinked_card() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I will receive confirmation I've registered my card$")
	public void i_will_receive_confirmation_I_ve_registered_my_card() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I will see the card in my list of linked cards$")
	public void i_will_see_the_card_in_my_list_of_linked_cards() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I have authenticated as a Smartcard$")
	public void i_have_authenticated_as_a_Smartcard() {
	    requester = TestData.getSmartcard();
	    cardToRegister = (Smartcard) requester;
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	}

	@When("^I try to register myself$")
	public void i_try_to_register_myself() {
	    scRegService = new SmartcardRegisterService(requester);
	    response = scRegService.registerCardResponse(cardToRegister);
	}

	@Then("^I will be denied permission to register the card to myself$")
	public void i_will_be_denied_permission_to_register_the_card_to_myself() throws Throwable {
	    Assert.assertEquals("We were not denied permission to register a card", 403, response.getResponseCode());
	    Assert.assertTrue("We did not receive an expected error message", response.getBodyElement().contains("Message"));
	}

	@When("^I try to register another Customers card$")
	public void i_try_to_register_another_Customers_card() {
	    MobileUser otherCustomer = TestData.getUser();
	    Smartcard otherCard = TestData.getUsersSmartcard(otherCustomer.getID());
	    
	    scRegService = new SmartcardRegisterService(requester);
	    response = scRegService.registerCardResponse(otherCard);
	}

	@Then("^I will be denied permission to register that card$")
	public void i_will_be_denied_permission_to_register_that_card() throws Throwable {
		System.out.println(response.getBodyElement().toString());
	    Assert.assertEquals("We were not denied permission to register a card", 425, response.getResponseCode());
	    Assert.assertTrue("We did not receive an expected error message", response.getBodyElement().contains("Message"));
	    Assert.assertEquals("We didn't get the right error message", "CARD ALREADY REGISTERED", response.getBodyElement().getValue("Message"));
	}

	@Given("^I have an entitlement card I wish to register$")
	public void i_have_an_entitlement_card_I_wish_to_register() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("^I try to register the entitlement card$")
	public void i_try_to_register_the_entitlement_card() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("^I will not be able to register the entitlement card$")
	public void i_will_not_be_able_to_register_the_entitlement_card() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I have not authenticated as a Customer holding an unregistered card$")
	public void i_have_not_authenticated_as_a_Customer_holding_an_unregistered_card() {
	    requester = TestData.getUser();
	    cardToRegister = TestData.getSmartcard();
	    authService = new AuthenticateService();
	    requester.Token = new Token();
	}

	@When("^I try to register an unlinked card$")
	public void i_try_to_register_an_unlinked_card() {
	    scRegService = new SmartcardRegisterService(requester);
	    response = scRegService.registerCardResponse(cardToRegister);
	}

	@Then("^I will be denied permission to register my card$")
	public void i_will_be_denied_permission_to_register_my_card() throws Throwable {
	    Assert.assertEquals("We were not denied permission to register a card", 401, response.getResponseCode());
	    Assert.assertTrue("We did not receive an expected error message", response.getBodyElement().contains("error_description"));
	}

}
