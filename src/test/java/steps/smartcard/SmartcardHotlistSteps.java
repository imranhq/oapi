package steps.smartcard;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.Authenticator;
import model.actors.Smartcard;
import model.datatypes.DetailedSmartcard;
import model.datatypes.Token;
import model.services.AuthenticateService;
import model.services.SmartcardHotlistService;
import steps.BaseSteps;
import utilities.ESB;
import utilities.data.TestData;

public class SmartcardHotlistSteps extends BaseSteps{
	
	@Given("^I am an authenticated Customer with an ISSUED card$")
	public void i_am_authenticated_Customer_with_an_issued_card() throws Throwable {
	    authService = new AuthenticateService();
		requester = TestData.getUser(true);
		requester = authenticateUser(requester);
		 
		requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
		ESB.DeHotlistCard(requester.getCard().getID());
	}

	
	@Given("^I am an authenticated unlinked card with ISSUED state$")
	public void i_am_an_authenticated_unlinked_card_with_ISSUED_state() throws Throwable {
	    requester = TestData.getSmartcard();
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	    
		ESB.DeHotlistCard(requester.getCard().getID());
	}

	@When("^I report my card lost/stolen$")
	public void i_report_my_card_lost_stolen() throws Throwable {
	    SmartcardHotlistService scHotlistService = new SmartcardHotlistService(requester);
	    response = scHotlistService.hotlistCardResponse(requester.getCard().getID(), "Card Damaged");
	}

	@Then("^the card will be marked as HOTLISTED$")
	public void the_card_will_be_marked_as_HOTLISTED() throws Throwable {
		System.out.println(response.getBodyElement());
	    Assert.assertEquals("The card was not marked as hotlisted", "HOTLISTED", ESB.GetCardState(requester.getCard().getID()));
		
		ESB.DeHotlistCard(requester.getCard().getID());
	}

	@When("^I report my card lost/stolen with details$")
	public void i_report_my_card_lost_stolen_with_details() throws Throwable {
	    SmartcardHotlistService scHotlistService = new SmartcardHotlistService(requester);
	    response = scHotlistService.hotlistCardResponse(requester.getCard().getID(), "Card Lost");
	}

	@Then("^the additional details will be sent through$")
	public void the_additional_details_will_be_sent_through() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("^I am an unauthenticated Customer with an ISSUED card$")
	public void i_am_an_unauthenticated_Customer_with_an_ISSUED_card() throws Throwable {
	    authService = new AuthenticateService();
		requester = TestData.getUser(true);
		requester.Token = new Token();
		
		requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
		ESB.DeHotlistCard(requester.getCard().getID());
	}

	@When("^I try to report my card lost/stolen$")
	public void i_try_to_report_my_card_lost_stolen() throws Throwable {
	    SmartcardHotlistService scHotlistService = new SmartcardHotlistService(requester);
	    response = scHotlistService.hotlistCardResponse(requester.getCard().getID(), "Card Stolen");
	}

	@Then("^I should be denied because I have not authenticated$")
	public void i_should_be_denied_because_I_have_not_authenticated() throws Throwable {
	    Assert.assertEquals("We allowed to hotlist a card when we weren't authenticated", 401, response.getResponseCode());
	}

	@Given("^I am an authenticated Customer with a HOTLISTED card$")
	public void i_am_an_authenticated_Customer_with_a_HOTLISTED_card() throws Throwable {
	    authService = new AuthenticateService();
		requester = TestData.getUser(true);
		requester = authenticateUser(requester);
		
		requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
		ESB.HotlistCard(requester.getCard().getID());
	}

	@When("^I try to report my card as lost/stolen$")
	public void i_try_to_report_my_card_as_lost_stolen() throws Throwable {
	    SmartcardHotlistService scHotlistService = new SmartcardHotlistService(requester);
	    response = scHotlistService.hotlistCardResponse(requester.getCard().getID(), "Card Lost");
	}

	@Then("^I should be denied because the card is already Hotlisted$")
	public void i_should_be_denied_because_the_card_is_already_Hotlisted() throws Throwable {
	    Assert.assertEquals("We hotlisted a card that was already hotlisted", 425, response.getResponseCode());
	    Assert.assertTrue("We did not receive an expected business error message", response.getBodyElement().contains("ErrorType"));
	}

	@Then("^I should be denied because the reported card does not belong to me$")
	public void i_should_be_denied_because_the_reported_card_does_not_belong_to_me() throws Throwable {
		Assert.assertEquals("We hotlisted a card that was already hotlisted", 403, response.getResponseCode());
	    Assert.assertTrue("We did not receive an expected business error message", response.getBodyElement().contains("ErrorType"));
	}
	
	@Given("^I am an authenticated Customer with an unlinked ISSUED card$")
	public void i_am_an_authenticated_Customer_with_an_unlinked_ISSUED_card() throws Throwable {
	    authService = new AuthenticateService();
		requester = TestData.getSmartcard();
		requester = authenticateUser(requester);
		
		ESB.DeHotlistCard(requester.getCard().getID());
	}

	@Then("^I should be denied because the card does not belong to anyone$")
	public void i_should_be_denied_because_the_card_does_not_belong_to_anyone() throws Throwable {
		Assert.assertEquals("We allowed to hotlist a card when we weren't authenticated", 403, response.getResponseCode());
	}

	@Given("^I am an authenticated Customer$")
	public void i_am_an_authenticated_Customer() throws Throwable {
	    authService = new AuthenticateService();
		requester = TestData.getUser(true);
		requester = authenticateUser(requester);
		
		requester.addCard(new DetailedSmartcard(TestData.getUsersSmartcard(requester.getID())));
		ESB.DeHotlistCard(requester.getCard().getID());
	}

	@When("^I try to report another Customers card as lost/stolen$")
	public void i_try_to_report_another_Customers_card_as_lost_stolen() throws Throwable {
	    Authenticator otherCustomer = TestData.getAnyUserBut(requester.getID());
	    Smartcard otherCard = TestData.getUsersSmartcard(otherCustomer.getID());
	    SmartcardHotlistService scHotlistService = new SmartcardHotlistService(requester);
	    response = scHotlistService.hotlistCardResponse(otherCard.getCard().getID(), "Card Lost");
	}

	@When("^I try to report myself as lost/stolen$")
	public void i_try_to_report_myself_as_lost_stolen() throws Throwable {
	    SmartcardHotlistService scHotlistService = new SmartcardHotlistService(requester);
	    response = scHotlistService.hotlistCardResponse(requester.getCard().getID(), "Card Stolen");
	}

	@Then("^I should be denied because I cannot be marked as lost/stolen$")
	public void i_should_be_denied_because_I_cannot_be_marked_as_lost_stolen() throws Throwable {
		Assert.assertEquals("We allowed to hotlist ourselves when we're not a Customer", 403, response.getResponseCode());
	}

	@Then("^I should be denied because I do not have permission$")
	public void i_should_be_denied_because_I_do_not_have_permission() throws Throwable {
		Assert.assertEquals("We allowed to hotlist a card when we weren't authorized to", 403, response.getResponseCode());
	}
}
