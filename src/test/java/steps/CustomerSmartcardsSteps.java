package steps;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.datatypes.DetailedSmartcard;
import model.datatypes.Token;
import model.services.AuthenticateService;
import model.services.CustomerService;
import utilities.data.TestData;

public class CustomerSmartcardsSteps extends BaseSteps {

	private DetailedSmartcard card;
	
	public CustomerSmartcardsSteps() {
		super();
	}
	
	@Given("^I am an authenticated Customer without linked cards$")
	public void i_am_an_authenticated_Customer_without_linked_cards() {
	    requester = TestData.getUser(false);
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	}
	
	@Given("^I am an authenticated Customer with some linked cards$")
	public void i_am_an_authenticated_Customer_with_some_linked_cards() {
	    requester = TestData.getUser(true);
	    authService = new AuthenticateService();
	    requester = authenticateUser(requester);
	    
	    CustomerService cs = new CustomerService(requester);
	    requester.addCard(cs.getSmartcardDetails());
	}
	
	@Given("^I am an unauthenticated Customer with a linked card$")
	public void i_am_an_unauthenticated_Customer_with_a_linked_card() {
	    requester = TestData.getUser(true);
	    requester.Token = new Token();
	}
	
	@When("^I view my Smartcards$")
	public void i_view_my_Smartcards() {
	    CustomerService cs = new CustomerService(requester);
	    card = cs.getSmartcardDetails();
	}

	@Then("^I should see details of my linked cards$")
	public void i_should_see_details_of_my_linked_cards() {
	    Assert.assertTrue("The card details are not valid", card.getID() > 0);
	}
 
	@Then("^I should receive a blank list instead of card details$")
	public void i_should_receive_a_blank_list_instead_of_card_details() {
		CustomerService cs = new CustomerService(requester);
		
	//	System.out.println("card type is now "+card.getCardTypeCode());
		
		
		Assert.assertTrue("No card exists", card.getCardTypeCode() < 0);
	}


	@Then("^I should be denied access to my smartcards$")
	public void i_should_be_denied_access_to_my_smartcards() {
	    Assert.assertTrue("We weren't denied access to the Smartcard List", card.getID() < 0);
	}
}
