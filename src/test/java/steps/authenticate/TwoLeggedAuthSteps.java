package steps.authenticate;

import java.util.HashMap;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.Smartcard;
import model.response.ParameterType;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import model.services.CustomerService;
import model.services.SmartcardDetailsService;
import steps.BaseSteps;
import utilities.data.TestData;

public class TwoLeggedAuthSteps extends BaseSteps {

	private Smartcard card;
	
	public TwoLeggedAuthSteps() {
		
	}
	 
	@Given("^I am a valid Customer$")
	public void i_am_a_valid_Customer() throws Throwable {
	    authService = new AuthenticateService();
		requester = TestData.getUser();
	    
	}

	@When("^I successfully authenticate using two-legged authentication$")
	public void i_successfully_authenticate_using_two_legged_authentication() throws Throwable {
	    requester.Token = authService.twoLeggedAuth(requester);
	}

	@Then("^I should be able to access my secured resources$")
	public void i_should_be_able_to_access_my_secured_resources() throws Throwable {
		CustomerService cs = new CustomerService(requester);
		response = cs.requestDetailsResponse(new HashMap<String, ParameterValue>());
		Assert.assertTrue("We unexpectedly were denied access", response.getBodyElement().contains("FirstName"));
		Assert.assertEquals("The error code was unexpected", 200, response.getResponseCode());
	}
	
	@Then("^I should be able to access secured resources$")
	public void i_should_be_able_to_access_secured_resources() throws Throwable {
		SmartcardDetailsService sds = new SmartcardDetailsService(requester);
		response = sds.requestDetailsResponse(String.valueOf(requester.getCard().getID()), requester.Token.accessToken, new HashMap<String, ParameterValue>());
		Assert.assertTrue("We unexpectedly were denied access", response.getBodyElement().contains("CardState"));
		Assert.assertEquals("The error code was unexpected", 200, response.getResponseCode());
	}

	@Given("^I am a Customer with an incorrect password$")
	public void i_am_a_Customer_with_an_incorrect_password() throws Throwable {
	    authService = new AuthenticateService();
		requester = TestData.getUser();
		requester.setCode("1");	//Password is impossible on the system
	}

	@When("^I try to authenticate using two-legged authentication$")
	public void i_try_to_authenticate_using_two_legged_authentication() throws Throwable {
	    response = authService.twoLeggedAuthResponse(requester, new HashMap<String, ParameterValue>());
	}

	@Then("^I should be denied trying to access secured resources$")
	public void i_should_be_denied_trying_to_access_secured_resources() throws Throwable {
		Assert.assertFalse("We unexpectedly received a token", response.getBodyElement().contains("access_token"));
	   
		CustomerService cs = new CustomerService(requester);
	    response = cs.requestDetailsResponse(new HashMap<String, ParameterValue>());
	    Assert.assertNotEquals("We did not receive an expected error response", 200, response.getResponseCode());
	}

	@Given("^I am a Customer with an invalid username$")
	public void i_am_a_Customer_with_an_invalid_username() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser();
	    requester.setID("1");	//Username is impossible on system
	}

	@Then("^I should not be granted an access token$")
	public void i_should_not_be_granted_an_access_token() throws Throwable {
	    Assert.assertTrue("We unexpectedly authenticated", response.getBodyElement().contains("error"));
	    Assert.assertEquals("The error code was unexpected", 400, response.getResponseCode());
	    Assert.assertFalse("We unexpectedly received an authentication token", response.getBodyElement().contains("access_token"));
	}

	@Given("^I am an unlinked Smartcard$")
	public void i_am_an_unlinked_Smartcard() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getSmartcard();
	}

	@Given("^I am an unlinked Smartcard with an invalid security number$")
	public void i_am_an_unlinked_Smartcard_with_an_invalid_security_number() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getSmartcard();
	    requester.setCode("99999");
	}

	@Given("^I am an unlinked Smartcard with an invalid Id$")
	public void i_am_an_unlinked_Smartcard_with_an_invalid_Id() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getSmartcard();
	    requester.setID("3085230000000000");
	}

	@Given("^I have valid Customer details and valid unlinked Smartcard details$")
	public void i_have_valid_Customer_details_and_valid_unlinked_Smartcard_details() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser();
	    card = TestData.getSmartcard();
	}

	@When("^I try to authenticate with both credentials at once$")
	public void i_try_to_authenticate_with_both_credentials_at_once() throws Throwable {
	    HashMap<String, ParameterValue> params = new HashMap<String, ParameterValue>();
	    params.put("smartcardId", new ParameterValue(ParameterType.STRING, card.getID()));
	    params.put("smartcardSc", new ParameterValue(ParameterType.STRING, card.getCode()));
	    
	    response = authService.twoLeggedAuthResponse(requester, params);
	}

	@Then("^I cannot access secured resources$")
	public void i_cannot_access_secured_resources() throws Throwable {
	    CustomerService cs = new CustomerService(requester);
	    response = cs.requestDetailsResponse(new HashMap<String, ParameterValue>());
	    System.out.println(response);
	}

	@Given("^I have a valid Customer and 4 digit security code$")
	public void i_have_a_valid_Customer_and_digit_security_code() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser();
	    requester.setCode("0000");
	}

	@When("^I try to authenticate using username and security code$")
	public void i_try_to_authenticate_using_username_and_security_code() throws Throwable {
	    HashMap<String, ParameterValue> params = new HashMap<String, ParameterValue>();
	    params.put("smartcardSc", new ParameterValue(ParameterType.STRING, requester.getCode()));
	    params.put("password", new ParameterValue(ParameterType.STRING, "1"));
	    
	    response = authService.twoLeggedAuthResponse(requester, params);
	}

	@Then("^I should be denied because the credentials are incompatible$")
	public void i_should_be_denied_because_the_credentials_are_incompatible() throws Throwable {
	    Assert.assertEquals("We did not receive the expected response code", 400, response.getResponseCode());
	    Assert.assertTrue("We did not receive an expected error response", response.getBodyElement().contains("error"));
	    Assert.assertFalse("We unexpectedly received a token", response.getBodyElement().contains("access_token"));
	}

	@Given("^I have a valid Customer and unlinked Smartcard$")
	public void i_have_a_valid_Customer_and_unlinked_Smartcard() throws Throwable {
	    authService = new AuthenticateService();
	    requester = TestData.getUser();
	    card = TestData.getSmartcard();
	}

	@When("^I try to authenticate using Smartcard id and password$")
	public void i_try_to_authenticate_using_Smartcard_id_and_password() throws Throwable {
	    HashMap<String, ParameterValue> params = new HashMap<String, ParameterValue>();
	    params.put("smartcardId", new ParameterValue(ParameterType.STRING, card.getID()));
	    params.put("password", new ParameterValue(ParameterType.STRING, requester.getCode()));
	    params.put("username", new ParameterValue(ParameterType.STRING, ""));
	    response = authService.twoLeggedAuthResponse(requester, params);
	}

	@Given("^I am a valid Customer with my login blocked$")
	public void i_am_a_valid_Customer_with_my_login_blocked() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}
}
