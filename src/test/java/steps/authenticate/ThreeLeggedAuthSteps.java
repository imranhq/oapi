package steps.authenticate;

import java.util.HashMap;

import org.junit.Assert;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.MobileUser;
import model.actors.Smartcard;
import model.response.ParameterType;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import steps.BaseSteps;
import utilities.data.TestData;
 
/**
 * Step Definitions centered around the Session layer to authentication
 * Contains step definitions relating to the three legged authentication feature
 * @author Mike Fleig - mfleig@planittesting.com
 */
public class ThreeLeggedAuthSteps extends BaseSteps {
	
	public ThreeLeggedAuthSteps() { super(); }
	
	@Given("^I log in with username (\\S+) and password (\\S+)$")
	public void i_log_in_with_username_and_password(String username, String password) {
		
		requester = new MobileUser(username, password);
		authService = new AuthenticateService();
		
		//Log in
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
		
		authCode = authService.requestAuthorizationCode(requester, sessionID);
		Assert.assertNotEquals("An invalid authorization code was returned", "", authCode);
	}

	@Given("^I log in with a valid smartcard$")
	public void i_log_in_with_a_valid_smartcard() throws Throwable {
	    requester = TestData.getSmartcard();
		authService = new AuthenticateService();
		
		//Log in
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
		
		authCode = authService.requestAuthorizationCode(requester, sessionID);
		Assert.assertNotEquals("An invalid authorization code was returned", "", authCode);
	}
	
	@Given("^I have an valid username and invalid password$")
	public void i_have_an_valid_username_and_invalid_password() throws Throwable {
	    requester = TestData.getUser();
	    requester.setCode("password");	//Not possible accidentally set as this password in system - too weak
		authService = new AuthenticateService();
	   
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
	}

	@Given("^I have a valid smartcard and invalid security code$")
	public void i_have_a_valid_smartcard_and_invalid_security_code() throws Throwable {
	    requester = TestData.getSmartcard();
	    requester.setCode("9999");
		authService = new AuthenticateService();
	    
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
	}

	@Given("^I have an a smartcard belonging to a Customer$")
	public void i_have_an_a_smartcard_belonging_to_a_Customer() throws Throwable {
	    requester = new Smartcard("3085226900000038", "0006");
		authService = new AuthenticateService();
	    
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
	}
	
	@When("^I request a token with a valid authentication code$")
	public void i_request_a_token_with_a_valid_authentication_code() {
		newToken = authService.requestAccessToken(authCode);
		requester.Token = newToken;
	}

	@Then("^I should receive a valid token to access secure resources$")
	public void i_should_receive_a_valid_token_to_access_secure_resources() {
		Assert.assertNotNull("An invalid access_token was provided", requester.Token.accessToken);
	}

	@Given("^I have an invalid client_id$")
	public void i_have_an_invalid_client_id() {
	    requester = new Smartcard("3085226900000046", "0078");
		authService = new AuthenticateService();
		
		badParams = new HashMap<String, ParameterValue>();
		badParams.put("client_id", new ParameterValue(ParameterType.STRING, "ABC!@#"));
		
	}

	@When("^I load request the callback$")
	public void i_load_request_the_callback() {
		response = authService.requestSessionIDResponse(badParams);
	}

	@Then("^I will not receive a session_id$")
	public void i_will_not_receive_a_session_id() {
		Assert.assertEquals(401, response.getResponseCode());
		System.out.println(response.getContentType());
	}

	@Given("^I have an invalid username and password$")
	public void i_have_an_invalid_username_and_password() {
		requester = new MobileUser("John Smith", "ABC123");
		authService = new AuthenticateService();
		
		//Log in
		sessionID = authService.requestSessionID();
	}

	@When("^I try to get an authentication code$")
	public void i_try_to_get_an_authentication_code() {
		authCode = authService.requestAuthorizationCode(requester, sessionID);
	}

	@Then("^I shall receive an error$")
	public void i_shall_receive_an_error() {
		Assert.assertEquals("We unexpectedly received a valid authorization code", "", authCode);
	}

	@When("^I request a token with an invalid authentication code$")
	public void i_request_a_token_with_an_invalid_authentication_code() {
		response = authService.requestAccessTokenResponse("ABC123", new HashMap<String, ParameterValue>());
	}

	@Then("^I should not receive a token$")
	public void i_should_not_receive_a_token() {
		Assert.assertNull("An unexpected access_token was provided", newToken);
		Assert.assertNotEquals("An unexpected valid response was received from the server", 200, response.getResponseCode());
	}

	@When("^I request a token with a different client_id$")
	public void i_request_a_token_with_a_different_client_id() {
		badParams = new HashMap<String, ParameterValue>();
		badParams.put("client_id", new ParameterValue(ParameterType.STRING, "54f0c455-4d80-421f-82ca-9194df24859d"));
		response = authService.requestAccessTokenResponse(authCode, badParams);
	}

	@When("^I request a token with a different redirect_uri$")
	public void i_request_a_token_with_a_different_redirect_uri() {
		badParams = new HashMap<String, ParameterValue>();
		badParams.put("redirect_uri", new ParameterValue(ParameterType.STRING, "http://www.google.com/"));
		response = authService.requestAccessTokenResponse(authCode, badParams);
	}

	
}
