package steps.authenticate;

import java.util.HashMap;

import org.junit.Assert;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import model.actors.MobileUser;
import model.actors.Smartcard;
import model.response.ParameterValue;
import model.services.AuthenticateService;
import model.services.CustomerService;
import model.services.SmartcardDetailsService;
import steps.BaseSteps;

/**
 * The Basic Authentication Steps
 * Contains step definitions relating to the basic authentication feature file
 * @author Mike Fleig - mfleig@planittesting.com
 */
public class BasicAuthSteps extends BaseSteps {

	public BasicAuthSteps() { super(); }
	
	@Before
	public void BeforeScenario(Scenario scenario) {
		System.out.println(String.format("-------------------"));
		System.out.println(String.format("SCENARIO: STARTING %s", scenario.getName()));
	}

	@After
	public void AfterScenario(Scenario scenario) {
		System.out.println(String.format("SCENARIO: %s %s", scenario.getStatus().toUpperCase(), scenario.getName()));
		System.out.println(String.format("-------------------"));
		System.out.println("");
	}
	
	@Given("^I have a user with username (\\S+) and password (\\S+)$")
	public void i_have_a_user_with_username_ABC_and_password(String username, String password) {
	    requester = new MobileUser(username, password);
	}

	@Given("^I have a card that is registered to a user$")
	public void i_have_a_card_that_is_registered_to_a_user() {
	    requester = new Smartcard("3085226900000038", "0006");
	}
	
	@Given("^I have a user with an invalid user name$")
	public void i_have_a_user_with_an_invalid_user_name() {
	    requester = new MobileUser("John Smith", "passw0rd");
	}
	
	@Given("^I have an unregistered card$")
	public void i_have_an_unregistered_card() {
	    requester = new Smartcard("3085226900000046", "0078");
	}
	
	@Given("^I have authenticated with username (\\S+) and password (\\S+)$")
	public void i_have_authenticated_with_username_and_password(String username, String password) {
		requester = new MobileUser(username, password);
		authService = new AuthenticateService();
		
		//Log in
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
		
		authCode = authService.requestAuthorizationCode(requester, sessionID);
		Assert.assertNotEquals("An invalid authorization code was returned", "", authCode);
	    
		newToken = authService.requestAccessToken(authCode);
		requester.Token = newToken;

		//Check we have access before continuing on
	    Assert.assertNotNull("We do not have an access token when authenticating as a user", requester.Token.accessToken);
	    Assert.assertNotNull("We do not have a refresh token when authenticating as a user", requester.Token.refreshToken);
	}
	
	@Given("^I have authenticated with an unregistered smartcard$")
	public void i_have_authenticated_with_an_unregistered_smartcard() {
	    requester = new Smartcard("3085226900000046", "0078");
		authService = new AuthenticateService();
		
		//Log in
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
		
		authCode = authService.requestAuthorizationCode(requester, sessionID);
		Assert.assertNotEquals("An invalid authorization code was returned", "", authCode);

		newToken = authService.requestAccessToken(authCode);
		requester.Token = newToken;
		
		//Check we have access before continuing on
	    Assert.assertNotNull("We do not have an access token when authenticating as a user", requester.Token.accessToken);
	    Assert.assertNotNull("We do not have a refresh token when authenticating as a user", requester.Token.refreshToken);
	}
	
	@When("^I authenticate$")
	public void i_authenticate() {	    
	    authService = new AuthenticateService();
	    
		//Log in
		sessionID = authService.requestSessionID();
		Assert.assertNotEquals("An invalid session ID was returned", "", sessionID);
		
		authCode = authService.requestAuthorizationCode(requester, sessionID);
		Assert.assertNotEquals("An invalid authorization code was returned", "", authCode);
		
		newToken = authService.requestAccessToken(authCode);
		requester.Token = newToken;
		//Check we have access before continuing on
	    Assert.assertNotNull("We do not have an access token when authenticating as a user", requester.Token.accessToken);
	    Assert.assertNotNull("We do not have a refresh token when authenticating as a user", requester.Token.refreshToken);
	}

	@When("^I revoke my refresh token$")
	public void i_revoke_my_refresh_token() {
	    boolean response = authService.revokeToken(requester.Token.refreshToken, "refresh_token");
	    Assert.assertTrue("An error occurred while revoking the refresh token", response);
	}
	
	@When("^I revoke my access token$")
	public void i_revoke_my_access_token() {
	    boolean response = authService.revokeToken(requester.Token.accessToken, "access_token");
	    Assert.assertTrue("An error occurred while revoking the access token", response);
	}
	
	@When("^I refresh my token with an invalid refresh_token$")
	public void i_refresh_my_token_with_an_invalid_refresh_token() {
	    response = authService.refreshTokenResponse("XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX", new HashMap<String, ParameterValue>());
	}
	
	@When("^I refresh my token with a valid refresh_token$")
	public void i_refresh_my_token_with_a_valid_refresh_token() {
	    newToken = authService.refreshToken(requester.Token.refreshToken);
	}
	
	@Then("^I should receive an access token and a refresh token$")
	public void i_should_receive_an_access_token_and_a_refresh_token() {
	    Assert.assertNotNull("We do not have an access token when authenticating as a user", requester.Token.accessToken);
	    Assert.assertNotNull("We do not have a refresh token when authenticating as a user", requester.Token.refreshToken);
	}

	@Then("^I cannot refresh my access with my refresh token$")
	public void i_cannot_refresh_my_access_with_my_refresh_token() {
	    boolean response = authService.revokeToken(requester.Token.refreshToken, "refresh_token");
	    Assert.assertFalse("The current refresh_token is still valid", response);
	}

	@Then("^I cannot access my secure resources$")
	public void i_cannot_access_secure_resources() throws Throwable {
		CustomerService cs = new CustomerService(requester);
		response = cs.requestDetailsResponse(new HashMap<String, ParameterValue>());
		Assert.assertFalse("We unexpectedly were denied access", response.getBodyElement().contains("FirstName"));
		Assert.assertNotEquals("The error code was unexpected", 200, response.getResponseCode());
	}
	
	@Then("^I cannot access the secure resources$")
	public void i_cannot_access_the_secure_resources() {
		SmartcardDetailsService sds = new SmartcardDetailsService(requester);
		response = sds.requestDetailsResponse(String.valueOf(requester.getCard().getID()), requester.Token.accessToken, new HashMap<String, ParameterValue>());
		Assert.assertFalse("We unexpectedly were denied access", response.getBodyElement().contains("CardState"));
		Assert.assertNotEquals("The error code was unexpected", 200, response.getResponseCode());
	}

	@Then("^my current token is still valid$")
	public void my_current_token_is_still_valid() {
	    boolean response = authService.revokeToken(requester.Token.refreshToken, "refresh_token");
	    Assert.assertTrue("The current refresh_token is invalid", response);
	}

	@Then("^I will receive a new valid token$")
	public void i_will_receive_a_new_valid_token() {
		Assert.assertNotEquals("The new access_token is the same as the old one", requester.Token.accessToken, newToken.accessToken);
		Assert.assertNotEquals("The new refresh_token is the same as the old one", requester.Token.refreshToken, newToken.refreshToken);
	    
		boolean response = authService.revokeToken(newToken.refreshToken, "refresh_token");
	    Assert.assertTrue("The new refresh_token is invalid", response);
	}

	@Then("^my old token is invalid$")
	public void my_old_token_is_invalid() {
	    boolean response = authService.revokeToken(requester.Token.refreshToken, "refresh_token");
	    Assert.assertFalse("The old refresh_token is still valid", response);
	}
	
	@Then("^I will not receive a new token$")
	public void i_will_not_receive_a_new_token() {
		Assert.assertEquals("An expected error was not returned", 400, response.getResponseCode());
	}
}
