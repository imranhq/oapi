package utilities;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;

import org.w3c.dom.Document;

public class SOAPDriver {

	public static Response request(String urlStr, String qname, String soapAction, String version, String identVersion, HashMap<String, String> parameters) {
		try {
			URL url = new URL(urlStr);
			System.out.println(String.format("SOAP REQUEST: %s", urlStr));
			//System.out.println(String.format("SOAP ACTION: %s", soapAction));
			//System.out.println(String.format("SOAP QNAME: %s", qname));
			System.out.println(String.format("Parameters: %s", parameters.toString()));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			//MessageFactory is used to build message
			SOAPMessage message = MessageFactory.newInstance().createMessage();
			
			SOAPPart soapPart = message.getSOAPPart();
			SOAPEnvelope envelope = soapPart.getEnvelope();
			envelope.addNamespaceDeclaration("soapenv", "http://schemas.xmlsoap.org/soap/envelope/");
	        envelope.addNamespaceDeclaration(version, soapAction);
	        envelope.addNamespaceDeclaration(identVersion, "http://css.cubic.com/ws/identifiers/v1/");
			//For SOAP 1.2 -> This isn't necessary if you use MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL)
	        //envelope.addNamespaceDeclaration("soap12", "http://www.w3.org/2003/05/soap-envelope");
	        envelope.setPrefix("soapenv");
	        envelope.removeNamespaceDeclaration("SOAP-ENV");
	        
	        message.getSOAPHeader().setPrefix("soapenv");
	        message.getSOAPBody().setPrefix("soapenv");
	        
	        SOAPBody body = envelope.getBody();
	        
	        SOAPElement main = body.addChildElement(envelope.createQName(qname, version));
			main = addCommonDetails(envelope, main, version, identVersion);
			main = addContent(envelope, main, parameters, version);
			message.saveChanges();
			
			int len = 0;
			String strMsg = "";
			byte[] b = null;
			
			try {
				ByteArrayOutputStream outStream = new ByteArrayOutputStream();
				message.writeTo(outStream);
				b = outStream.toByteArray();
				strMsg = new String(outStream.toByteArray());
				len = strMsg.length();
				System.out.println(strMsg);
				outStream.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			conn.setRequestProperty("Content-Type", "text/xml; charset=UTF-8");
			conn.setRequestProperty("SOAPAction", "\"" + soapAction + qname + "\"");
			conn.setRequestProperty("Content-Length", String.valueOf(len));
			
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);
			conn.setDoInput(true);
			
			OutputStream connOutStream = conn.getOutputStream();
			connOutStream.write(b);
			connOutStream.close();
			
			Response response = checkResponse(conn);
			return response;
			
		} catch (SOAPException soapEx) {
			System.out.println(soapEx.getMessage());
			System.out.println(soapEx.getCause());
			soapEx.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		return null;
	}

	public Document parseResponse(String response, String format) {
		//This is how SOAP parses the response String
		try {
			InputStream is = new ByteArrayInputStream(response.getBytes());
			SOAPMessage request = MessageFactory.newInstance().createMessage(null, is);
			
			return parseResponse(request);
		} catch (SOAPException soapEx) {
			soapEx.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		return null;

	}
	
	//blatantly stolen from the public at http://java-codesnippets.blogspot.com.au/2013/07/convert-soap-message-to-dom-document.html
	public Document parseResponse(SOAPMessage response) {
		try {
		    Source src = response.getSOAPPart().getContent();  
		    TransformerFactory tf = TransformerFactory.newInstance();  
		    Transformer transformer = tf.newTransformer();  
		    DOMResult result = new DOMResult();  
		    transformer.transform(src, result);  
		    return (Document)result.getNode();
		} catch(SOAPException soapex) {
			soapex.printStackTrace();
		} catch(TransformerException tex) {
			tex.printStackTrace();
		}
		
		return null;
	}

	private static SOAPElement addContent(SOAPEnvelope envelope, SOAPElement parent, HashMap<String, String> parameters, String version) {
		try {
			if(parameters != null && parameters.size() > 0) {
	
				for(String key : parameters.keySet()) {
					QName content = envelope.createQName(key, version);
					parent.addChildElement(content).setValue(parameters.get(key));
				}
			}

			return parent;
		} catch(SOAPException soapEx) {
			soapEx.printStackTrace();
		}
		
		return null;
	}
	
	private static Response checkResponse(HttpURLConnection conn) {
		String errorBody = "";
		String message = "";
		String messageBody = "";
		int code = 999;
		HashMap<String, String> respHeaders = new HashMap<String, String>();

		try {
			System.out.println(String.format("RESPONSE (%s): %s", conn.getResponseCode(), conn.getResponseMessage()));
			code = conn.getResponseCode();
			message = conn.getResponseMessage();
			respHeaders = getHeaders(conn);
		
			// Check the error stream
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getErrorStream())); 
			StringBuilder sb = new StringBuilder(); 
			String line; 
			while ((line = rd.readLine()) != null) { 
				sb.append(line); 
			} 
			rd.close(); 
			errorBody = sb.toString();
		} catch (NullPointerException npe) {
			errorBody = "";
		} catch (IOException ioe) {
			errorBody = "";
		}
		
		try {
			// Check the input stream
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			StringBuilder sb = new StringBuilder(); 
			String line; 
			
			while ((line = rd.readLine()) != null) { 
				sb.append(line); 
			} 
			rd.close(); 
			conn.disconnect();
			
			messageBody = sb.toString();
		} catch(NullPointerException npe) {
			messageBody = "";
		} catch(IOException ioe) {
			messageBody = "";
		}

		return new Response(code, message, messageBody, errorBody, respHeaders);
	}
	
	private static HashMap<String, String> getHeaders(HttpURLConnection conn) {
		HashMap<String, String> headers = new HashMap<String, String>();
		
		for(String key : conn.getHeaderFields().keySet()) {
			headers.put(key, conn.getHeaderField(key));
		}
		
		return headers;
	}
	
	private static SOAPElement addCommonDetails(SOAPEnvelope envelope, SOAPElement main, String version, String identVersion) {

		try {
			QName cdetails = envelope.createQName("CommonDetails", version);
			QName cci = envelope.createQName("CubicCorrelationIdentifier", identVersion);
			QName si = envelope.createQName("SourceIdentifier", identVersion);
			SOAPElement cd = main.addChildElement(cdetails);
			cd.addChildElement(cci).setValue("1");
			cd.addChildElement(si).setValue("AUTOMATION");
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return main;
	}
}
