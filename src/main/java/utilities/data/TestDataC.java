package utilities.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.actors.MobileUser;
import model.datatypes.CustomerDetails;

public final class TestDataC {
	
	private static final File fileLoc = new File("src/main/resources/testdata.xlsx");
	
	public static CustomerDetails getCustomer() {
		//Go to the datasheet
		Workbook wb = TestDataC.getWorkbook(fileLoc);
		
		//Look up any row that has a Smartcard Id
		
		//Row row = TestData.findRow(wb.getSheetAt(0), "smartcardId");
		
		Row row = TestDataC.findRow(wb.getSheet("CustomerDetails"), "username");
		
		//Return that row as a Smartcard object
		
		return TestDataC.parseRowToCustomer(wb.getSheet("CustomerDetails"), row);
	}
	
	public static MobileUser getCustomerDetails() {
		//Go to the datasheet
		Workbook wb = TestDataC.getWorkbook(fileLoc);
		
		//Look up any row with a username + haslinkedcards = true
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put("username", "*");
		//criteria.put("hasLinkedCards", String.valueOf(hasLinkedCards));
		//criteria.put("hasBillingInfo", String.valueOf(hasBillingInfo));
		
		Row row = TestDataC.findRow(wb.getSheet("CustomerDetails"), criteria);
		
		//Return that row as a MobileUser object
		return TestDataC.parseRowToCustomerDetails(wb.getSheet("CustomerDetails"), row);
	}
	
	public static MobileUser getCustomerDetail() {
		//Go to the datasheetl
		Workbook wb = TestDataC.getWorkbook(fileLoc);
		
		//Get all the rows with a username
		Row row = TestDataC.findRow(wb.getSheet("CustomerDetails"), "username");

		//Return a random row as a MobileUser object
		return TestDataC.parseRowToCustomerDetails(wb.getSheet("CustomerDetails"), row);
	}
	
	public static MobileUser getCustomerDetail(String username) {
		Workbook wb = TestDataC.getWorkbook(fileLoc);
		
		Row row = TestDataC.findRow(wb.getSheet("CustomerDetails"), "username", username);
		
		return TestDataC.parseRowToCustomerDetails(wb.getSheet("CustomerDetails"), row);
	}
	
	
	/**
	public static MobileUser getCustomerDetails() {
		//Go to the datasheet
		Workbook wb = TestDataC.getWorkbook(fileLoc);
		
		//Look up any row with a username + haslinkedcards = true
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put("username", "*");
		//criteria.put("hasLinkedCards", String.valueOf(hasLinkedCards));
		//Row row = TestDataC.findRow(wb.getSheet("CustomerDetails"), criteria);
		
		//Return that row as a MobileUser object
		return TestDataC.parseRowToMobileUser(wb.getSheet("CustomerDetails"), row);
	}
	*/
	
	private static Workbook getWorkbook(File excelFilePath) {		
		try {
			FileInputStream inputStream = new FileInputStream(excelFilePath);
		    Workbook workbook = null;
		 
		    if (excelFilePath.getName().endsWith("xlsx")) {
		        workbook = new XSSFWorkbook(inputStream);
		    } else if (excelFilePath.getName().endsWith("xls")) {
		        workbook = new HSSFWorkbook(inputStream);
		    } else {
		        System.out.println(String.format("ERROR: File %s is not a valid .xls or .xlsx file.", excelFilePath));
		        inputStream.close();
		        return null;
		    }
		    
		    inputStream.close();
		    return workbook;
	    } catch (IOException ioe) { 
	    	System.out.println(String.format("Cannot load Workbook at %s", fileLoc));
	    }

		return null;
	}
	
	private static Row findRow(Sheet sheet, String col) {
		final int firstRow = sheet.getFirstRowNum();		
		
		int i = findCol(sheet.getRow(firstRow), col);
		
		ArrayList<Row> rows = new ArrayList<Row>();
		
		for(Row row: sheet) {
			if(row.getRowNum() != 0) {
				if(TestDataC.getCellValue(row.getCell(i)) != null && !String.valueOf(TestDataC.getCellValue(row.getCell(i))).equals("")) {
					rows.add(row);
				}
			}
		}
		
		Random r = new Random();
		if(rows.size() > 1) {
			rows.get(r.nextInt(rows.size() - 1));
		}
		
		if(rows.size() == 0) {
			System.out.println("WARNING: No Test Data found matching criteria");
			return null;
		}
		
		return rows.get(0);
	}
	
	private static Row findRow(Sheet sheet, String col, String value) {
		final int firstRow = sheet.getFirstRowNum();		
		
		int i = findCol(sheet.getRow(firstRow), col);
		
		ArrayList<Row> rows = new ArrayList<Row>();
		
		for(Row row: sheet) {
			if(row.getRowNum() != 0) {
				if(TestDataC.getCellValue(row.getCell(i)) != null && String.valueOf(TestDataC.getCellValue(row.getCell(i))).equals(value)) {
					rows.add(row);
				}
			}
		}
		
		Random r = new Random();
		if(rows.size() > 1) {
			rows.get(r.nextInt(rows.size() - 1));
		}
		
		if(rows.size() == 0) {
			System.out.println("WARNING: No Test Data found matching criteria");
			return null;
		}
		
		return rows.get(0);
	}
	
	private static Row findRow(Sheet sheet, HashMap<String, String> criteria) {
		ArrayList<Row> rows = new ArrayList<Row>();
		final int firstRow = sheet.getFirstRowNum();		
		

		for(Row row: sheet) {
			boolean valid = true;
			if(row.getRowNum() != firstRow) {
				for(String criterion: criteria.keySet()) {
					int i = findCol(sheet.getRow(firstRow), criterion);
					if(TestDataC.getCellValue(row.getCell(i)) == null || (!String.valueOf(TestDataC.getCellValue(row.getCell(i))).equals(criteria.get(criterion)) && !criteria.get(criterion).equals("*") )) {
						valid = false;
					}
				}
			} else {
				valid = false;
			}
			
			if(valid) {
				rows.add(row);
			}
		}
		
		Random r = new Random();
		if(rows.size() > 1) {
			rows.get(r.nextInt(rows.size() - 1));
		}
		
		if(rows.size() == 0) {
			System.out.println("WARNING: No Test Data found matching criteria");
			return null;
		}
		
		return rows.get(0);
	}
	
	private static Object getCellValue(Cell cell) {
		
		if(cell != null) {
		    switch (cell.getCellType()) {
		    case Cell.CELL_TYPE_STRING:
		        return cell.getStringCellValue();
		 
		    case Cell.CELL_TYPE_BOOLEAN:
		        return cell.getBooleanCellValue();
		 
		    case Cell.CELL_TYPE_NUMERIC:
		        return cell.getNumericCellValue();
		    }
		}
	 
	    return null;
	}
	
	private static int findCol(Row row, String value) {
		final short minCol = row.getFirstCellNum();
		
		int i = minCol;
		Iterator<Cell> headerIterator = row.iterator();
		
		while(headerIterator.hasNext()) {
			Cell cell = headerIterator.next();
			if(TestDataC.getCellValue(row.getCell(i)) != null && String.valueOf(TestDataC.getCellValue(cell)).equalsIgnoreCase(value)) {
				return i;
			}
			
			i++;
		}
		
		return -1;
	}
	
	private static CustomerDetails parseRowToCustomer(Sheet sheet, Row row) {
		int uCol = TestDataC.findCol(sheet.getRow(sheet.getFirstRowNum()), "username");
		int pCol = TestDataC.findCol(sheet.getRow(sheet.getFirstRowNum()), "password");

		String Email = String.valueOf(TestDataC.getCellValue(row.getCell(uCol)));
		String PIN = String.valueOf(TestDataC.getCellValue(row.getCell(pCol)));
		
		return new CustomerDetails(Email, PIN);
	}
	
	private static MobileUser parseRowToCustomerDetails(Sheet sheet, Row row) {
		int userCol = TestDataC.findCol(sheet.getRow(sheet.getFirstRowNum()), "username");
		int passCol = TestDataC.findCol(sheet.getRow(sheet.getFirstRowNum()), "password");
		
		String user = String.valueOf(TestDataC.getCellValue(row.getCell(userCol)));
		String pass = String.valueOf(TestDataC.getCellValue(row.getCell(passCol)));
		
		return new MobileUser(user, pass);
	}
}