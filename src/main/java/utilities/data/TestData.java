package utilities.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import model.actors.MobileUser;
import model.actors.Smartcard;

public class TestData {
	
	private static final File fileLoc = new File("src/main/resources/testdata.xlsx");
	
	public static Smartcard getSmartcard() {
		//Go to the datasheet
		Workbook wb = TestData.getWorkbook(fileLoc);
		
		//Look up any row that has a Smartcard Id
		
		//Row row = TestData.findRow(wb.getSheetAt(0), "smartcardId");
		
		Row row = TestData.findRow(wb.getSheet("Smartcards"), "smartcardId");
		
		//Return that row as a Smartcard object
		return TestData.parseRowToSmartcard(wb.getSheet("Smartcards"), row);
	}
	
	public static Smartcard getUsersSmartcard(String username) {
		//Go to the datasheet
		Workbook wb = TestData.getWorkbook(fileLoc);
		
		//Look up any row with a username + haslinkedcards = true
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put("username", username);
		criteria.put("smartcardId", "*");
		
		Row row = TestData.findRow(wb.getSheet("Users"), criteria);
		
		//Return that row as a MobileUser object
		return TestData.parseRowToSmartcard(wb.getSheet("Users"), row);
	}
	 
	public static MobileUser getUser(boolean hasLinkedCards, boolean hasBillingInfo) {
		//Go to the datasheet
		Workbook wb = TestData.getWorkbook(fileLoc);
		
		//Look up any row with a username + haslinkedcards = true
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put("username", "*");
		
		if(hasLinkedCards) {
			criteria.put("smartcardId", "*");
		} else {
			criteria.put("smartcardId", "-");
		}
		
		criteria.put("hasBillingInfo", String.valueOf(hasBillingInfo));

		Row row = TestData.findRow(wb.getSheet("Users"), criteria);
		
		//Return that row as a MobileUser object
		return TestData.parseRowToMobileUser(wb.getSheet("Users"), row);
	}
	
	public static MobileUser getUser() {
		//Go to the datasheet
		Workbook wb = TestData.getWorkbook(fileLoc);
		
		//Get all the rows with a username
		Row row = TestData.findRow(wb.getSheet("Users"), "username");

		//Return a random row as a MobileUser object
		return TestData.parseRowToMobileUser(wb.getSheet("Users"), row);
	}
	
	public static MobileUser getAnyUserBut(String username) {
		Workbook wb = TestData.getWorkbook(fileLoc);
		final int firstRow = wb.getSheet("Users").getFirstRowNum();
		Row row = null;
		boolean valid = false;
		int i = findCol(wb.getSheet("Users").getRow(firstRow), "smartcardId");
		int x = findCol(wb.getSheet("Users").getRow(firstRow), "username");
		int safety = 0;
		int count = 1;

		row = wb.getSheet("Users").getRow(count);
		while(valid == false && safety < 50) {

			row = wb.getSheet("Users").getRow(count);
			if(TestData.getCellValue(row.getCell(i)) == null || String.valueOf(TestData.getCellValue(row.getCell(i))).equals("") || String.valueOf(TestData.getCellValue(row.getCell(x))).equals(username)) {
				valid = false;
			} else {
				valid = true;
			}
			safety++;
			count++;
		}

		return TestData.parseRowToMobileUser(wb.getSheet("Users"), row);
	}
	
	public static MobileUser getUser(String username) {
		Workbook wb = TestData.getWorkbook(fileLoc);
		
		Row row = TestData.findRow(wb.getSheet("Users"), "username", username);
		
		return TestData.parseRowToMobileUser(wb.getSheet("Users"), row);
	}
	
	public static MobileUser getUser(boolean hasLinkedCards) {
		//Go to the datasheet
		Workbook wb = TestData.getWorkbook(fileLoc);
		
		//Look up any row with a username + haslinkedcards = true
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put("username", "*");
		if(hasLinkedCards) {
			criteria.put("smartcardId", "*");
		} else {
			criteria.put("smartcardId", "-");
		}
		
		Row row = TestData.findRow(wb.getSheetAt(0), criteria);
		
		//Return that row as a MobileUser object
		return TestData.parseRowToMobileUser(wb.getSheetAt(0), row);
	}
	
	public static MobileUser getUser(CardState state, boolean hasBillingInfo) {
		//Go to the datasheet
		Workbook wb = TestData.getWorkbook(fileLoc);
		
		//Look up any row with a username + haslinkedcards = true
		HashMap<String, String> criteria = new HashMap<String, String>();
		criteria.put("username", "*");
		criteria.put("smartcardId", "*");
		criteria.put("hasBillingInfo", String.valueOf(hasBillingInfo));
		
		Row row = TestData.findRow(wb.getSheet("Users"), criteria);
		
		//Return that row as a MobileUser object
		return TestData.parseRowToMobileUser(wb.getSheet("Users"), row);
	}
	
	private static Workbook getWorkbook(File excelFilePath) {		
		try {
			FileInputStream inputStream = new FileInputStream(excelFilePath);
		    Workbook workbook = null;
		 
		    if (excelFilePath.getName().endsWith("xlsx")) {
		        workbook = new XSSFWorkbook(inputStream);
		    } else if (excelFilePath.getName().endsWith("xls")) {
		        workbook = new HSSFWorkbook(inputStream);
		    } else {
		        System.out.println(String.format("ERROR: File %s is not a valid .xls or .xlsx file.", excelFilePath));
		        inputStream.close();
		        return null;
		    }
		    
		    inputStream.close();
		    return workbook;
	    } catch (IOException ioe) { 
	    	System.out.println(String.format("Cannot load Workbook at %s", fileLoc));
	    }

		return null;
	}
	
	private static Row findRow(Sheet sheet, String col) {
		final int firstRow = sheet.getFirstRowNum();		
		
		int i = findCol(sheet.getRow(firstRow), col);
		
		ArrayList<Row> rows = new ArrayList<Row>();
		
		for(Row row: sheet) {
			if(row.getRowNum() != 0) {
				if(TestData.getCellValue(row.getCell(i)) != null && !String.valueOf(TestData.getCellValue(row.getCell(i))).equals("")) {
					rows.add(row);
				}
			}
		}
		
		Random r = new Random();
		if(rows.size() > 1) {
			rows.get(r.nextInt(rows.size() - 1));
		}
		
		if(rows.size() == 0) {
			System.out.println("WARNING: No Test Data found matching criteria");
			return null;
		}
		
		return rows.get(0);
	}
	
	private static Row findRow(Sheet sheet, String col, String value) {
		final int firstRow = sheet.getFirstRowNum();		
		
		int i = findCol(sheet.getRow(firstRow), col);
		
		ArrayList<Row> rows = new ArrayList<Row>();
		
		for(Row row: sheet) {
			if(row.getRowNum() != 0) {
				if(TestData.getCellValue(row.getCell(i)) != null && String.valueOf(TestData.getCellValue(row.getCell(i))).equals(value)) {
					rows.add(row);
				}
			}
		}
		
		Random r = new Random();
		if(rows.size() > 1) {
			rows.get(r.nextInt(rows.size() - 1));
		}
		
		if(rows.size() == 0) {
			System.out.println("WARNING: No Test Data found matching criteria");
			return null;
		}
		
		return rows.get(0);
	}
	
	private static Row findAnyRowBut(Sheet sheet, String col, String value) {
		final int firstRow = sheet.getFirstRowNum();		
		
		int i = findCol(sheet.getRow(firstRow), col);
		
		ArrayList<Row> rows = new ArrayList<Row>();
		
		for(Row row: sheet) {
			if(row.getRowNum() != 0) {
				if(TestData.getCellValue(row.getCell(i)) != null && !String.valueOf(TestData.getCellValue(row.getCell(i))).equals(value)) {
					rows.add(row);
				}
			}
		}
		
		Random r = new Random();
		if(rows.size() > 1) {
			rows.get(r.nextInt(rows.size() - 1));
		}
		
		if(rows.size() == 0) {
			System.out.println("WARNING: No Test Data found matching criteria");
			return null;
		}
		
		return rows.get(0);
	}
	
	private static Row findRow(Sheet sheet, HashMap<String, String> criteria) {
		ArrayList<Row> rows = new ArrayList<Row>();
		final int firstRow = sheet.getFirstRowNum();		

		for(Row row: sheet) {
			boolean valid = true;
			if(row.getRowNum() != firstRow) {
				for(String criterion: criteria.keySet()) {
					int i = findCol(sheet.getRow(firstRow), criterion);
					if(criteria.get(criterion).equals("*")) {
						if(TestData.getCellValue(row.getCell(i)) == null || String.valueOf(TestData.getCellValue(row.getCell(i))).equals("")) {
							valid = false;
						}
					} else if(criteria.get(criterion).equals("-")) {
						if(TestData.getCellValue(row.getCell(i)) != null) {
							valid = false;
						}
					} else {
						if(TestData.getCellValue(row.getCell(i)) == null || !(String.valueOf(TestData.getCellValue(row.getCell(i))).equals(criteria.get(criterion)))) {
							valid = false;
						}
					}
				}
			} else {
				valid = false;
			}
			
			if(valid) {
				rows.add(row);
			}
		}
		
		Random r = new Random();
		if(rows.size() > 1) {
			rows.get(r.nextInt(rows.size() - 1));
		}
		
		if(rows.size() == 0) {
			System.out.println("WARNING: No Test Data found matching criteria");
			return null;
		}
		
		return rows.get(0);
	}
	
	private static Object getCellValue(Cell cell) {
		
		if(cell != null) {
		    switch (cell.getCellType()) {
		    case Cell.CELL_TYPE_STRING:
		        return cell.getStringCellValue();
		 
		    case Cell.CELL_TYPE_BOOLEAN:
		        return cell.getBooleanCellValue();
		 
		    case Cell.CELL_TYPE_NUMERIC:
		        return cell.getNumericCellValue();
		    }
		}
	 
	    return null;
	}
	
	private static int findCol(Row row, String value) {
		final short minCol = row.getFirstCellNum();
		
		int i = minCol;
		Iterator<Cell> headerIterator = row.iterator();
		
		while(headerIterator.hasNext()) {
			Cell cell = headerIterator.next();
			if(TestData.getCellValue(row.getCell(i)) != null && String.valueOf(TestData.getCellValue(cell)).equalsIgnoreCase(value)) {
				return i;
			}
			
			i++;
		}
		
		return -1;
	}
	
	private static Smartcard parseRowToSmartcard(Sheet sheet, Row row) {
		int idCol = TestData.findCol(sheet.getRow(sheet.getFirstRowNum()), "smartcardId");
		int scCol = TestData.findCol(sheet.getRow(sheet.getFirstRowNum()), "smartcardSc");

		String id = String.valueOf(TestData.getCellValue(row.getCell(idCol)));
		String sc = String.valueOf(TestData.getCellValue(row.getCell(scCol)));
		return new Smartcard(id, sc);
	}
	
	private static MobileUser parseRowToMobileUser(Sheet sheet, Row row) {
		int userCol = TestData.findCol(sheet.getRow(sheet.getFirstRowNum()), "username");
		int passCol = TestData.findCol(sheet.getRow(sheet.getFirstRowNum()), "password");
		
		String user = String.valueOf(TestData.getCellValue(row.getCell(userCol)));
		String pass = String.valueOf(TestData.getCellValue(row.getCell(passCol)));
		
		return new MobileUser(user, pass);
	}
}
