package utilities.data;

public enum CardState {

	INITIALIZED, ENABLED, ISSUED, HOTLISTED, DEHOTLISTED, BLOCKED
}