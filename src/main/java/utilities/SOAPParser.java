package utilities;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class SOAPParser {
	
	public static Element parseSOAP(String response) {
		try {
			StringBuilder builder = new StringBuilder();
			builder.append("<?xml version=\"1.0\"?>");
			builder.append(response);
			String xml = builder.toString(); 
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder docBuilder = factory.newDocumentBuilder(); 
			InputSource is = new InputSource(new StringReader(xml)); 
			Document doc = docBuilder.parse(is); 
			return doc.getDocumentElement();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
}
