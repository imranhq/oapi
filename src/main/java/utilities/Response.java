package utilities;

import java.util.HashMap;

import model.response.ResponseBodyType;

public class Response {

	private String errorBodyText;
	private String messageBodyText;
	private String message;
	private ResponseElement messageBody;
	private ResponseElement errorBody;
	private HashMap<String, String> headers;
	private int code;
	
	public Response() {
		this.errorBodyText = "";
		this.messageBodyText = "";
		this.message = "";
		this.code = 999;
		this.headers = new HashMap<String, String>();
		messageBody = parseResponse(this.messageBodyText);
		errorBody = parseResponse(this.errorBodyText);
	}
	
	public Response(int code, String message, String responseBody, String errorBody, HashMap<String, String> headers) {
		this.errorBodyText = errorBody;
		this.messageBodyText = responseBody;
		this.message = message;
		this.code = code;
		this.headers = headers;
		this.messageBody = parseResponse(this.messageBodyText);
		this.errorBody = parseResponse(this.errorBodyText);
	}
	
	public String getContentType() {
		if(headers.containsKey("Content-Type")) {
			return headers.get("Content-Type");
		}
		
		return "";
	}
	
	public int getResponseCode() {
		return this.code;
	}
	
	public HashMap<String, String> getResponseHeaders() {
		return this.headers;
	}
	
	public String toString() {
		return "HEADERS: " + this.headers.toString() + "\nBODY: " + this.messageBodyText + "\nERROR: " + this.errorBody + "\nCODE: " + this.code + "\nMESSAGE: " + this.message;
	}

	public String getSessionID() {
		
		if(!this.headers.get("Content-Type").contains("text/html")) {
			return "";
		}
		
		String[] split = this.messageBodyText.split("<input type=\"hidden\" name=\"sessionID\" value=\"");
		
		if(split == null) {
			return "";
		}
		
		if(split.length > 1) {
			split = split[1].split("\"/>");
		}
		
		if(split == null) {
			return "";
		}
		
		if(split.length > 0) {
			return split[0];
		}
		
		return "";
	}
	
	private ResponseElement parseResponse(String resp) {
		if(!this.headers.get("Content-Type").contains("application/json") && !this.headers.get("Content-Type").contains("text/xml")) {
			return null;
		}
		
		if(resp.equals("")) {
			return null;
		}
		
		if(this.headers.get("Content-Type").contains("application/json")) {
			return new ResponseElement(JSONParser.getJSONStringAsElement(resp));
		}
		
		if(this.headers.get("Content-Type").contains("text/xml")) {
			return new ResponseElement(SOAPParser.parseSOAP(resp));
		}
		
		return null;
	}
	
	public ResponseElement getResponseElement(ResponseBodyType type) {
		
		if(errorBody == null && messageBody == null) {
			return null;
		}
		
		if(type == ResponseBodyType.MESSAGE) {
			return messageBody;
		}
		
		if(type == ResponseBodyType.ERROR) {
			return errorBody;
		}
		
		return null;
	}
	
	public ResponseElement getErrorElement() {
		return getResponseElement(ResponseBodyType.ERROR);
	}
	
	public ResponseElement getMessageElement() {
		return getResponseElement(ResponseBodyType.MESSAGE);
	}
	
	public ResponseElement getBodyElement() {
		if(getResponseElement(ResponseBodyType.MESSAGE) != null) {
			return getResponseElement(ResponseBodyType.MESSAGE);
		}
		
		if(getResponseElement(ResponseBodyType.ERROR) != null) {
			return getResponseElement(ResponseBodyType.ERROR);
		}
		
		return null;
	}

}
