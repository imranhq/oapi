package utilities;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ResponseElement {

	private Element element;
	
	public ResponseElement(Element element) {
		this.element = element;
	}
	
	public boolean contains(String key, String value) {
		if(this.getValue(key).equalsIgnoreCase(value)) {
			return true;
		}
		
		return false;
	}
	
	public boolean contains(String key) {
		return hasTag(key);
	}
	
	public ResponseElement getChild(String key) {
		NodeList nodes = element.getChildNodes();
		for(int i = 0; i < nodes.getLength(); i++) {
			if(nodes.item(i).getNodeName().equalsIgnoreCase(key) && nodes.item(i).getChildNodes().getLength() > 1) {
				return new ResponseElement((Element)nodes.item(i));
			}
		}
		
		return null;
	}
	
	public ResponseElement getNestedChild(String key) {
		NodeList nodes = element.getChildNodes();
		Node child = null;
		if(nodes.getLength() > 0) {
			child = searchChildren(nodes, key);
		}
		
		return new ResponseElement((Element)child);
	}
	
	private Node searchChildren(NodeList nodeList, String key) {
		Node child = null;
		for(int i = 0; i < nodeList.getLength(); i++) {
			Node el = nodeList.item(i);
			
			if(el.getNodeName().endsWith(key)) {
				child = el;
				break;
			}
			
			if(el.hasChildNodes()) {
				child = searchChildren(el.getChildNodes(), key);
			}
		}
		
		return child;
	}
	
	public boolean isEmpty() {
		if(element == null) {
			return true;
		}
		
		return element.getChildNodes().getLength() <= 0;
	}
	
	public ArrayList<ResponseElement> getList(String key) {
		ArrayList<ResponseElement> respList = new ArrayList<ResponseElement>();
		NodeList nodes = element.getElementsByTagName(key);
		for(int i = 0; i < nodes.getLength(); i++) {
			respList.add(new ResponseElement((Element)nodes.item(i)));
		}
		
		return respList;
	}
	
	/**
	 * Checks the Element and children for a given tag and returns the value of the first found.
	 * @param element Parent element to be searched
	 * @param tagName Tag to be identified
	 * @return The String value of first identified tag.  Returns blank if not found.
	 */
	public String getValue(String tagName) { 
		String text = ""; 
		NodeList nodeList = element.getElementsByTagName(tagName); 
		if(nodeList != null && nodeList.getLength() > 0) { 
			Element el = (Element)nodeList.item(0); 
			if(el.getFirstChild() != null) {
				text = el.getFirstChild().getNodeValue(); 
			}
		} 

		return text;
	} 
	
	/**
	 * Checks the Element and children for a given tag and returns true if found
	 * @param element Parent element to be searched
	 * @param tagName Tag to be identified
	 * @return True/False whether tag was found.  Returns false if not found.
	 */
	private boolean hasTag(String tagName) {
		NodeList nodeList = element.getElementsByTagName(tagName);
		if(nodeList != null && nodeList.getLength() > 0) {
			return true;
		}
		return false;
	}
	
	/**
	 * Checks the Element and children for a given tag and returns the value of the first found.
	 * Parses the value found into int
	 * @param element Parent element to be searched
	 * @param tagName Tag to be identified
	 * @return The int value of first identified tag.  Returns -1 if not found.
	 */
	public int getIntValue(String tagName) { 
		try { 
			return Integer.parseInt(getValue(tagName)); 
		} catch(NumberFormatException nfe) { 
			return -1; 
		} 
	} 
	
	//returns false if it can't parse 
	/**
	 * Checks the Element and children for a given tag and returns the value of the first found.
	 * Parses the value found into boolean
	 * @param element Parent element to be searched
	 * @param tagName Tag to be identified
	 * @return The boolean value of first identified tag.  Returns false if not found.
	 */
	public boolean getBooleanValue(String tagName) { 
		return Boolean.parseBoolean(getValue(tagName)); 
	} 
	
	/**
	 * Checks the Element and children for a given tag and returns the value of the first found.
	 * Parses the value found into long
	 * @param element Parent element to be searched
	 * @param tagName Tag to be identified
	 * @return The long value of first identified tag.  Returns -1 if not found.
	 */
	public long getLongValue(String tagName) { 
		try {
			return Long.parseLong(getValue(tagName)); 
		} catch(NumberFormatException nfe) { 
			return -1; 
		} 
	}
	
	//Come up with a more break-proof way to display this.
	public String toString() {
		String line = String.format("%s\n", element.getTagName());
		NodeList nodeList = element.getChildNodes();

		line = line + printList(nodeList);

		return line;
	}
	
	private String printList(NodeList nodeList) { 
		String line = "";
		for(int i = 0; i < nodeList.getLength(); i++) {
			Node el = nodeList.item(i);
			if(el.hasChildNodes() && (el.getFirstChild().getNodeType() != Node.TEXT_NODE)) {
				line = line + String.format("%s\n", el.getNodeName());
				line = line + printList(el.getChildNodes());
				line = line + String.format("/%s\n", el.getNodeName());
			} else {
				line = line + String.format("%s: %s\n", el.getNodeName(), el.getTextContent());
			}
		}
		return line;
	}
}
