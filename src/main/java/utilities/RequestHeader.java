package utilities;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.HashMap;

/**
 * Static methods relating to setting up common request headers
 * @author Mike Fleig - mfleig@planittesting.com
 */
public class RequestHeader {

	/**
	 * Creates the standard Content-Type header
	 * @return A hashmap representing the header properties
	 */
	public static HashMap<String, String> STANDARD() {
	    HashMap<String, String> header = new HashMap<String, String>();
	    header.put("Content-Type", "application/x-www-form-urlencoded");
	    //header.put("Accept-Language", "en-us,en;q=0.8");
	    //header.put("Accept", "*/*");
		//header.put("Connection", "close");
		//header.put("Accept-Encoding", "gzip, deflate");
		//header.put("Cache-Control", "no-cache");

		return header;
	}
	
	/**
	 * Creates a standard Content-Type header and adds authorization properties
	 * @param token A string representing {@literal {client_id}:{client_secret}}
	 * @return A hashmap representing the header properties
	 */
	public static HashMap<String, String> BASICAUTH(String token) {
		HashMap<String, String> header = new HashMap<String, String>();
		header.put("Content-Type", "application/x-www-form-urlencoded");
		Encoder encoder = Base64.getEncoder();
		
		byte[] encodedBytes = encoder.encode(token.getBytes());
		String encodedString = new String(encodedBytes, StandardCharsets.UTF_8);
		
		header.put("Authorization", String.format("Basic %s", encodedString));
		return header;
	}
	
	public static HashMap<String, String> ENCODEDAUTH(String token) {
		HashMap<String, String> header = new HashMap<String, String>();
		
		//header.put("Content-Type", "application/x-www-form-urlencoded");
		byte[] encodedBytes = token.getBytes();
		String encodedString = new String(encodedBytes, StandardCharsets.UTF_8);
		
		header.put("Authorization", String.format("Bearer %s", encodedString));
		//header.put("User-Agent", "Apache-HttpClient/4.2.6");
		//header.put("Accept-Language", "en-US,en;q=0.8");
		
		return header;
	}
	
	public static HashMap<String, String> JSONAUTH(String token) {
		HashMap<String, String> header = new HashMap<String, String>();
		
		header.put("Content-Type", "application/json");
		byte[] encodedBytes = token.getBytes();
		String encodedString = new String(encodedBytes, StandardCharsets.UTF_8);
		
		header.put("Authorization", String.format("Bearer %s", encodedString));
		
		return header;
	}
	
	/**
	 * Spits the header out to the console
	 * @param header The header properties to display
	 */
	public static void printHeader(HashMap<String, String> header) {
		String headers = "";
		int i = 0;
		for(String key : header.keySet()) {
			headers = headers + String.format("%s=%s", key, header.get(key));
			i++;
			if(i < header.size()) {
				headers = headers + " | ";
			}
		}
		System.out.println(String.format("REQUEST Headers: %s", headers));
	}
}
