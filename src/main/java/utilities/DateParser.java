package utilities;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateParser {
	
	public static String ISO8601DateTime(String dateString) {
		try {
			dateString = java.net.URLDecoder.decode(dateString, "UTF-8");
			DateFormat origFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss a");
			DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = origFormat.parse(dateString);
			
			String temp = targetFormat.format(date);
			String[] dt = temp.split(" ");
			temp = dt[0] + "T" + dt[1] + "Z";
			return temp;
		} catch(ParseException pEx) {
			System.out.println(String.format("PARSE ERROR: %s", dateString));
			return "";
		} catch (UnsupportedEncodingException e) {
			System.out.println(String.format("ENCODE ERROR: %s", dateString));
			return "";
		} catch(IllegalArgumentException iae) {
			System.out.println(String.format("ARGUMENT ERROR: %s", dateString));
			return "";
		}
	}
	
	public static String ISO8601Date(String dateString) {
		try {
			dateString = java.net.URLDecoder.decode(dateString, "UTF-8");
			DateFormat origFormat = new SimpleDateFormat("yyyyMMdd");
			DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = origFormat.parse(dateString);
			return targetFormat.format(date);
		} catch(ParseException pEx) {
			return "";
		} catch (UnsupportedEncodingException e) {
			return "";
		} catch(IllegalArgumentException iae) {
			System.out.println(String.format("PARSE ERROR: %s", dateString));
			return "";
		}
	}

}
