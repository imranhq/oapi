package utilities;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import model.response.ParameterValue;

public class JSONParser {
		
	/**
	 * Takes the JSON response from a REST call and converts it to an org.w3c.Element type.
	 * @param json JSON formatted response from webservice
	 * @return org.w3c.Element representation of the JSON string
	 */
	public static Element getJSONStringAsElement(String json) {
		try {
			Document doc = loadJSONFromString(json);
			return doc.getDocumentElement();
		}
		catch(JSONException jsonEx) {
			return null;
		}
	}
		
	/**
	 * Used internally to convert a JSON string to a Document type.
	 * For Cubic, the JSON object is wrapped inside a 'reponse' root element for document building.
	 * @param json JSON formatted response from webservice
	 * @return org.w3c.Document representation of the JSON string
	 */
	private static Document loadJSONFromString(String json) { 
		try { 
			JSONObject jsonObject = new JSONObject(json);
			
			StringBuilder builder = new StringBuilder();
			builder.append("<?xml version=\"1.0\"?>");
			builder.append("<response>");
			builder.append(XML.toString(jsonObject));
			builder.append("</response>");
			String xml = builder.toString(); 
			
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
			DocumentBuilder docBuilder = factory.newDocumentBuilder(); 
			InputSource is = new InputSource(new StringReader(xml)); 
			return docBuilder.parse(is); 
		} catch(ParserConfigurationException pce) { 
			pce.printStackTrace(); 
		} catch(SAXException se) { 
			se.printStackTrace(); 
		} catch(IOException ioe) { 
			ioe.printStackTrace(); 
		}
		return null; 
	} 
	
	public static String buildJSONBody(HashMap<String, ParameterValue> params) {
		StringBuilder postData = new StringBuilder();
		int i = 0;
		postData.append("{\n");
		for(String key: params.keySet()) {
			switch(params.get(key).getType()) {
				case LONG:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				case INT:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				case STRING:
					postData.append(String.format("    \"%s\": \"%s\"", key, params.get(key).getValue()));
					break;
				case BOOLEAN:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				case LIST:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				case CUSTOM:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				default:
					break;
			}
			i++;
			
			if(i < params.size()) {
				postData.append(",\n");
			}
		}
		postData.append("\n}");
		
		return postData.toString();
	}
}
