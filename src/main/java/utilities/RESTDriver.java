package utilities;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import model.response.ParameterValue;

/**
 * Handles the interaction with a URL
 * @author Mike Fleig - mfleig@planittesting.com
 *
 */
public final class RESTDriver {

	private static int TIME_OUT = 15000;
	
	public RESTDriver() {
		Properties properties = new java.util.Properties();

		try {
			properties.load(new FileInputStream("src/main/resources/environment.properties"));  
			
			TIME_OUT = Integer.parseInt(properties.getProperty("RESTTimeout"));
		// If we error out, set a default
		} catch(FileNotFoundException fnfEx) {
			System.out.println("ERROR: Cannot location environment.properties");
			TIME_OUT = 15000;
		} catch(IOException ioe) {
			System.out.println("ERROR: Cannot location environment.properties");	
			TIME_OUT = 15000;
		}
	}
	
	public static Response GET(String urlStr, HashMap<String, String> headers, HashMap<String, ParameterValue> params) {
		try {
			//Build request

			System.out.println(String.format("REQUEST (%s): %s", "GET", urlStr));
			RequestHeader.printHeader(headers);

			urlStr = appendUrl(urlStr, params);

			
			URL url = new URL(urlStr);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			
			//This shouldn't be a fixed number.  Load from settings?
			conn.setConnectTimeout(TIME_OUT);
			conn.setUseCaches(false);

			//Add headers
			if(!headers.isEmpty()) {
				for(String key : headers.keySet()) {
					conn.setRequestProperty(key, headers.get(key));
				}
			}

			//Send!
			Response response = checkResponse(conn);
			return response;
			
		} catch(SocketTimeoutException ste) { 
			System.out.println(String.format("TIMEOUT: We ran out of time to connect to %s", urlStr));
		} catch(MalformedURLException e) {
			//Log error
			System.out.println(String.format("BAD URL: The URL %s isn't a proper format", urlStr));
		} catch(IOException ioe) {
			System.out.println(String.format("IO ERROR: %s", ioe.getMessage()));
		} 
			
		return new Response();
	}
	
	public static Response DELETE(String urlStr, HashMap<String, String> headers, HashMap<String, ParameterValue> params) {
		
		try {
			//Build request

			System.out.println(String.format("REQUEST (%s): %s", "DELETE", urlStr));
			RequestHeader.printHeader(headers);

			urlStr = appendUrl(urlStr, params);
			
			URL url = new URL(urlStr);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			
			//This shouldn't be a fixed number.  Load from settings?
			conn.setConnectTimeout(TIME_OUT);
			conn.setRequestMethod("DELETE");
			conn.setUseCaches(false);

			//Add headers
			if(!headers.isEmpty()) {
				for(String key : headers.keySet()) {
					conn.setRequestProperty(key, headers.get(key));
				}
			}

			//Send!
			Response response = checkResponse(conn);
			return response;
			
		} catch(SocketTimeoutException ste) { 
			System.out.println(String.format("TIMEOUT: We ran out of time to connect to %s", urlStr));
		} catch(MalformedURLException e) {
			//Log error
			System.out.println(String.format("BAD URL: The URL %s isn't a proper format", urlStr));
		} catch(IOException ioe) {
			System.out.println(String.format("IO ERROR: %s", ioe.getMessage()));
		}
	
		return new Response();
	}
	
	public static Response PUT(String urlStr, HashMap<String, String> headers, HashMap<String, ParameterValue> params) {
		// Come up with a meaningful implementation of PUT as required
		return new Response();
	}
	
	public static Response POST(String urlStr, HashMap<String, String> headers, HashMap<String, ParameterValue> params) {
		try {
			//Build request

			System.out.println(String.format("REQUEST (%s): %s", "POST", urlStr));
			RequestHeader.printHeader(headers);
			
			URL url = new URL(urlStr);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			
			//This shouldn't be a fixed number.  Load from settings?
			conn.setConnectTimeout(TIME_OUT);
			conn.setRequestMethod("POST");
			conn.setUseCaches(false);

			//Add headers
			if(!headers.isEmpty()) {
				for(String key : headers.keySet()) {
					conn.setRequestProperty(key, headers.get(key));
				}
			}
			
			String urlParameters;
			//Add params
			if(headers.containsKey("Content-Type") && headers.get("Content-Type").contains("application/json")) {
				urlParameters = buildJSONBody(params);
			} else {
				urlParameters = buildENCODEDURLBody(params);
			}

		    //Overrides conn.setRequestMethod() to POST
		    conn.setDoOutput(true);
		   
			System.out.println(String.format("REQUEST Parameters: %s", urlParameters));
		    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());

		    writer.write(urlParameters);
		    writer.flush();	
		    //End Add params
		    

			//Send!
			Response response = checkResponse(conn);
			return response;
			
		} catch(SocketTimeoutException ste) { 
			System.out.println(String.format("TIMEOUT: We ran out of time to connect to %s", urlStr));
		} catch(MalformedURLException e) {
			//Log error
			System.out.println(String.format("BAD URL: The URL %s isn't a proper format", urlStr));
		} catch(IOException ioe) {
			System.out.println(String.format("IO ERROR: %s", ioe.getMessage()));
		} 
			
		return new Response();
	}
	
	private static HashMap<String, String> getHeaders(HttpsURLConnection conn) {
		HashMap<String, String> headers = new HashMap<String, String>();
		
		for(String key : conn.getHeaderFields().keySet()) {
			headers.put(key, conn.getHeaderField(key));
		}
		
		return headers;
	}
	
	private static String appendUrl(String url, HashMap<String, ParameterValue> params) {
		//Append content for query - this may vary depend on Content-Type
		String param = "";
		if(params != null && params.size() > 0) { 
			int i = 0; 
			param = param + "?"; 
			for(String key : params.keySet()) { 
				param = param + key + "=" + params.get(key).getValue(); 
				i = i + 1; 
				if(i < params.size()) { 
					param = param + "&"; 
				} 
			} 
			url = url + param;
			System.out.println(String.format("REQUEST Parameters: %s", param));
		}
		
		return url;
	}
	
	private static String buildJSONBody(HashMap<String, ParameterValue> params) {
		StringBuilder postData = new StringBuilder();
		int i = 0;
		postData.append("{\n");
		for(String key: params.keySet()) {
			switch(params.get(key).getType()) {
				case LONG:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				case INT:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				case STRING:
					postData.append(String.format("    \"%s\": \"%s\"", key, params.get(key).getValue()));
					break;
				case BOOLEAN:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
					break;
				case LIST:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
				case CUSTOM:
					postData.append(String.format("    \"%s\": %s", key, params.get(key).getValue()));
				default:
					break;
			}
			i++;
			
			if(i < params.size()) {
				postData.append(",\n");
			}
		}
		postData.append("\n}");
		
		return postData.toString();
	}
	
	private static String buildENCODEDURLBody(HashMap<String, ParameterValue> params) {
		StringBuilder postData = new StringBuilder();
		try {
		    for (String key : params.keySet()) {
		        if (postData.length() != 0) postData.append('&');
		        postData.append(URLEncoder.encode(key, "UTF-8"));
		        postData.append('=');
		        postData.append(URLEncoder.encode(params.get(key).getValue(), "UTF-8"));
		    }
	    	return postData.toString();
		} catch (UnsupportedEncodingException uex) {
			return "";
		}
	}
	
	private static Response checkResponse(HttpsURLConnection conn) {
		String errorBody = "";
		String message = "";
		String messageBody = "";
		int code = 999;
		HashMap<String, String> respHeaders = new HashMap<String, String>();

		try {
			System.out.println(String.format("RESPONSE (%s): %s", conn.getResponseCode(), conn.getResponseMessage()));
			code = conn.getResponseCode();
			message = conn.getResponseMessage();
			respHeaders = getHeaders(conn);
		
			// Check the error stream
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getErrorStream())); 
			StringBuilder sb = new StringBuilder(); 
			String line; 
			while ((line = rd.readLine()) != null) { 
				sb.append(line); 
			} 
			rd.close(); 
			errorBody = sb.toString();
		} catch (NullPointerException npe) {
			errorBody = "";
		} catch (IOException ioe) {
			errorBody = "";
		}
		
		try {
			// Check the input stream
			BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream())); 
			StringBuilder sb = new StringBuilder(); 
			String line; 
			
			while ((line = rd.readLine()) != null) { 
				sb.append(line); 
			} 
			rd.close(); 
			conn.disconnect();
			
			messageBody = sb.toString();
		} catch(NullPointerException npe) {
			messageBody = "";
		} catch(IOException ioe) {
			messageBody = "";
		}
		
		return new Response(code, message, messageBody, errorBody, respHeaders);
	}
}
