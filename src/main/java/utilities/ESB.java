package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import model.datatypes.Address;
import model.datatypes.Customer;

public class ESB {

	public static boolean UnlinkCard(String cardname) {
		return null != null;
	}
	
	public static Customer GetCustomerDetails(String username) {
		String domain = getDomain();
		String qname = "GetCustomerProfile";
		String uri = "/cm-esb/http/Customer";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("Username", username);
		
		Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/cm/Customer/v1/", "v1", "v11", parameters);
		
		if(response.getResponseCode() == 200) {
			ResponseElement body = response.getBodyElement().getNestedChild("GetCustomerProfileResponse");
			String title = body.getValue("ns2:Title");
			String fName = body.getValue("ns2:FirstName");
			String mName = body.getValue("ns2:MiddleName");
			String lName = body.getValue("ns2:LastName");
			String email = body.getValue("ns2:Email");
			String phone = body.getValue("ns2:Telephone");
			String mobile = body.getValue("ns2:Mobile");
			String pin = body.getValue("ns2:PIN");
			
			ResponseElement addressElement = body.getNestedChild("ns2:Address");
			String address1 = addressElement.getValue("ns2:Address1");
			String address2 = addressElement.getValue("ns2:Address2");
			String address3 = addressElement.getValue("ns2:Address3");
			String postcode = addressElement.getValue("ns2:Postcode");
			String suburb = addressElement.getValue("ns2:Suburb");
			String state = addressElement.getValue("ns2:State");
			String country = addressElement.getValue("ns2:country");
			Address address = new Address(address1, address2, address3, postcode, suburb, state, country);
			
			return new Customer(title, fName, mName, lName, email, phone, mobile, pin, address);
		} else {
			return new Customer();
		}
	}
	
	public static boolean HotlistCard(long id) {
		return HotlistCard(String.valueOf(id));
	}
	
	public static boolean HotlistCard(String id) {
		String domain = getDomain();
		String qname = "HotlistSmartcard";
		String uri = "/smartcard-esb/http/SmartcardHotlist";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("SmartcardId", id);
		parameters.put("HotlistReasonCode", "1");
		
		if(!GetCardState(id).equals("HOTLISTED")) {
			Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/sc/SmartcardHotlist/v1/", "v1", "v11", parameters);
			
			return response.getResponseCode() == 200;
		} else {
			return true;
		}
	}
	
	public static boolean DeHotlistCard(long id) {
		return DeHotlistCard(String.valueOf(id));
	}
	
	public static boolean DeHotlistCard(String cardId) {
		String domain = getDomain();
		String qname = "DehotlistSmartcard";
		String uri = "/smartcard-esb/http/SmartcardHotlist";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("SmartcardId", cardId);
		parameters.put("HotlistReasonCode", "10");
		
		if(GetCardState(cardId).equals("HOTLISTED")) {
			Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/sc/SmartcardHotlist/v1/", "v1", "v11", parameters);
			
			return response.getResponseCode() == 200;
		} else {
			return true;
		}
	}
	
	public static boolean AddValueToCard(String cardId, int value) {
		String domain = getDomain();
		String qname = "AddValue";
		String uri = "/smartcard-esb/http/SmartcardLoad";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("SmartcardId", cardId);
        parameters.put("Value", String.valueOf(value));
        parameters.put("PaymentMethodCode", "1");
        parameters.put("ReasonCode", "1");
        parameters.put("ChannelCode", "CWS");
        parameters.put("Operator", "1");
		
		Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/sc/SmartcardLoad/v1/", "v1", "v11", parameters);
		return response.getResponseCode() == 200;
	}
	
	public static String GetCustomerId(String username) {
		String domain = getDomain();
		String qname = "GetCustomerId";
		String uri = "/cm-esb/http/Customer";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("Username", username);
		
		Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/cm/Customer/v1/", "v1", "v11", parameters);
		
		return response.getBodyElement().getNestedChild("GetCustomerIdResponse").getValue("ns2:CustomerId");
	}
	
	public static int GetValueOnCard(String cardId) {
		String domain = getDomain();
		String qname = "GetSmartcardDetails";
		String uri = "/smartcard-esb/http/Smartcard_v2";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("SmartcardId", cardId);
		
		Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/sc/Smartcard/v3/", "v3", "v1", parameters);
		
		return Integer.parseInt(response.getBodyElement().getNestedChild("GetSmartcardDetailsResponse").getValue("ns4:SVPending"));
	}
	
	public static int GetAutoloadOnCard(long cardId) {
		return ESB.GetAutoloadOnCard(String.valueOf(cardId));
	}
	
	public static int GetAutoloadOnCard(String cardId) {
		String domain = getDomain();
		String qname = "GetSmartcardDetails";
		String uri = "/smartcard-esb/http/Smartcard_v2";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("SmartcardId", cardId);
		
		Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/sc/Smartcard/v3/", "v3", "v1", parameters);
		
		return Integer.parseInt(response.getBodyElement().getNestedChild("GetSmartcardDetailsResponse").getValue("ns4:AutoLoadAmount"));
	}
	
	public static String GetCardState(String cardId) {
		String domain = getDomain();
		String qname = "GetSmartcardDetails";
		String uri = "/smartcard-esb/http/Smartcard_v2";
		
		HashMap<String, String> parameters = new HashMap<String, String>();
		parameters.put("SmartcardId", cardId);
		
		Response response = SOAPDriver.request((domain + uri), qname, "http://css.cubic.com/ws/sc/Smartcard/v3/", "v3", "v1", parameters);
		
		return response.getBodyElement().getNestedChild("GetSmartcardDetailsResponse").getValue("ns4:CardState");
	}
	
	private static String getDomain() {
		Properties properties = new java.util.Properties();
		String domain = "http://engtstsoa1-vl.eng.test.cub:8080";
		try {
			properties.load(new FileInputStream("src/main/resources/environment.properties"));		
		// If we error out, just set a default to engtest
		} catch(FileNotFoundException fnfEx) {
			System.out.println("ERROR: Cannot location environment.properties");
		} catch(IOException ioe) {
			System.out.println("ERROR: Cannot load ESBEndpoint from environment.properties");
		}
		
		return domain;
	}

	public static String GetCardState(long id) {
		return GetCardState(String.valueOf(id));
	}
}
