/**
 * Contains service classes that allow automators to semantically interact with services.
 */
/**
 * Contains service classes that allow automators to semantically interact with services.
 * @author Mike Fleig - mfleig@planittesting.com
 * 
 */
package model.services;