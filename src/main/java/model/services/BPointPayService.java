package model.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import model.datatypes.BPointPayReference;
import model.datatypes.CreditCard;
import model.datatypes.PaymentAuthority;
import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;

public class BPointPayService {
	
	public BPointPayService() {
		
	}
	
	public BPointPayReference pay(PaymentAuthority payAuth, CreditCard cc, int topup) {
		HashMap<String, ParameterValue> params = new HashMap<String, ParameterValue>();
		String url = "http://localhost/";
		
		Properties properties = new java.util.Properties();
		
		try {
			properties.load(new FileInputStream("src/main/resources/environment.properties"));  
			
			url = properties.getProperty("BPointEndpoint");
		} catch(FileNotFoundException fnfEx) {
			System.out.println("ERROR: Cannot location environment.properties");
		} catch(IOException ioe) {
			System.out.println("ERROR: Cannot location environment.properties");			
		}
		
		params.put("in_pay_token", new ParameterValue(ParameterType.STRING, payAuth.getPaymentAuthToken()));
        params.put("in_merchant_number", new ParameterValue(ParameterType.LONG, String.valueOf(payAuth.getMerchantNumber())));
        params.put("in_merchant_reference", new ParameterValue(ParameterType.LONG, String.valueOf(payAuth.getMerchantReceipt())));
        params.put("in_crn1", new ParameterValue(ParameterType.STRING, payAuth.getCRN1()));
        params.put("in_crn2", new ParameterValue(ParameterType.STRING, payAuth.getCRN2()));
        params.put("in_amount", new ParameterValue(ParameterType.INT, String.valueOf(topup)));
        params.put("in_credit_card", new ParameterValue(ParameterType.LONG, String.valueOf(cc.getNumber())));
        params.put("in_expiry_month", new ParameterValue(ParameterType.STRING, cc.getExpiryDate()));
        params.put("in_expiry_year", new ParameterValue(ParameterType.STRING, cc.getExpiryYear()));
        params.put("in_cvv", new ParameterValue(ParameterType.INT, String.valueOf(cc.getCVV())));
        params.put("in_store_card", new ParameterValue(ParameterType.INT, String.valueOf(0)));
        params.put("in_receipt_page_url", new ParameterValue(ParameterType.STRING, url));
        
        Response resp = RESTDriver.POST("https://evolve-uat.premier.com.au/connectornew/pay.aspx", RequestHeader.STANDARD(), params);
        
        return new BPointPayReference(resp.getResponseHeaders().get("Location").replace(String.format("%s?", url), ""));
	}
}
