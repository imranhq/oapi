package model.services;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import model.actors.Authenticator;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;
import utilities.ResponseElement;

public class SmartcardDetailsService extends BaseService {
	
	private Authenticator requester;
	
	public SmartcardDetailsService(Authenticator requester) {
		super("/v1/smartcard/details/");
		this.requester = requester;
	}
	
	public boolean responseContainsRequiredFields(String smartcardId) {
		List<String> reqParams = Arrays.asList("AutoLoadAmount", "AutoLoadThreshold", "CardState", "CardTypeCode", "IsRegistered", "DepositPaid", "isEntitlementBased", "LastSeenDateTime", "MediaTypeCode", "PassengerTypeCode", "PendingBalanceActivation", "reloadable", "SmartcardId", "SVBalance", "SVPending");
		Response response = RESTDriver.GET(this.getRequestUrl(smartcardId), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		return this.responseContainsRequiredParameters(response, reqParams);
	}
	
	public ResponseElement requestDetails(String smartcardId) {
		
		Response response = RESTDriver.GET(this.getRequestUrl(smartcardId), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		return response.getBodyElement();
	}
	
	public Response requestDetailsResponse(String smartcardId, String accessToken, HashMap<String, ParameterValue> overloadedParams) {
		this.overloadParameters(overloadedParams);
		return RESTDriver.GET(this.getRequestUrl(smartcardId), RequestHeader.ENCODEDAUTH(accessToken), parameters);
	}
	
	public Response requestDetailsResponse(String smartcardId, HashMap<String, ParameterValue> overloadedParams) {
		this.overloadParameters(overloadedParams);
		return RESTDriver.GET(this.getRequestUrl(smartcardId), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
	}
}
