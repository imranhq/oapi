package model.services;

import java.util.HashMap;

import model.actors.Authenticator;
import model.actors.Smartcard;
import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;

public class SmartcardActivateService extends BaseService {
	private Authenticator requester;
	
	public SmartcardActivateService(Authenticator requester) {
		super("/v1/smartcard/activate/");
		this.requester = requester;
	}
	
	public Response activateCardResponse(Smartcard card, HashMap<String, ParameterValue> params) {
		parameters = new HashMap<String, ParameterValue>();
		
		addParameter("SmartcardId", ParameterType.LONG, card.getID());
		addParameter("SmartcardSc", ParameterType.STRING, card.getCode());
		
		this.overloadParameters(params);
		
		return RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
	}
	
	public boolean activateCard(Smartcard card) {
		addParameter("SmartcardId", ParameterType.LONG, card.getID());
		addParameter("SmartcardSc", ParameterType.STRING, card.getCode());
		
		Response response = RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
		
		if(!response.getBodyElement().contains("SmartcardId")) {
			return false;
		}
		
		if(response.getBodyElement().getValue("SmartcardId").equals(card.getID())) {
			return true;
		}
		
		//Any other issues
		return false;
	}
}
