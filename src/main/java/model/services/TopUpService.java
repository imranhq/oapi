package model.services;

import java.util.ArrayList;

import model.actors.Authenticator;
import model.datatypes.DetailedSmartcard;
import model.datatypes.Order;
import model.datatypes.TopUpAmount;
import model.response.ParameterType;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;
import utilities.ResponseElement;

public class TopUpService extends BaseService {
	
	private Authenticator requester;
	
	//Remember, we need to be authenticated before we try accessing this resource
	public TopUpService(Authenticator requester) {
		super("/v1/smartcard/topup/");
		this.requester = requester;
	}
	
	public ArrayList<TopUpAmount> getTopUpAmounts(DetailedSmartcard card) {
		ArrayList<TopUpAmount> amounts = new ArrayList<TopUpAmount>();
		Response resp = RESTDriver.GET(this.getRequestUrl(String.valueOf(card.getID())), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		
		ResponseElement body = resp.getMessageElement();
		ArrayList<ResponseElement> elements = body.getList("TopupAmount");
		for(ResponseElement element: elements) {
			amounts.add(new TopUpAmount(element));
		}
		
		return amounts;
	}
	
	public ArrayList<TopUpAmount> getTopUpAmounts(int fcc) {
		ArrayList<TopUpAmount> amounts = new ArrayList<TopUpAmount>();
		addParameter("fcc", ParameterType.INT, String.valueOf(fcc));
		Response resp = RESTDriver.GET(this.getRequestUrl(), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		
		ResponseElement body = resp.getMessageElement();
		ArrayList<ResponseElement> elements = body.getList("TopupAmount");
		for(ResponseElement element: elements) {
			amounts.add(new TopUpAmount(element));
		}
		
		return amounts;
	}
	
	public Response getTopUpAmountsResponse(DetailedSmartcard card) {
		return RESTDriver.GET(this.getRequestUrl(String.valueOf(card.getID())), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
	}
	
	public boolean topUpAmountsContain(DetailedSmartcard card, TopUpAmount amount) {
		ArrayList<TopUpAmount> amounts = new ArrayList<TopUpAmount>();
		Response resp = RESTDriver.GET(this.getRequestUrl(String.valueOf(card.getID())), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		
		ResponseElement body = resp.getMessageElement();
		
		ArrayList<ResponseElement> elements = body.getList("TopupAmount");
		for(ResponseElement element: elements) {
			amounts.add(new TopUpAmount(element));
		}
		
		for(TopUpAmount existingAmount: amounts) {
			if(existingAmount.equals(amount)) {
				return true;
			}
		}
		
		return false;
	}

	public Order topUpCard(DetailedSmartcard card, long payReceipt, int topup) {
		addParameter("SmartcardId", ParameterType.LONG, String.valueOf(card.getID()));
		addParameter("TopupValue", ParameterType.INT, String.valueOf(topup));
		addParameter("MerchantReceipt", ParameterType.STRING, String.valueOf(payReceipt));
		
		Response response = RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
		System.out.println(response.getBodyElement().toString());
		if(response.getBodyElement().contains("OrderId")) {
			return new Order(response.getBodyElement());
		} else {
			return new Order();
		}
	}
	
	public Order topUpCard(DetailedSmartcard card, int topup) {
		addParameter("SmartcardId", ParameterType.LONG, String.valueOf(card.getID()));
		addParameter("TopupValue", ParameterType.INT, String.valueOf(topup));
		
		Response response = RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
		if(response.getBodyElement().contains("OrderId")) {
			return new Order(response.getBodyElement());
		} else {
			return new Order();
		}
	}
	
	public Response topUpCardResponse(DetailedSmartcard card, long payReceipt, int topup) {
		addParameter("SmartcardId", ParameterType.LONG, String.valueOf(card.getID()));
		addParameter("TopupValue", ParameterType.INT, String.valueOf(topup));
		addParameter("MerchantReceipt", ParameterType.STRING, String.valueOf(payReceipt));
		
		return RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
	}
}
