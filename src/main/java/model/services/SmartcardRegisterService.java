package model.services;

import java.util.HashMap;

import model.actors.Authenticator;
import model.actors.Smartcard;
import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;

public class SmartcardRegisterService extends BaseService {

	private Authenticator requester;
	
	public SmartcardRegisterService(Authenticator requester) {
		super("/v1/smartcard/register/");
		this.requester = requester;
	}
	
	public String registerCard(Smartcard card){
		parameters = new HashMap<String, ParameterValue>();
		
		addParameter("SmartcardId", ParameterType.LONG, card.getID());
		addParameter("SmartcardSc", ParameterType.STRING, card.getCode());
		
		Response response = RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
		
		if(!response.getBodyElement().contains("SmartcardId")) {
			return "";
		}
		
		return response.getBodyElement().getValue("SmartcardId");
	}
	
	public Response registerCardResponse(Smartcard card) {
		parameters = new HashMap<String, ParameterValue>();
		
		addParameter("SmartcardId", ParameterType.LONG, card.getID());
		addParameter("SmartcardSc", ParameterType.STRING, card.getCode());
		
		return RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
	}
	
	public String registerCard(Smartcard card, String nick) {
		parameters = new HashMap<String, ParameterValue>();
		
		addParameter("SmartcardId", ParameterType.LONG, card.getID());
		addParameter("SmartcardSc", ParameterType.STRING, card.getCode());
		addParameter("CardNickname", ParameterType.STRING, nick);
		
		Response response = RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
		
		if(!response.getBodyElement().contains("SmartcardId")) {
			return "";
		}
		
		return response.getBodyElement().getValue("SmartcardId");
	}
}
