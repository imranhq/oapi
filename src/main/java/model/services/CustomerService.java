package model.services;

import java.util.ArrayList;
import java.util.HashMap;

import model.actors.Authenticator;
import model.datatypes.DetailedSmartcard;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;
import utilities.ResponseElement;

public class CustomerService extends BaseService {

	private Authenticator requester;
	
	public CustomerService(Authenticator requester) {
		super("/v1");
		this.requester = requester;
	}
	
	/**
	 * INCOMPLETE STANDARD.  RETURN A CUSTOMER DETAILS OBJECT.
	public ResponseElement requestDetails() {
		parameters = new HashMap<String, ParameterValue>();
		
		return RESTDriver.GET(this.getRequestUrl("/customer/details/"), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters).getBodyElement();
	}
	*/


	
	public Response requestDetailsResponse(HashMap<String, ParameterValue> overloadedParams) {
		parameters = new HashMap<String, ParameterValue>();
		
		//This gets called AFTER setting all the standard body parameters
		this.overloadParameters(overloadedParams);

		return RESTDriver.GET(this.getRequestUrl("/customer/details/"), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
	}
	
	public boolean hasLinkedSmartcards() {
		parameters = new HashMap<String, ParameterValue>();
		
		Response response = RESTDriver.GET(this.getRequestUrl("/customer/smartcards/"), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		
		if(response.getBodyElement().contains("SmartcardDetails") && response.getBodyElement().getChild("SmartcardDetails").isEmpty()) {
			return false;
		}
		
		ArrayList<ResponseElement> smartcards = response.getBodyElement().getList("SmartcardDetails");

		if(smartcards.size() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean hasCard(DetailedSmartcard card) {
		parameters = new HashMap<String, ParameterValue>();
		
		Response response = RESTDriver.GET(this.getRequestUrl("/customer/smartcards/"), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		
		if(!response.getBodyElement().contains("SmartcardDetails")) {
			return false;
		}
		
		ArrayList<ResponseElement> customerCards = response.getBodyElement().getList("SmartcardDetails");
		ArrayList<DetailedSmartcard> smartcards = new ArrayList<DetailedSmartcard>();
		
		for(ResponseElement element : customerCards) {
			smartcards.add(new DetailedSmartcard(element));
		}
		
		for(DetailedSmartcard smartcard : smartcards) {
			if(smartcard.getID() == card.getID()) {
				return true;
			}
		}
		
		//Didn't find.  Return false.
		return false;
	}
	
	public DetailedSmartcard getSmartcardDetails() {
		parameters = new HashMap<String, ParameterValue>();
		
		Response response = RESTDriver.GET(this.getRequestUrl("/customer/smartcards/"), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		
		if(!response.getBodyElement().contains("SmartcardDetails")) {
			return new DetailedSmartcard();
		}
		
		ArrayList<ResponseElement> customerCards = response.getBodyElement().getList("SmartcardDetails");
		ArrayList<DetailedSmartcard> smartcards = new ArrayList<DetailedSmartcard>();
		for(ResponseElement element : customerCards) {
			smartcards.add(new DetailedSmartcard(element));
		}
		
		//Work out which card to return
		if(smartcards.size() > 0) {
			return smartcards.get(0);
		} else {
			return new DetailedSmartcard();
		}
	}
	
	public Response requestPaymentDetails(HashMap<String, ParameterValue> overloadedParams) {
		parameters = new HashMap<String, ParameterValue>();
		
		//This gets called AFTER setting all the standard body parameters
		this.overloadParameters(overloadedParams);

		return RESTDriver.GET(this.getRequestUrl("/customer/paymentaccount/"), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
	}

}
