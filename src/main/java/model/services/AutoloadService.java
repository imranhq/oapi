package model.services;

import java.util.ArrayList;
import java.util.HashMap;

import model.actors.Authenticator;
import model.datatypes.AutoloadAmount;
import model.datatypes.TopUpAmount;
import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;
import utilities.ResponseElement;

public class AutoloadService extends BaseService {

	private Authenticator requester;
	
	public AutoloadService(Authenticator requester) {
		super("/v1/smartcard/autoload/");
		this.requester = requester;
	}
	
	public Response getAutoloadAmountsResponse(long cardId, HashMap<String, ParameterValue> params) {
		return getAutoloadAmountsResponse(String.valueOf(cardId), params);	
	}
	
	public Response getAutoloadAmountsResponse(String cardId, HashMap<String, ParameterValue> params) {
		resetParameters();
		overloadParameters(params);
		
		return RESTDriver.GET(this.getRequestUrl(cardId), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
	}
	
	public Response getAutoloadAmountsResponse(int fcc, HashMap<String, ParameterValue> params) {
		resetParameters();
		addParameter("fcc", ParameterType.INT, String.valueOf(fcc));
		return RESTDriver.GET(this.getRequestUrl(), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
	}
	
	public ArrayList<AutoloadAmount> getAutoloadAmounts(long cardId) {
		return getAutoloadAmounts(String.valueOf(cardId));
	}
	
	public ArrayList<AutoloadAmount> getAutoloadAmounts(int fcc) {
		Response response = getAutoloadAmountsResponse(fcc, new HashMap<String, ParameterValue>());
		ResponseElement body = response.getBodyElement();
		ArrayList<ResponseElement> elements = body.getList("AutoloadAmount");
		ArrayList<AutoloadAmount> amounts = new ArrayList<AutoloadAmount>();
		
		for(ResponseElement element: elements) {
			amounts.add(new AutoloadAmount(element));
		}
		
		return amounts;
	}
	
	public ArrayList<AutoloadAmount> getAutoloadAmounts(String cardId) {
		Response response = getAutoloadAmountsResponse(cardId, new HashMap<String, ParameterValue>());
		ResponseElement body = response.getBodyElement();
		
		ArrayList<ResponseElement> elements = body.getList("AutoloadAmount");
		ArrayList<AutoloadAmount> amounts = new ArrayList<AutoloadAmount>();
		
		for(ResponseElement element: elements) {
			amounts.add(new AutoloadAmount(element));
		}
		
		return amounts;
	}
	
	public Response setAutoloadAmountResponse(long cardId, int amount, HashMap<String, ParameterValue> params) {
		return setAutoloadAmountResponse(String.valueOf(cardId), String.valueOf(amount), params);
	}
	
	public Response setAutoloadAmountResponse(String cardId, String amount, HashMap<String, ParameterValue> params) {
		
		resetParameters();
		addParameter("SmartcardId", ParameterType.LONG, cardId);
		addParameter("AutoloadValue", ParameterType.INT, amount);
		overloadParameters(params);
		
		return RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
	}
	
	public String setAutoloadAmount(String cardId, String amount) {
		Response response = setAutoloadAmountResponse(cardId, amount, new HashMap<String, ParameterValue>());
		
		System.out.println(response.getBodyElement().toString());
		if(response.getBodyElement().contains("SmartcardId")) {
			return response.getBodyElement().getValue("SmartcardId");
		}
		
		return "";
	}
	
	public String setAutoloadAmount(long cardId, int amount) {
		return setAutoloadAmount(String.valueOf(cardId), String.valueOf(amount));
	}
	
	public Response deleteAutoloadAmountResponse(String cardId, HashMap<String, ParameterValue> params) {
		resetParameters();
		overloadParameters(params);
		
		Response response = RESTDriver.DELETE(this.getRequestUrl(cardId), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		System.out.println(response.getBodyElement().toString());
		return response;
	}
	
	public Response deleteAutoloadAmountResponse(long cardId, HashMap<String, ParameterValue> params) {
		return deleteAutoloadAmountResponse(String.valueOf(cardId), params);
	}
	
	public String deleteAutoloadAmount(String cardId) {
		Response response = deleteAutoloadAmountResponse(cardId, new HashMap<String, ParameterValue>());
		
		if(response.getBodyElement().contains("SmartcardId")) {
			return response.getBodyElement().getValue("SmartcardId");
		}
		
		return "";
	}
	
	public String deleteAutoloadAmount(long cardId) {
		return deleteAutoloadAmount(String.valueOf(cardId));
	}
}
