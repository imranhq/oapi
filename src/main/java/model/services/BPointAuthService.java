package model.services;

import model.actors.Authenticator;
import model.datatypes.DetailedSmartcard;
import model.datatypes.PaymentAuthority;
import model.response.ParameterType;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;

public class BPointAuthService extends BaseService {

	private Authenticator requester;
	
	public BPointAuthService(Authenticator requester) {
		super("/v1/bpoint/paymentauthtoken");
		this.requester = requester;
	}
	
	public PaymentAuthority getPaymentAuthority(DetailedSmartcard card, int amount) {
		addParameter("amt", ParameterType.INT, String.valueOf(amount));
		
		Response resp = RESTDriver.GET(this.getRequestUrl(String.format("/%s", card.getID())), RequestHeader.ENCODEDAUTH(requester.Token.accessToken), parameters);
		
		if(resp.getBodyElement().contains("PaymentAuthToken")) {
			return new PaymentAuthority(resp.getBodyElement());
		} else {
			System.out.println("WARNING: PaymentAuthToken not provided");
			return new PaymentAuthority();
		}
	}
}
