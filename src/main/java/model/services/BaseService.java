package model.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.Response;

/**
 * This class contains methods related to calling and parsing a webservice.
 * These methods and parameters are common to all webservices, such as the
 * domain name, client_id and client_secret.
 * @author Mike Fleig - mfleig@planittesting.com
 */
public abstract class BaseService {
	protected HashMap<String, ParameterValue> parameters; 
	protected String domain; 
	protected String URI;
	protected String client_id = "2420fede-e507-45dd-ac22-87360ca6ba77";
	protected String client_secret = "41cbdea8-a991-41fd-b045-860721394033";
	protected static final int AUTH_LIMIT = 36;	//Used to verify the length of auth tokens

	
	/**
	 * Constructor sets the domain for the webservice, and accepts a URI to be used by
	 * the service.
	 * @param URI should be of the format "/something/something/etc", eg. /oauth/token
	 */
	public BaseService(String URI) { 
		this.domain = "https://engtst-oapi.afc-itf.com.au:8443/openapi";
		this.URI = URI;
		parameters = new HashMap<String, ParameterValue>();
		
		Properties properties = new java.util.Properties();
		
		try {
			properties.load(new FileInputStream("src/main/resources/environment.properties"));  
			
			this.domain = properties.getProperty("OpenAPIEndpoint");
			this.client_id = properties.getProperty("client_id");
			this.client_secret = properties.getProperty("client_secret");
		// If we error out, just set a default to engtest
		} catch(FileNotFoundException fnfEx) {
			System.out.println("ERROR: Cannot location environment.properties");
			this.domain = "https://engtst-oapi.afc-itf.com.au:8443/openapi";
		} catch(IOException ioe) {
			System.out.println("ERROR: Cannot location environment.properties");
			this.domain = "https://engtst-oapi.afc-itf.com.au:8443/openapi";
		}		
	} 
	
	/**
	 * Adds a parameter to the webservice.  Parameters can either be included in the URL,
	 * or added as part of the header, depending on the method.
	 * They are stored in a HashMap so that they can be added to the URL on request
	 * @param key name of the parameter to be included
	 * @param value value associated with the parameter
	 */
	protected void addParameter(String key, ParameterType type, String value) { 
		if(parameters.containsKey(key)) { 
			parameters.replace(key, new ParameterValue(type, value)); 
		} else { 
			parameters.put(key, new ParameterValue(type, value)); 
		} 
	} 
	
	/**
	 * Same as addParameter(String, String) but includes conversion of value from
	 * boolean to String
	 * @param key name of the parameter to be included
	 * @param value boolean representation of the value associated with the parameter
	 */
	protected void addParameter(String key, boolean value) { 
		if(parameters.containsKey(key)) { 
			parameters.replace(key, new ParameterValue(ParameterType.BOOLEAN, String.valueOf(value))); 
		} else { 
			parameters.put(key, new ParameterValue(ParameterType.BOOLEAN, String.valueOf(value))); 
		} 
	} 
	
	/**
	 * Same as addParameter(String, String) but includes conversion of value from
	 * int to String
	 * @param key name of the parameter to be included
	 * @param value int representation of the value associated with the parameter
	 */
	protected void addParameter(String key, int value) { 
		if(parameters.containsKey(key)) { 
			parameters.replace(key, new ParameterValue(ParameterType.INT, String.valueOf(value))); 
		} else { 
			parameters.put(key, new ParameterValue(ParameterType.INT, String.valueOf(value))); 
		} 
	} 
	
	/**
	 * Replaces existing parameters with a new empty {@literal HashMap<String, ParameterValue>}
	 */
	protected void resetParameters() { 
		parameters = new HashMap<String, ParameterValue>(); 
	} 
	
	//returns true if not null and equal to 'true', 
	//returns false otherwise 
	/**
	 * Returns a boolean representation of a String value in parameters.
	 * Returns true of not null and equal to true.  Returns false otherwise.
	 * @param key name of the parameter requested
	 * @return A boolean representation of the requested parameter.
	 */
	protected boolean getBooleanParameter(String key) { 
		return Boolean.parseBoolean(parameters.get(key).getValue()); 
	} 
	
	//returns a valid int if possible 
	//returns -1 if not valid 
	/**
	 * Returns an int representation of a String value in parameters.
	 * Returns a valid int if possible, or -1 if not possible.
	 * @param key name of the parameter requested
	 * @return an int representation of the requested parameter
	 */
	protected int getIntParameter(String key) { 
		try { 
			return Integer.parseInt(parameters.get(key).getValue()); 
		} catch(NumberFormatException nfe) { 
			return -1; 
		} 
	} 
	
	/**
	 * Returns the current domain of the service
	 * @return The current Domain, eg. "http://www.webservice.com/api/"
	 */
	protected String getDomain() { 
		return domain; 
	} 
	
	/**
	 * Concatenates Domain, URI and returns a String URL
	 * @return A concatenation of Domain and URI.
	 */
	protected String getRequestUrl() { 
		return getRequestUrl("");
	}
	
	/**
	 * Same as getRequestUrl(), but allows an extra URI to be appended to the request URL
	 * @param additionalURI extra URI to be appended
	 * @return A concatenation of Domain, URI and additionalURI, eg. "http://www.twitter.com/api/tweet/user/"
	 */
	protected String getRequestUrl(String additionalURI) {
		return domain + URI + additionalURI;
	}
	
	/**
	 * Used for basic authorization
	 * @return {@literal {client_id}:{client_secret}}, based on the client_id and client_secret of the auth service.
	 */
	protected String getClientString() {
		return String.format("%s:%s", client_id, client_secret);
	}
	
	protected void overloadParameters(HashMap<String, ParameterValue> overloadedParams) {
		if(!overloadedParams.isEmpty()) {
			for(String key : overloadedParams.keySet()) {
				addParameter(key, overloadedParams.get(key).getType(), overloadedParams.get(key).getValue());
			}
		}
	}
	
	protected boolean responseContainsRequiredParameters(Response resp, List<String> requiredParameters) {

		for(String parameter : requiredParameters) {
			if(!resp.getBodyElement().contains(parameter)) {
				System.out.println(String.format("WARNING: Required response field %s is missing", parameter));
				return false;
			}
		}
		
		return true;
	}
}

