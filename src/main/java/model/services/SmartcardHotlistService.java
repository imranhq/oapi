package model.services;

import java.util.HashMap;

import model.actors.Authenticator;
import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;

public class SmartcardHotlistService extends BaseService{
	private Authenticator requester;
	
	public SmartcardHotlistService(Authenticator requester) {
		super("/v1/smartcard/hotlist/");
		this.requester = requester;
	}
	
	public Response hotlistCardResponse(long cardId, String reason) {
		parameters = new HashMap<String, ParameterValue>();
		addParameter("SmartcardId", ParameterType.LONG, String.valueOf(cardId));
		
		if(reason != null && reason.length() > 0) {
			addParameter("HotlistReason", ParameterType.STRING, reason);
		}
		
		return RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
	}
	
	public Response hotlistCardResponse(long cardId) {
		return hotlistCardResponse(cardId , "");
	}
}
