package model.services;

import java.util.HashMap;

import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;
import utilities.ResponseElement;

public class SmartcardService extends BaseService {

	public SmartcardService() {
		super("/v1/smartcard");
	}
	
	public ResponseElement requestDetails(String smartcardId, String accessToken) {
		
		Response response = requestDetailsResponse(smartcardId, accessToken, new HashMap<String, ParameterValue>());
		return response.getBodyElement();
	}
	
	public Response requestDetailsResponse(String smartcardId, String accessToken, HashMap<String, ParameterValue> overloadedParams) {
		this.overloadParameters(overloadedParams);
		return RESTDriver.GET(this.getRequestUrl(String.format("/details/%s", smartcardId)), RequestHeader.ENCODEDAUTH(accessToken), parameters);
	}
	
	public ResponseElement registerSmartcard(String accessToken, String scid, String scsn, String nick) {
		Response response = registerSmartcardResponse(accessToken, scid, scsn, nick, new HashMap<String, ParameterValue>());
		return response.getBodyElement();
	}
	
	public Response registerSmartcardResponse(String accessToken, String scid, String scsn, String nick, HashMap<String, ParameterValue> overloadedParams) {
		
		addParameter("CardNickname", ParameterType.STRING, nick);
		addParameter("SmartcardSc", ParameterType.STRING, scsn);
		addParameter("SmartcardId", ParameterType.LONG, scid);
		
		this.overloadParameters(overloadedParams);
		
		return RESTDriver.POST(this.getRequestUrl("/register/"), RequestHeader.JSONAUTH(accessToken), parameters);
	}
}
