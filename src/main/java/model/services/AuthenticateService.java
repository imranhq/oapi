package model.services;

import java.net.HttpURLConnection;
import java.util.HashMap;

import model.actors.Authenticator;
import model.actors.MobileUser;
import model.actors.Smartcard;
import model.datatypes.Token;
import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;
import utilities.ResponseElement;

/**
 * Authenticate Service deals with authentication calls and handling
 * Tokens.
 * The URI used is "/auth/oauth/v2/token"
 * @author Mike Fleig - mfleig@planittesting.com
 *
 */
public class AuthenticateService extends BaseService {
	
	/**
	 * Sets Domain and URI with default URI
	 */
	public AuthenticateService() {
		super("/auth/oauth/v2");
	}
	
	/**
	 * Sets Domain and URI with custom URI
	 * @param URI should be of the format "/something/something/etc", eg. /oauth/token
	 */
	public AuthenticateService(String URI) {
		super(URI);
	}
	
	/**
	 * Sets parameters required for all token requests
	 */
	private void setStandardParameters() {
		parameters = new HashMap<String, ParameterValue>();
	    addParameter("client_id", ParameterType.STRING, client_id);
	    addParameter("client_secret", ParameterType.STRING, client_secret);
	    addParameter("scope", ParameterType.STRING, "openapi");
	}
	
	/**
	 * Requests a sessionID from the endpoint.  Part 1 of 3 for 3Legged authentication
	 * @return The sessionID from the endpoint if successful.  Null if not.
	 */
	public String requestSessionID() {
		
		parameters = new HashMap<String, ParameterValue>();
		addParameter("client_id", ParameterType.STRING, client_id);
		addParameter("grant_type", ParameterType.STRING, "authorization_code");
		addParameter("scope", ParameterType.STRING, "openapi");
		addParameter("redirect_uri", ParameterType.STRING, "https://engtst-oapi.afc-itf.com.au:8443/openapi/v1/callback");
		addParameter("state", ParameterType.STRING, "test");
		addParameter("response_type", ParameterType.STRING, "code");
		
		Response htmlResponse = RESTDriver.POST(this.getRequestUrl("/authorize"), RequestHeader.STANDARD(), parameters);
		
		String sessionID = htmlResponse.getSessionID();
		
		if(sessionID != null && sessionID.length() == AUTH_LIMIT) {
			return sessionID;
		} else {
			return "";
		}
	}
	
	/**
	 * Related to {@link AuthenticateService#requestSessionID()}
	 * @param overloadParams Parameters for the request body to be overloaded (for negative testing)
	 * @return A {@see Response} from the endpoint.
	 */	
	public Response requestSessionIDResponse(HashMap<String, ParameterValue> overloadedParams) {
		parameters = new HashMap<String, ParameterValue>();
		addParameter("client_id", ParameterType.STRING, client_id);
		addParameter("grant_type", ParameterType.STRING, "authorization_code");
		addParameter("scope", ParameterType.STRING, "openapi");
		addParameter("redirect_uri", ParameterType.STRING, "https://engtst-oapi.afc-itf.com.au:8443/openapi/v1/callback");
		addParameter("state", ParameterType.STRING, "test");
		addParameter("response_type", ParameterType.STRING, "code");
		
		this.overloadParameters(overloadedParams);
		
		return RESTDriver.POST(this.getRequestUrl("/authorize"), RequestHeader.STANDARD(), parameters);
	}
	
	/**
	 * Requests an Authorization code based off user credentials and a valid session ID.  Part 2/3 of 3-Legged authentication.
	 * @param requester User whose credentials will request an authorization code.
	 * @param sessionID Valid session code from Part 1 of 3-Legged authentication.
	 * @return An authorization code to be used in Part 3/3 of authentication process.
	 */
	public String requestAuthorizationCode(Authenticator requester, String sessionID) {
		resetParameters();
		addParameter("action", ParameterType.STRING, "Grant");
		addParameter("sessionID", ParameterType.STRING, sessionID);
		addParameter("persistent_cookie", ParameterType.STRING, "");
		
		if(requester instanceof Smartcard) {
			addParameter("smartcardId", ParameterType.STRING, requester.getID());
			addParameter("smartcardSc", ParameterType.STRING, requester.getCode());
		}
		
		if(requester instanceof MobileUser) {
			addParameter("username", ParameterType.STRING, requester.getID());
			addParameter("password", ParameterType.STRING, requester.getCode());
		}
		
		Response restResponse = RESTDriver.POST(this.getRequestUrl("/authorize"), RequestHeader.STANDARD(), parameters);
		
		if(restResponse.getResponseCode() >= HttpURLConnection.HTTP_BAD_REQUEST) {
			return "";
		}
		
		return restResponse.getMessageElement().getValue("code");
	}
	
	/**
	 * Requests an Authorization code based off user credentials and a valid session ID.  Part 2/3 of 3-Legged authentication.
	 * @param requester User whose credentials will request an authorization code.
	 * @param sessionID Valid session code from Part 1 of 3-Legged authentication.
	 * @param overloadedParams Parameters for the request body to be overloaded (for negative testing)
	 * @return A {@see Response} from the endpoint.
	 */
	public Response requestAuthorizationCodeResponse(Authenticator requester, String sessionID, HashMap<String, ParameterValue> overloadedParams) {
		resetParameters();
		addParameter("action", ParameterType.STRING, "Grant");
		addParameter("sessionID", ParameterType.STRING, sessionID);
		addParameter("persistent_cookie", ParameterType.STRING, "");
		
		if(requester instanceof Smartcard) {
			addParameter("smartcardId", ParameterType.STRING, requester.getID());
			addParameter("smartcardSc", ParameterType.STRING, requester.getCode());
		}
		
		if(requester instanceof MobileUser) {
			addParameter("username", ParameterType.STRING, requester.getID());
			addParameter("password", ParameterType.STRING, requester.getCode());
		}
		
		this.overloadParameters(overloadedParams);
		
		return RESTDriver.POST(this.getRequestUrl("/authorize"), RequestHeader.STANDARD(), parameters);
	}
	
	/**
	 * Part 3/3 for 3-Legged Authentication process.  Provides an access Token for the requester if successful.
	 * @param authCode See {@link AuthenticateService#requestAuthorizationCode(Authenticator, String)}
	 * @return A Token if successful.  The token will be empty if there's a business error.  It'll be null if the request fails.
	 */
	public Token requestAccessToken(String authCode) {
		parameters = new HashMap<String, ParameterValue>();
		addParameter("client_id", ParameterType.STRING, client_id);
		addParameter("client_secret", ParameterType.STRING, client_secret);
		addParameter("grant_type", ParameterType.STRING, "authorization_code");
		addParameter("redirect_uri", ParameterType.STRING, "https://engtst-oapi.afc-itf.com.au:8443/openapi/v1/callback");
		addParameter("code", ParameterType.STRING, authCode);
		
		ResponseElement response = RESTDriver.POST(this.getRequestUrl("/token"), RequestHeader.STANDARD(), parameters).getMessageElement();
		
		Token newToken = new Token();
		
		newToken.accessToken = response.getValue("access_token");
		newToken.tokenType = response.getValue("token_type");
		newToken.expiresIn = response.getIntValue("expires_in");
		newToken.refreshToken = response.getValue("refresh_token");
		newToken.scope = response.getValue("scope");
		
		return newToken;
	}
	
	/**
	 * Part 3/3 for 3-Legged Authentication process.  Provides an access Token for the requester if successful.
	 * @param authCode See {@link AuthenticateService#requestAuthorizationCode(Authenticator, String)}
	 * @param overloadedParams Parameters for the request body to be overloaded (for negative testing)
	 * @return A Token if successful.  The token will be empty if there's a business error.  It'll be null if the request fails.
	 */
	public Response requestAccessTokenResponse(String authCode, HashMap<String, ParameterValue> overloadedParams) {
		parameters = new HashMap<String, ParameterValue>();
		addParameter("client_id", ParameterType.STRING, client_id);
		addParameter("client_secret", ParameterType.STRING, client_secret);
		addParameter("grant_type", ParameterType.STRING, "authorization_code");
		addParameter("redirect_uri", ParameterType.STRING, "https://engtst-oapi.afc-itf.com.au:8443/openapi/v1/callback");
		addParameter("code", ParameterType.STRING, authCode);
		
		this.overloadParameters(overloadedParams);
		
		return RESTDriver.POST(this.getRequestUrl("/token"), RequestHeader.STANDARD(), parameters);
	}

	/**
	 * Attempts to revoke the access or refresh token
	 * @param token access or refresh code to be revoked
	 * @param tokenTypeHint method to determine which code to revoke (refresh_token or access_token)
	 * @return True/False, depending on whether revoke was successful
	 */
	public boolean revokeToken(String token, String tokenTypeHint) {
		
		parameters = new HashMap<String, ParameterValue>();
		
		//token
		addParameter("token", ParameterType.STRING, token);
		addParameter("token_type_hint", ParameterType.STRING, tokenTypeHint);
		
		Response jsonResponse = RESTDriver.DELETE(this.getRequestUrl("/token/revoke"), RequestHeader.BASICAUTH(getClientString()), parameters);

		ResponseElement response = jsonResponse.getBodyElement();
		
		if(response.getValue("result").equalsIgnoreCase("revoked")) {
			return true;
		} else {
			return false;
		}
	}
	
	public Response revokeTokenResponse(String token, String tokenTypeHint, HashMap<String, ParameterValue> overloadedParams) {
		parameters = new HashMap<String, ParameterValue>();
		
		//token
		addParameter("token", ParameterType.STRING, token);
		addParameter("token_type_hint", ParameterType.STRING, tokenTypeHint);
		
		this.overloadParameters(overloadedParams);
		
		return RESTDriver.DELETE(this.getRequestUrl("/token/revoke"), RequestHeader.BASICAUTH(getClientString()), parameters);
	}
	
	/**
	 * Makes an attempt to use the Token Refresh service
	 * @param refresh_token The refresh_token code that will be called
	 * @return A token representing the successful refresh
	 */
	public Token refreshToken(String refresh_token) {
		resetParameters();
		setStandardParameters();
		addParameter("grant_type", ParameterType.STRING, "refresh_token");
		addParameter("refresh_token", ParameterType.STRING, refresh_token);
		
		ResponseElement response = RESTDriver.POST(this.getRequestUrl("/token"), RequestHeader.STANDARD(), parameters).getBodyElement();
		
		Token newToken = new Token();
		
		newToken.accessToken = response.getValue("access_token");
		newToken.tokenType = response.getValue("token_type");
		newToken.expiresIn = response.getIntValue("expires_in");
		newToken.refreshToken = response.getValue("refresh_token");
		newToken.scope = response.getValue("scope");
		
		return newToken;
	}
	
	public Response refreshTokenResponse(String refresh_token, HashMap<String, ParameterValue> overloadedParams) {
		resetParameters();
		setStandardParameters();
		addParameter("grant_type", ParameterType.STRING, "refresh_token");
		addParameter("refresh_token", ParameterType.STRING, refresh_token);
		
		this.overloadParameters(overloadedParams);
		
		return RESTDriver.POST(this.getRequestUrl("/token"), RequestHeader.STANDARD(), parameters);
	}
	
	public Token twoLeggedAuth(Authenticator requester) {
		resetParameters();
		setStandardParameters();
		addParameter("grant_type", ParameterType.STRING, "password");
		
		if(requester instanceof Smartcard) {
			addParameter("smartcardId", ParameterType.STRING, requester.getID());
			addParameter("smartcardSc", ParameterType.STRING, requester.getCode());
		}
		
		if(requester instanceof MobileUser) {
			addParameter("username", ParameterType.STRING, requester.getID());
			addParameter("password", ParameterType.STRING, requester.getCode());
		}
		
		Response response = RESTDriver.POST(this.getRequestUrl("/token"), RequestHeader.STANDARD(), parameters);
		Token token;
		if(response.getBodyElement().contains("access_token")) {
			token = new Token(response.getBodyElement());
		} else {
			token = new Token();
		}
		
		return token;
	}
	
	public Response twoLeggedAuthResponse(Authenticator requester, HashMap<String, ParameterValue> params) {
		resetParameters();
		setStandardParameters();
		addParameter("grant_type", ParameterType.STRING, "password");
		
		if(requester instanceof Smartcard) {
			addParameter("smartcardId", ParameterType.STRING, requester.getID());
			addParameter("smartcardSc", ParameterType.STRING, requester.getCode());
		}
		
		if(requester instanceof MobileUser) {
			addParameter("username", ParameterType.STRING, requester.getID());
			addParameter("password", ParameterType.STRING, requester.getCode());
		}
		
		overloadParameters(params);
		
		return RESTDriver.POST(this.getRequestUrl("/token"), RequestHeader.STANDARD(), parameters);
		
	}
}
