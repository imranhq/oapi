package model.services;

import model.actors.Authenticator;
import model.datatypes.BPointPayReference;
import model.datatypes.DetailedSmartcard;
import model.response.ParameterType;
import utilities.RESTDriver;
import utilities.RequestHeader;
import utilities.Response;

public class PaymentService extends BaseService {

	private Authenticator requester;
	
	public PaymentService(Authenticator requester) {
		super("/v1/bpoint/paymenttransaction/");
		this.requester = requester;
	}
	
	public long processPayment(DetailedSmartcard card, BPointPayReference bpPayRef) {		
		addParameter("SmartcardId", ParameterType.LONG, String.valueOf(card.getID()));
		addParameter("AmountInCents", ParameterType.INT, bpPayRef.getValue("out_amount"));
		addParameter("MerchantReceipt", ParameterType.STRING, bpPayRef.getValue("out_merchant_reference"));
		addParameter("PaymentProviderResponse", ParameterType.CUSTOM, bpPayRef.toJSON());
		Response response = RESTDriver.POST(this.getRequestUrl(), RequestHeader.JSONAUTH(requester.Token.accessToken), parameters);
		
		return response.getBodyElement().getLongValue("MerchantReceipt");
	}
}
