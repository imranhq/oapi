package model.actors;

/**
 * Concept of a 'User' - ie someone with a username or password.
 * Every type of user with a username or password should inherit this.
 * @author Mike Fleig - mfleig@planittesting.com
 *
 */
public abstract class User extends Authenticator {
	
	/**
	 * Creates a user with a given name and password.
	 * Sets a blank list of Smartcards to be populated.
	 * @param username name of the user
	 * @param password password of the user
	 */
	public User(String username, String password) {
		super(username, password);
	}
}
