package model.actors;

import java.util.ArrayList;

import model.datatypes.DetailedSmartcard;
import model.datatypes.Token;

/**
 * A concept representing a requester who wishes to authenticate.
 * This could be a Mobile User, Smart card, etc.
 * The Authenticator is provided a Token when they authenticate
 * Each Authenticator has an id and security code combination.  This could be a
 * SmartcardID/SecurityNumber combination or Username/Password.
 * @author Mike Fleig - mfleig@planittesting.com
 *
 */
public abstract class Authenticator {

	/**
	 * Represents the ID of the requester.  Could be username, could be SmartcardID
	 */
	private String id;
	
	/**
	 * Represents the Code of the requester.  Could be password, could be SecurityNumber
	 */
	private String code;
	
	/**
	 * Access Token of the user who connects.
	 */
	public Token Token;

	/**
	 * A list of Smartcards registered to a user
	 */
	protected ArrayList<DetailedSmartcard> cards;
	
	/**
	 * Adds a Smartcard to the list of Smartcards
	 * @param card Smartcard to be added to their list of Smartcards
	 */
	public void addCard(DetailedSmartcard card) {
		if(cards != null) {
			cards.add(card);
		}
	}
	
	public DetailedSmartcard getCard(){
		if(cards.isEmpty()) {
			return null;
		} else {
			return cards.get(0);
		}
	}

	/**
	 * Basic constructor.  Also creates a blank Token to be populated
	 * @param id {@link Authenticator#id}
	 * @param code {@link Authenticator#code}
	 */
	public Authenticator(String id, String code) {
		this.cards = new ArrayList<DetailedSmartcard>();
		this.id = id;
		this.code = code;
		this.Token = new Token();
	}
	
	/**
	 * Returns the ID of the Authenticator
	 * @return {@link Authenticator#id}
	 */
	public String getID() {
		return id;
	}
	
	/**
	 * Returns the code of the Authenticator
	 * @return {@link Authenticator#code}
	 */
	public String getCode() { 
		return code;
	}
	
	public void setID(String id) {
		this.id = id;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
}
