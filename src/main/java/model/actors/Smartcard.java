package model.actors;

import model.datatypes.DetailedSmartcard;

/**
 * Contains details attached to a given Smartcard.
 * @author Mike
 *
 */
public class Smartcard extends Authenticator{
	
	/**
	 * Creates a basic Smartcard
	 * @param cardId 16 digit Smartcard number (starting with 308522)
	 * @param securityNumber 4 digit security code attached to the card
	 */
	public Smartcard(String cardId, String securityNumber) {
		super(cardId, securityNumber);
		cards.add(new DetailedSmartcard(cardId));
	}
	
	public Smartcard() {
		super("", "");
		cards.add(new DetailedSmartcard());
	}
}
