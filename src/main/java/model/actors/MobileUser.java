package model.actors;

/**
 * Represents methods unique to a user who logs in from a mobile phone.
 * @author Mike Fleig - mfleig@planittesting.com
 *
 */
public class MobileUser extends User {

	/**
	 * Creates a smart phone user with username and password
	 * @param username Username of the User
	 * @param password Password of the User
	 */
	public MobileUser(String username, String password) {
		super(username, password);
	}
}
