package model.response;

public enum ParameterType {

	STRING, INT, BOOLEAN, LONG, CUSTOM, LIST
}
