package model.response;

public class ParameterValue {

	private ParameterType type;
	private String value;
	
	public ParameterValue(ParameterType type, String value) {
		this.type = type;
		this.value = value;
	}
	
	public ParameterType getType() {
		return type;
	}
	
	public String getValue() {
		return value;
	}
}
