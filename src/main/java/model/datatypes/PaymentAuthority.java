package model.datatypes;

import utilities.ResponseElement;

public class PaymentAuthority {

	private String paymentAuthToken;
	private long merchantReceipt;
	private long merchantNumber;
	private String crn1;
	private String crn2;
	
	public PaymentAuthority(ResponseElement element) {
		this.paymentAuthToken = element.getValue("PaymentAuthToken");
		this.merchantReceipt = element.getLongValue("MerchantReceipt");
		this.merchantNumber = element.getLongValue("MerchantNumber");
		this.crn1 = element.getValue("Crn1");
		this.crn2 = element.getValue("Crn2");
	}
	
	public PaymentAuthority() {
		this.paymentAuthToken = "";
		this.merchantReceipt = 0;
		this.merchantNumber = 0;
		this.crn1 = "";
		this.crn2 = "";
	}
	
	public String getPaymentAuthToken(){
		return this.paymentAuthToken;
	}
	
	public long getMerchantReceipt() {
		return this.merchantReceipt;
	}
	
	public long getMerchantNumber() {
		return this.merchantNumber;
	}
	
	public String getCRN1() {
		return this.crn1;
	}
	
	public String getCRN2() {
		return this.crn2;
	}
}
