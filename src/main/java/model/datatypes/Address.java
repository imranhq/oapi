package model.datatypes;

import utilities.ResponseElement;

public class Address {
	
	private String address1;
	private String address2;
	private String address3;
	private String postcode;
	private String suburb;
	private String state;
	private String country;
	
	public Address(ResponseElement address) {
		this.address1 = address.getValue("Address1");
		this.address2 = address.getValue("Address2");
		this.address3 = address.getValue("Address3");
		this.postcode = address.getValue("Postcode");
		this.suburb = address.getValue("Suburb");
		this.state = address.getValue("State");
		this.country = address.getValue("Country");
	}
	
	public Address() {
		this.address1 = "";
		this.address2 = "";
		this.address3 = "";
		this.postcode = "";
		this.suburb = "";
		this.state = "";
		this.country = "";
	}
	
	public Address(String address1, String address2, String address3, String postcode, String suburb, String state, String country){
		this.address1 = address1;
		this.address2 = address2;
		this.address3 = address3;
		this.postcode = postcode;
		this.suburb = suburb;
		this.state = state;
		this.country = country;
	}
	
	public String getAddress1() {
		return this.address1;
	}
	
	public String getAddress2() {
		return this.address2;
	}
	
	public String getAddress3() {
		return this.address3;
	}
	
	public String getPostcode() {
		return this.postcode;
	}
	
	public String getSuburb() {
		return this.suburb;
	}
	
	public String getState() {
		return this.state;
	}
	
	public String getCountry() {
		return this.country;
	}
}
