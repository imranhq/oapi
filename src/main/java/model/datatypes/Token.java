package model.datatypes;

import utilities.ResponseElement;

/**
 * DataType representing an Access Token
 * @author Mike
 *
 */
public class Token {
	
	/**
	 * The current access token.  Can be null if not set.
	 */
	public String accessToken;

	/**
	 * The current refresh token.  Can be null if not set.
	 */
	public String refreshToken;
	
	/**
	 * The current token type (usually "Bearer").  Can be null if not set.
	 */
	public String tokenType;
	
	/**
	 * The time out on the tokens in seconds.
	 */
	public int expiresIn;
	
	/**
	 * If set, is set to "openapi".
	 */
	public String scope;
	 
	/**
	 * When authenticated, a token is generated.  The details of the token are stored for future verification/use.
	 * @param accessToken {@link Token#accessToken}
	 * @param refreshToken {@link Token#refreshToken}
	 * @param tokenType {@link Token#tokenType}
	 * @param expiresIn {@link Token#expiresIn}
	 * @param scope {@link Token#scope}
	 */
	public Token(String accessToken, String refreshToken, String tokenType, int expiresIn, String scope) {
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.tokenType = tokenType;
		this.expiresIn = expiresIn;
		this.scope = scope;
	}
	
	public Token(ResponseElement element) {
		this.accessToken = element.getValue("access_token");
		this.refreshToken = element.getValue("refresh_token");
		this.tokenType = element.getValue("token_type");
		this.expiresIn = element.getIntValue("expires_in");
		this.scope = element.getValue("scope");
	}
	
	/**
	 * Sets a blank Token.
	 */
	public Token() {
		this.accessToken = "";
		this.refreshToken = "";
		this.tokenType = "";
		this.expiresIn = 0;
		this.scope = "";
	}
	
	public String toString() {
		return String.format("access_token: %s\nrefresh_token: %s\ntoken_type: %s\n", accessToken, refreshToken, tokenType);
	}
}
