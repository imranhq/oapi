package model.datatypes;

import utilities.ResponseElement;

public class CustomerDetails {
	
	private String Email;
	private String PIN;
	private String Title;
	private String FirstName;
	private String LastName;
	private long Telephone;
	private long Mobile;
	private String Address1;
	private String Address2;
	private String Address3;
	private String Suburb;
	private int Postcode;
	private String State;
	private String Country;
	
	
	
	public CustomerDetails(ResponseElement details) {
		
		this.Email = details.getValue("Email");
		this.PIN = details.getValue("PIN");
		this.Title = details.getValue("Title");
		this.FirstName = details.getValue("FirstName");
		this.LastName = details.getValue("LastName");
		this.Telephone = details.getLongValue("Telephone");
		this.Mobile = details.getLongValue("Mobile");
		this.Address1 = details.getValue("Address1");
		this.Address2 = details.getValue("Address2");
		this.Address3 = details.getValue("Address3");
		this.Suburb = details.getValue("Suburb");
		this.Postcode = details.getIntValue("Postcode");
		this.State = details.getValue("State");
		this.Country = details.getValue("Country");
	
	}
		
	
	

	public CustomerDetails(String Email, String PIN) {
		// TODO Auto-generated constructor stub
		this.Email = Email;
		this.PIN = PIN;
	}


	public String getEmail()
	{
		return this.Email;
	}
	

	public String getPIN()
	{
		return this.PIN;
	}
	
	public String getTitle()
	{
		return this.Title;
	}

	
	public String getFirstName()
	{
		return this.FirstName;
	}

	
	public String getLastName()
	{
		return this.LastName;
	}

	
	public long getTelephone()
	{
		return this.Telephone;
	}
	

	public long getMobile()
	{
		return this.Mobile;
	}
	
	
	public String getAddress1()
	{
		return this.Address1;
	}
	

	public String getAddress2()
	{
		return this.Address2;
	}
	
	
	
	public String getAddress3()
	{
		return this.Address3;
	}
	
	
	public String getSuburb()
	{
		return this.Suburb;
	}
	
	
	public int getPostcode()
	{
		return this.Postcode;
	}


	public String getState()
	{
		return this.State;
	}

	public String getCountry()
	{
		return this.Country;
	}

	

}
