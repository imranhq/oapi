package model.datatypes;

import model.actors.Smartcard;
import utilities.ResponseElement;

public class DetailedSmartcard {

	private boolean pendingBalanceActivation;
	private int autoloadAmount;
	private String nickname;
	private String state;
	private String typeDescription;
	private String expiry;
	private int code;
	private int svBalance;
	private long id;

	public DetailedSmartcard(ResponseElement element) {
		this.pendingBalanceActivation = element.getBooleanValue("PendingBalanceActivation");
		this.autoloadAmount = element.getIntValue("AutoLoadAmount");
		this.code = element.getIntValue("CardTypeCode");
		this.svBalance = element.getIntValue("SVBalance");
		this.id = element.getLongValue("SmartcardId");
		this.state = element.getValue("CardState");
		
		if(element.contains("CardTypeDescription")) {
			this.typeDescription = element.getValue("CardTypeDescription");
		} else {
			this.typeDescription = "";
		}
		
		if(element.contains("CardExpiryDate")) {
			this.expiry = element.getValue("CardExpiryDate");
		} else {
			this.expiry = "";
		}
		
		if(element.contains("CardNickName")) {
			this.nickname = element.getValue("CardNickName");
		} else {
			this.nickname = "";
		}
	}
	
	public DetailedSmartcard() {
		this.pendingBalanceActivation = false;
		this.autoloadAmount = -1;
		this.nickname = "";
		this.state = "";
		this.typeDescription = "";
		this.expiry = "";
		this.code = -1;
		this.svBalance = -1;
		this.id = -1;
	}
	
	public DetailedSmartcard(String id) {
		this.pendingBalanceActivation = false;
		this.autoloadAmount = -1;
		this.nickname = "";
		this.state = "";
		this.typeDescription = "";
		this.expiry = "";
		this.code = -1;
		this.svBalance = -1;
		this.id = Long.parseLong(id);
	}
	
	public DetailedSmartcard(String id, String nickname) {
		this.pendingBalanceActivation = false;
		this.autoloadAmount = -1;
		this.nickname = nickname;
		this.state = "";
		this.typeDescription = "";
		this.expiry = "";
		this.code = -1;
		this.svBalance = -1;
		this.id = Long.parseLong(id);
	}
	
	public DetailedSmartcard(Smartcard card) {
		this.pendingBalanceActivation = false;
		this.autoloadAmount = -1;
		this.nickname = "";
		this.state = "";
		this.typeDescription = "";
		this.expiry = "";
		this.code = -1;
		this.svBalance = -1;
		this.id = Long.parseLong(card.getID());
	}

	public boolean isActivated() {
		return pendingBalanceActivation;
	}
	
	public int getAutoloadAmount() {
		return this.autoloadAmount;
	}
	
	public String getNickName() {
		return this.nickname;
	}
	
	public String getCardState() {
		return this.state;
	}
	
	public String getCardTypeDescription() {
		return this.typeDescription;
	}
	
	public String getExpiry() {
		return this.expiry;
	}
	
	public int getCardTypeCode() {
		return this.code;
	}
	
	public int getSVBalance() {
		return this.svBalance;
	}
	
	public long getID() {
		return this.id;
	}
}
