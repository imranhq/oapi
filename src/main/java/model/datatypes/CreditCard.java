package model.datatypes;

public class CreditCard {

	private long number;
	private int cvv;
	private String expiryYear;
	private String expiryDate;
	
	public CreditCard(long number, int cvv, String expiryYear, String expiryDate) {
		this.number = number;
		this.cvv = cvv;
		this.expiryYear = expiryYear;
		this.expiryDate = expiryDate;
	}
	
	public long getNumber() {
		return this.number;
	}
	
	public int getCVV() {
		return this.cvv;
	}
	
	public String getExpiryYear() {
		return this.expiryYear;
	}
	
	public String getExpiryDate() {
		return this.expiryDate;
	}
}
