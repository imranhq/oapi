package model.datatypes;

import utilities.ResponseElement;

public class Order {

	private long smartcardId;
	private long orderId;
	
	public Order(ResponseElement element) {
		this.smartcardId = element.getLongValue("SmartcardId");
		this.orderId = element.getLongValue("OrderId");
	}
	
	public Order() {
		this.smartcardId = -1;
		this.orderId = -1;
	}
	
	public long getSmartcardId() {
		return this.smartcardId;
	}
	
	public long getOrderId() {
		return this.orderId;
	}
	
	public String toString() {
		return String.format("SmartcardId: %s\n OrderId: %s\n", this.smartcardId, this.orderId);
	}
}
