package model.datatypes;

import utilities.ResponseElement;

public class PaymentAccount {
	
	private String BankCardType;
	private int ExpiryMonth;
	private String MaskedAccountNumber;
	private String AccountStatus;
	private int ExpiryYear ;
	private String AccountHolderName;

	
	public PaymentAccount(ResponseElement payment) {
		
		this.BankCardType = payment.getValue("BankCardType");
		this.ExpiryMonth = payment.getIntValue("ExpiryMonth");
		this.MaskedAccountNumber = payment.getValue("MaskedAccountNumber");
		this.AccountStatus = payment.getValue("AccountStatus");
		this.ExpiryYear = payment.getIntValue("ExpiryYear");
		this.AccountHolderName = payment.getValue("AccountHolderName");
	
	}

	
	public PaymentAccount(String BankCardType, int ExpiryMonth, String MaskedAccountNumber, String AccountStatus,
			int ExpiryYear, String AccountHolderName) {
		// TODO Auto-generated constructor stub

		this.BankCardType = BankCardType;
		this.ExpiryMonth = ExpiryMonth;
		this.MaskedAccountNumber = MaskedAccountNumber;
		this.AccountStatus = AccountStatus;
		this.ExpiryYear = ExpiryYear;
		this.AccountHolderName = AccountHolderName;

		
	}


	public PaymentAccount(String BankCardType, String AccountHolderName) {
		// TODO Auto-generated constructor stub
		
		this.BankCardType = BankCardType;
		this.AccountHolderName = AccountHolderName;
	}


	public String getBankCardType()
	{
		return this.BankCardType;
		
	}
	
	
	public int getExpiryMonth()
	{
		return this.ExpiryMonth;
	}
	
	
	public String getMaskedAccountNumber()
	{
		return this.MaskedAccountNumber;
	}
	
	
	public String getAccountStatus()
	{
		return this.AccountStatus;
	}
	
	
	public int getExpiryYear()
	{
		return this.ExpiryYear;
	}
	

	public String getAccountHolderName()
	{
		return this.AccountHolderName;
	}
}
