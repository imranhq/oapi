package model.datatypes;

import utilities.ResponseElement;

public class Customer {
	
	private String title;
	private String fName;
	private String mName;
	private String lName;
	private String dob;
	private String email;
	private String phone;
	private String mobile;
	private String pin;
	private Address address;
	
	public Customer(ResponseElement element) {
		
	}
	
	public Customer() {
		this.title = "";
		this.fName = "";
		this.mName = "";
		this.lName = "";
		this.dob = "";
		this.email = "";
		this.phone = "";
		this.mobile = "";
		this.pin = "";
		this.address = new Address();
	}
	
	public Customer(String title, String fName, String mName, String lName, String email, String phone, String mobile, String pin, Address address) {
		this.title = title;
		this.fName = fName;
		this.mName = mName;
		this.lName = lName;
		this.email = email;
		this.phone = phone;
		this.mobile = mobile;
		this.pin = pin;
		this.address = address;
		this.dob = "";
	}
	
	public String getTitle() {
		return this.title;
	}
	
	public String getFirstName() {
		return this.fName;
	}
	
	public String getMiddleName() {
		return this.mName;
	}
	
	public String getLastName() {
		return this.lName;
	}
	
	public String getDOB() {
		return this.dob;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	public String getPhoneNumber() {
		return this.phone;
	}
	
	public String getMobileNumber() {
		return this.mobile;
	}
	
	public String getPIN() {
		return this.pin;
	}
	
	public Address getAddress() {
		return this.address;
	}
	
	public String toString() {
		return String.format("NAME: %s %s %s %s\nADDRESS: %s, %s, %s, %s\nPHONE: %s\nMOBILE: %s\nEMAIL: %s\n", this.title, this.fName, this.mName, this.lName, this.address.getAddress1(), this.address.getSuburb(), this.address.getState(), this.address.getPostcode(), this.phone, this.mobile, this.email);
	}
}
