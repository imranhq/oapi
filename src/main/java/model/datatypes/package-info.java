/**
 * Object representations of concepts within the OpenAPI, such as Tokens, Error Responses, etc.
 */
/**
 * Object representations of concepts within the OpenAPI, such as Tokens, Error Responses, etc.
 * @author Mike Fleig - mfleig@planittesting.com
 * 
 */
package model.datatypes;