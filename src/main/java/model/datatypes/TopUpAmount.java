package model.datatypes;

import utilities.ResponseElement;

public class TopUpAmount {

	private int amountInCents;
	private int value;
	private String description;
	
	public TopUpAmount(int amountInCents, int value, String description) {
		this.amountInCents = amountInCents;
		this.value = value;
		this.description = description;
	}
	
	public TopUpAmount(ResponseElement element) {
		this.amountInCents = element.getIntValue("AmountInCents");
		this.value = element.getIntValue("Value");
		this.description = element.getValue("Description");
	}
	
	public int getAmountInCents() {
		return this.amountInCents;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public String toString() {
		return String.format("{ AmountInCents=%d, Value=%d, Description=%s }", this.amountInCents, this.value, this.description);
	}
	
    @Override
    public boolean equals(Object o) {
        if (o == this) return true;   //If objects equal, is OK
        if (o instanceof TopUpAmount) {
           TopUpAmount that = (TopUpAmount)o;
           return (description.equals(that.description) && value == that.value && amountInCents == that.amountInCents);
        }
        return false;
    }
}
