package model.datatypes;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import model.response.ParameterType;
import model.response.ParameterValue;
import utilities.DateParser;
import utilities.JSONParser;

public class BPointPayReference {

	HashMap<String, String> codes;
	
	public BPointPayReference(String response) {
		codes = new HashMap<String, String>();
		parseResponse(response);
	}
	
	public BPointPayReference() {
		codes = new HashMap<String, String>();
	}
	
	private void parseResponse(String response) {
		String[] data = response.split("&");
		for(int i = 0; i < data.length; i++) {
			String[] pair = data[i].split("=");
			if(pair.length > 1) {
				codes.put(pair[0], pair[1]);
			} else {
				//System.out.println(String.format("WARNING: Response %s does not have value", pair[0]));
			}
		}
	}
	
	public String getValue(String key) {
		if(codes.containsKey(key)) {
			return codes.get(key);
		}
		
		return "-1";
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("BPOINT PAY REFERENCE: \n");
		for(String key: codes.keySet()) {
			sb.append(String.format("%s: %s\n", key, codes.get(key)));
		}
		
		return sb.toString();
	}
	
	public String toJSON() {
		HashMap<String, ParameterValue> ppr = new HashMap<String, ParameterValue>();
		String storeCard = "";
		if(Integer.parseInt(this.getValue("out_store_card")) == 0) {
			storeCard = "false";
		}
		
		if(Integer.parseInt(this.getValue("out_store_card")) == 1) {
			storeCard = "true";
		}
		
		try {
		ppr.put("ProviderReceiptId", new ParameterValue(ParameterType.STRING, this.getValue("out_receipt_number")));
		ppr.put("ProviderTransactionId", new ParameterValue(ParameterType.STRING, this.getValue("out_txn_number")));
		ppr.put("ProviderResponseCode", new ParameterValue(ParameterType.STRING, this.getValue("out_response_code")));
		ppr.put("ProviderBankResponseCode", new ParameterValue(ParameterType.STRING, this.getValue("out_bank_response_code")));
		ppr.put("ProviderAuthResultCode", new ParameterValue(ParameterType.STRING, this.getValue("out_auth_result")));
		ppr.put("AccountNumber", new ParameterValue(ParameterType.STRING, this.getValue("out_account_number")));
		ppr.put("ExpiryDate", new ParameterValue(ParameterType.STRING, java.net.URLDecoder.decode(this.getValue("out_expiry_date"), "UTF-8")));
		ppr.put("ProviderVerifyToken", new ParameterValue(ParameterType.STRING, this.getValue("out_verify_token")));
		ppr.put("PaymentDate", new ParameterValue(ParameterType.STRING, DateParser.ISO8601DateTime(this.getValue("out_payment_date"))));
		ppr.put("SettlementDate", new ParameterValue(ParameterType.STRING, DateParser.ISO8601Date(this.getValue("out_settlement_date"))));
		ppr.put("StoreAccount", new ParameterValue(ParameterType.BOOLEAN, storeCard));
		
		return JSONParser.buildJSONBody(ppr);
		} catch (UnsupportedEncodingException e) {
			return JSONParser.buildJSONBody(ppr);
		}
	}
}
