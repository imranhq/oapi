package model.DataTypes;

/**
 * DataType representing an Access Token
 * @author Mike
 *
 */
public class Token {
	
	/**
	 * The current access token.  Can be null if not set.
	 */
	public String accessToken;

	/**
	 * The current refresh token.  Can be null if not set.
	 */
	public String refreshToken;
	
	/**
	 * The current token type (usually "Bearer").  Can be null if not set.
	 */
	public String tokenType;
	
	/**
	 * The time out on the tokens in seconds.
	 */
	public int expiresIn;
	
	/**
	 * If set, is set to "openapi".
	 */
	public String scope;
	 
	/**
	 * When authenticated, a token is generated.  The details of the token are stored for future verification/use.
	 * @param accessToken {@link Token#accessToken}
	 * @param refreshToken {@link Token#refreshToken}
	 * @param tokenType {@link Token#tokenType}
	 * @param expiresIn {@link Token#expiresIn}
	 * @param scope {@link Token#scope}
	 */
	public Token(String accessToken, String refreshToken, String tokenType, int expiresIn, String scope) {
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.tokenType = tokenType;
		this.expiresIn = expiresIn;
		this.scope = scope;
	}
	
	/**
	 * Sets a blank Token.
	 */
	public Token() {

	}
}
